/*

	HashingObject.js library
	
	Version: 1.0
	 
	Relies upon:
		
		Hashing.js
	
	Updates:
		

		1.0
			Added the HashingObject library header

*/

function CreateHashingObject()
{
	return {
				MD5:new Hashes.MD5,
				SHA1:new Hashes.SHA1,
				SHA256:new Hashes.SHA256,
				SHA512:new Hashes.SHA512,
				RMD160:new Hashes.RMD160,
				
				GetMD5:function(IncomingString){return MD5.hex(IncomingString);}
				GetSHA1:function(IncomingString){return SHA1.hex(IncomingString);}
				GetSHA256:function(IncomingString){return SHA256.hex(IncomingString);}
				GetSHA512:function(IncomingString){return SHA512.hex(IncomingString);}
				GetRMD160:function(IncomingString){return RMD160.hex(IncomingString);}
			}
	
}
