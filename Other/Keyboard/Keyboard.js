/*

	Keyboard.js library
	
	Version: 2.0
	Created: 26/08/2018
	Last Updated: 02/01/2022
	
	Purpose:
		Displays an on-screen keyboard (with anti-keylogging features).
	
	Relies upon:
		PseudoRandom.js
		
	Notes:
		None
		
	Updates:
	
		2.0
			Added UnicodeChars
		
		1.0
			Added the Keyboard library header
			
	Todo:
		None
	
*/

var UnicodeChars = {
						Pound:"\u00A3",
						BlackSpade:"\u2660",
						BlackHeart:"\u2665",
						BlackDiamond:"\u2666",
						BlackClub:"\u2663",
						WhiteSpade:"\u2664",
						WhiteHeart:"\u2661",
						WhiteDiamond:"\u2662",
						WhiteClub:"\u2667",
						Sun:"\u263C",
						SunFull:"\u2600",
						Moon:"\u263E",
						Cloud:"\u2601",
						Umbrella:"\u2602",
						RainyUmbrella:"\u2614",
						HappyFace:"\u263A",
						SadFace:"\u2639",
						Anchor:"\u2693",
						Flag:"\u2690",
						FlagFull:"\u2691",
						MusicNote1:"\u2669",
						MusicNote2:"\u266A",
						MusicNote3:"\u266B",
						MusicNote4:"\u266C",
						RadiationWarning:"\u2622",
						PoisonWarning:"\u2620",
						BiologicalHazard:"\u2623",
						WarningSymbol:"\u26A0",
						Clock:"\u231A",
						WhiteQueen:"\u2654",
						WhiteKing:"\u2655",
						WhiteRook:"\u2656",
						WhiteBishop:"\u2657",
						WhiteKnight:"\u2658",
						WhitePawn:"\u2659",
						BlackQueen:"\u265A",
						BlackKing:"\u265B",
						BlackRook:"\u265C",
						BlackBishop:"\u265D",
						BlackKnight:"\u265E",
						BlackPawn:"\u265F"
					};

var KeyboardButtonStyle = {
								"background-color":"#E0E0E0",
								"text-align":"center",
								"width":"25px",
								"height":"25px",
								"margin-left":"4px",
								"margin-right":"4px",
								"margin-top":"4px",
								"margin-bottom":"4px",
								"padding-left":"4px",
								"padding-right":"4px",
								"padding-top":"4px",
								"padding-bottom":"4px",
								"font-size":"20px",
								"border":"solid",
								"border-width":"2px",
								"display":"inline-block",
								"-moz-box-shadow":"4px 4px 5px 0px grey",
								"-webkit-box-shadow":"4px 4px 5px 0px grey",
								"box-shadow":"4px 4px 5px 0px grey"
							};

var SpaceButtonSpace = {
							"background-color":"#E0E0E0",
							"text-align":"center",
							"width":"200px",
							"height":"25px",
							"margin-left":"4px",
							"margin-right":"4px",
							"margin-top":"4px",
							"margin-bottom":"4px",
							"padding-left":"4px",
							"padding-right":"4px",
							"padding-top":"4px",
							"padding-bottom":"4px",
							"font-size":"25px",
							"border":"solid",
							"border-width":"2px",
							"display":"inline-block",
							"-moz-box-shadow":"4px 4px 5px 0px grey",
							"-webkit-box-shadow":"4px 4px 5px 0px grey",
							"box-shadow":"4px 4px 5px 0px grey"
						};

function CreateArmourDilloKeyboard()
{
	try
	{
		var Temp = {
						
						RemoveTargetElementChildren:function(TargetElement)
						{
							try
							{
								while(TargetElement.firstChild)
								{
									TargetElement.removeChild(Temp.firstChild);
								}
							}
							catch(err)
							{
								throw{name:"RemoveTargetElementChildren error",message:"RemoveTargetElementChildren error: "+err.message}
							}
						},
						RandomiseKeyboard:function(TargetElement,XORObject)
						{
							try
							{
								RemoveTargetElementChildren(TargetElement);
								CreateKeyboardUI(TargetElement,XORObject);
							}
							catch(err)
							{
								throw{name:"RandomiseKeyboard error",message:"RandomiseKeyboard error: "+err.message}
							}
						},
						CreateKeyboardUI:function(TargetElementName,TargetDisplayElementID,XORObject)
						{
							try
							{
								
								var TempCEO = CreateCommonEncryptionsObject();
								var UnicodeList = "";
								
								for(var Key in UnicodeChars)
								{
									UnicodeList += UnicodeChars[Key];
								}
								
								UnicodeList += "!\"$%^&*()_+QWERTYUIOP{}ASDFGHJKL:@~|ZXCVBNM<>? 1234567890-=qwertyuiop[]asdfghjkl;'#\\zxcvbnm,./";
								
								//Yes, this should really support unicode
								UnicodeList = TempCEO.StringToArrayOfInts(UnicodeList);
								
								var Target = document.getElementById(TargetElementName);
								AppendListToKeyboard(Target,UnicodeList,TargetDisplayElementID,XORObject);
							}
							catch(err)
							{
								throw{name:"CreateKeyboardUI error",message:"CreateKeyboardUI error: "+err.message}
							}	
						},
						AppendListToKeyboard:function(TargetElement,KeyList,TargetDisplayElementID,XORObject)
						{
							try
							{
								
								var Temp = null,
									Iter = 0;
								
								while(KeyList.length > 0)
								{
									Iter = GenerateCryptoIntBetweenRanges(0,KeyList.length-1);
									
									Temp = document.createElement("span");
									Temp.innerHTML = ""+String.fromCharCode(KeyList[Iter]);
									
									if(Temp.innerHTML == " ")
									{
										for(var Key in SpaceButtonSpace)
										{
											Temp.style[Key] = SpaceButtonSpace[Key];	
										}
									}
									else
									{
										for(var Key in KeyboardButtonStyle)
										{
											Temp.style[Key] = KeyboardButtonStyle[Key];	
										}
									}
									
									Temp.onclick = (function(TargetElement2, TargetDisplayElementID2, XORObject2)
													{
														if(!!XORObject2)
														{
															XORObject2.AddInt(XORObject2.CEO.StringToArrayOfInts(this.innerHTML)[0]);
															TargetDisplayElementID2.value += "*";
														}
										
														if(!!TargetElement2)
														{
															RandomiseKeyboard(TargetElement2,XORObject2);
														}
													}.bind(Temp,TargetElement,TargetDisplayElementID,XORObject));

									TargetElement.appendChild(Temp);
									
									KeyList.splice(Iter, 1);
									
								}
							}
							catch(err)
							{
								throw{name:"RemoveTargetElementChildren error",message:"RemoveTargetElementChildren error: "+err.message}	
							}
						},
					};
	}
	catch(err)
	{
		throw{name:"CreateArmourDilloKeyboard error",message:"CreateArmourDilloKeyboard error: "+err.message}
	}
}
