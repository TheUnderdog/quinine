/*
	DiffieHellmanUpgradedKeyExchangeBigInt.js
	
	Created: 28/06/2021
	Updated: 28/06/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		XORMemoryBigInt.js
		PrimeBigInt.js
		Temporal.js
	
	Updates:
		
		2.0
			Added CreateDHUKEBI
			Added Initialise
			Added GeneratePrimeAndRootPrimitive
			Added GenerateSuitableSecret
			Added GenerateSecretAndSharedKey
			Added GenerateSharedSecret
			Added GetSharedKeyAsXORMemory
			Added TestDHUKEBI
			Patched "GetBigAt" typos to GetBigIntAt
		
		1.0
			Added library

*/

function CreateDHUKEBI(TargetXORMemory)
{
	"use strict";
	
	try
	{
		var Temp = {
					//All of these variables **must** be BigInts
					//Pos 0: ModPrime
					//Pos 1: ModPrime length
					//Pos 2: BaseRootPrimitive
					//Pos 3: SecretKey
					//Pos 4: SharedKey
					//Pos 5: Prime initialising variable (EG Timedate)
					//Pos 6: SharedSecret
					//Pos 7: OtherIdentifier
					//Pos 8: OwnIdentifier
					XORMemory:CreateXORMemoryBigInt(9),
					//XORMemory must contain two pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime
					Initialise:function(IncomingXORMemory)
					{
						try
						{
							if(typeof(IncomingXORMemory) === "object")
							{
								this.GeneratePrimeAndRootPrimitive(IncomingXORMemory)
								this.GenerateSuitableSecret()
								this.GenerateSecretAndSharedKey()
							}
							
							this.XORMemory.SetBigIntAt(G_PRBIO.GetGeneratedBigInt() ^ G_PRBIO.GetGeneratedBigInt(),8)
						}
						catch(err)
						{
							throw{name:"Initialise (CreateDHUKEBI) error",message:"Initialise (CreateDHUKEBI), "+err.lineNumber+": "+err.message}
						}
					},
					//XORMemory must contain two pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime
					//Pos 2: prime number itself
					//Pos 3: root primitive
					AcceptPrimeAndRootPrimitive:function(IncomingXORMemory)
					{
						console.log("AcceptPrimeAndRootPrimitive");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"AcceptPrimeAndGenerateRootPrimitive error",message:"IncomingXORMemory is not an object."}
							}
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 5: Prime initialising variable
							
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,253n),5,253n) //Prime initialising variable
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,252n),1,252n) //modulus prime len
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(2,241n),0,241n) //modulus prime
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(3,230n),2,230n) //BaseRootPrimitive
						}
						catch(err)
						{
							throw{name:"AcceptPrimeAndGenerateRootPrimitive error",message:"AcceptPrimeAndGenerateRootPrimitive, "+err.lineNumber+": "+err.message}
						}
					},
					//XORMemory must contain two pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime
					GeneratePrimeAndRootPrimitive:function(IncomingXORMemory)
					{
						"use strict";
						console.log("GeneratePrimeAndRootPrimitive");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"GeneratePrimeAndRootPrimitive error",message:"IncomingXORMemory is not an object."}
							}
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 5: Prime initialising variable
							
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,229n),5,229n) //PIV
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,101n),1,101n) //MP len
							this.XORMemory.SetBigIntAt(G_PGBI.FindNextNearestPrimeBigIntTargetLength(IncomingXORMemory.GetBigIntAt(0),IncomingXORMemory.GetBigIntAt(1)),0) //MP
							this.XORMemory.SetBigIntAt(G_PGBI.FindRootPrimitiveBigInt(this.XORMemory.GetBigIntAt(0)),2) //BRP
						}
						catch(err)
						{
							throw{name:"GeneratePrimeAndRootPrimitive error",message:"GeneratePrimeAndRootPrimitive, "+err.lineNumber+": "+err.message}
						}
					},
					//Must call GeneratePrimeAndRootPrimitive before calling this
					GenerateSuitableSecret:function()
					{
						"use strict";
						console.log("GenerateSuitableSecret");
						
						try
						{
							//Pos 3: SecretKey
							this.XORMemory.SetBigIntAt(G_PRBIO.GetGeneratedBigInt(),3);
							
							//This should generate a batch of individual ints into XOR int memory storage
							//And then append those numbers together to form a BigInt
							
							
							//Pos 1: ModPrime length
							while(this.XORMemory.GetBigIntAt(3).toString().length < (this.XORMemory.GetBigIntAt(1).toString().length+1) )
							{
								//Pos 3: SecretKey
								this.XORMemory.SetBigIntAt(this.XORMemory.GetBigIntAt(3) ^ G_PRBIO.GetGeneratedBigInt(),3)
							};
							
							return;
						}
						catch(err)
						{
							throw{name:"GenerateSuitableSecret error",message:"GenerateSuitableSecret, "+err.lineNumber+": "+err.message}
						}
					},
					//GenerateSuitableSecret must be called before this
					GenerateSecretAndSharedKey:function()
					{
						"use strict";
						console.log("GenerateSecretAndSharedKey");
						try
						{
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 3: SecretKey
							//Pos 4: SharedKey
							
							//Base^SecretKey mod ModPrime
							this.XORMemory.SetBigIntAt(G_PGBI.Pow3BigInt(this.XORMemory.GetBigIntAt(2), this.XORMemory.GetBigIntAt(3), this.XORMemory.GetBigIntAt(0)),4)
						}
						catch(err)
						{
							throw{name:"GenerateSecretAndSharedKey error",message:"GenerateSecretAndSharedKey, "+err.lineNumber+": "+err.message}
						}
					},
					//GenerateSecretAndSharedKey must be called before this
					//Pos 0: IncomingSharedKey
					//Pos 1: IncomingIdenifier
					GenerateSharedSecret:function(IncomingXORMemory)
					{
						"use strict";
						console.log("GenerateSharedSecret");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"GenerateSharedSecret error",message:"IncomingXORMemory is not an object."}
							}
							
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 3: SecretKey
							//Pos 4: SharedKey
							//Pos 5: Prime initialising variable (EG Timedate)
							//Pos 6: SharedSecret
							//Pos 7: OtherIdentifier
							
							//SharedSecret = IncomingSharedKey ^ SecretKey % ModPrime
							
							this.XORMemory.SetBigIntAt(G_PGBI.Pow3BigInt(IncomingXORMemory.GetBigIntAt(0),this.XORMemory.GetBigIntAt(3),this.XORMemory.GetBigIntAt(0)),6)
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,73n),7,73n)
						}
						catch(err)
						{
							throw{name:"GenerateSharedSecret error",message:"GenerateSharedSecret, "+err.lineNumber+": "+err.message}
						}
					},
					//GenerateSecretAndSharedKey must be called before this
					//Pos 0: IncomingSharedKey
					//Pos 1: IncomingIdenifier
					GetSharedKeyAsXORMemory:function()
					{
						try
						{
							var Temp = CreateXORMemoryBigInt(2);
							
							//Pos 4: SharedKey
							Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(4,41n),0,41n)
							//Pos 8: OurIdentifier
							Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(8,37n),1,37n)
							return Temp;
						}
						catch(err)
						{
							throw{name:"GetSharedKeyAsXORMemory error",message:"GetSharedKeyAsXORMemory, "+err.lineNumber+": "+err.message}
						}
					},
					ExportSharedKeyAsJSON:function()
					{
						try
						{
							return this.GetSharedKeyAsXORMemory();
						}
						catch(err)
						{
							throw{name:"ExportSharedKeyAsJSON error",message:"ExportSharedKeyAsJSON, "+err.lineNumber+": "+err.message}
						}
					}
				};
				
			Temp.Initialise(TargetXORMemory);
			
			return Temp;
	}
	catch(err)
	{
		throw{name:"CreateDHUKEBI error",message:"CreateDHUKEBI, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:CreateDHUKEBI,IsWebWorkerCompatible:true});

function TestDHUKEBI()
{
	try
	{
		//Pos 0: timedate stamp (or other prime initialising variable)
		//Pos 1: target length of prime
		
		var TDP = CreateXORMemoryBigInt(2)
		
		//ssmmhhDDMMYYYY
		//20202020102020
		//256 length prime
		
		TDP.SetBigIntAt(20202020102020n,0);
		TDP.SetBigIntAt(10n,1);
		
		console.log("Key A start");
		var KeyA = CreateDHUKEBI(TDP);
		console.log("Key A finished.\r\nKey B start");
		var KeyB = CreateDHUKEBI(TDP);
		console.log("Key B finished");
		
		console.log("KeyA Prime is: "+KeyA.XORMemory.GetBigIntAt(0));
		console.log("KeyB Prime is: "+KeyB.XORMemory.GetBigIntAt(0));
		
		//Pos 0: IncomingSharedKey
		//Pos 1: IncomingIdenifier
		console.log("Client A start");
		var ClientA = KeyA.GetSharedKeyAsXORMemory()
		console.log("Client A finished. Client B start");
		var ClientB = KeyB.GetSharedKeyAsXORMemory()
		console.log("Client B finished");
		
		var FakeIDA = 2323232324n;
		var FakeIDB = 2323232321n;
		
		ClientA.SetBigIntAt(FakeIDA,1);
		ClientB.SetBigIntAt(FakeIDB,1);
		
		KeyA.GenerateSharedSecret(ClientB);
		KeyB.GenerateSharedSecret(ClientA);
	
		alert("SharedSecret for A is: "+KeyA.XORMemory.GetBigIntAt(6)+". SharedSecret for B is: "+KeyB.XORMemory.GetBigIntAt(6));
	}
	catch(err)
	{
		throw{name:"TestDHUKEBI error",message:"TestDHUKEBI, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:TestDHUKEBI,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"DiffieHellmanUpgradedKeyExchangeBigInt.js"});
