/*
	PrimeBigInt.js
	
	Version: 4.1
	Created: 26/06/2021
	Updated: 11/07/2021
	
	Credit(s):
		Anton (SquareRootBigInt function, modified)
	
	Depends upon:
		PseudoRandom.js
	
	Licence(s):
		
		Rest of the code: MIT
	
	Updates:
	
		5.0
			Updated to use new JSLL DataObject format
	
		4.1
			Patched FindNextNearestPrimeBigIntTargetLength to call BigInts
			Patched FindNextNearestPrimeBigInt to call BigInt
			Added error handling to SquareRootBigInt and removed redundant error check
			Patched SquareRootBigInt to use IncomingBigInt instead of value
		
		4.0
			Added FindNextNearestPrimeBigInt
		
		3.0
			BigInt Overhaul to enable compatibility with arbitarily large primes
		
		2.0
			Added updates
		
		1.0
			Added library

*/

function PrimeGeneratorBigInt()
{
	"use strict";
	return {
				//[SquareRootStart,ResponseValue]
				SquareRootCache:[],
				ShuffleCache:function(Iter)
				{
					"use strict";
					try
					{
						if(Iter < 1){return Iter;}
						
						let Temp = this.SquareRootCache[Iter-1].slice();
						
						this.SquareRootCache[Iter-1] = this.SquareRootCache[Iter];
						
						this.SquareRootCache[Iter] = Temp;
						
						return Iter-1;
					}
					catch(err)
					{
						throw{name:"ShuffleCache error",message:"ShuffleCache, line number "+err.lineNumber+": "+err.message}
					}
				},
				CheckAndSetSquareRootCache:function(IncomingComparisonNumber)
				{
					try
					{
						if(this.SquareRootCache == null)
						{
							this.SquareRootCache = [];
						}
						
						if(this.SquareRootCache.length < 1)
						{
							return -1;
						}
						
						for(var Iter = 0;Iter < this.SquareRootCache.length;++Iter)
						{
							if(this.SquareRootCache[Iter][0] === IncomingComparisonNumber)
							{
								return this.ShuffleCache(Iter);
							}
						}
						
						return -1;
					
					}
					catch(err)
					{
						throw{name:"CheckAndSetSquareRootCache error",message:"CheckAndSetSquareRootCache, line number "+err.lineNumber+": "+err.message}
					}
				},
				SquareRootBigIntLoop:function(IncomingBigInt)
				{
					try
					{
						if(IncomingBigInt == undefined || IncomingBigInt == null)
						{
							throw{name:"SquareRootBigInt error",message:"IncomingBigInt is undefined/null.",lineNumber:45}
						}

						if(IncomingBigInt < 2n)
						{
							return IncomingBigInt;
						}
						
						var OriginalPoint = 1n, NewPoint = 1n;
						
						do
						{
							OriginalPoint = NewPoint;
						
							NewPoint = ((IncomingBigInt / OriginalPoint) + OriginalPoint) >> 1n;
						
						}
						while( !(OriginalPoint == NewPoint || OriginalPoint === (NewPoint - 1n)) ) 
						
						return OriginalPoint;
					
					}
					catch(err)
					{
						throw{name:"SquareRootBigIntLoop error",message:"SquareRootBigIntLoop, line number "+err.lineNumber+": "+err.message}
					}
					finally
					{
						NewPoint = null;
					}
				},
				SquareRootBigInt:function(IncomingBigInt)
				{
					"use strict";
					try
					{
						if(IncomingBigInt == undefined || IncomingBigInt == null)
						{
							throw{name:"SquareRootBigInt error",message:"IncomingBigInt is undefined/null.",lineNumber:45}
						}

						if(IncomingBigInt < 2n)
						{
							return IncomingBigInt;
						}
						
						var TempIter = this.CheckAndSetSquareRootCache(IncomingBigInt);
						
						if(TempIter > -1)
						{
							return this.SquareRootCache[TempIter][1];
						}

						let TempSquareRoot = this.SquareRootBigIntLoop(IncomingBigInt);
					
						this.SquareRootCache.shift([IncomingBigInt+0n,TempSquareRoot])
						
						if(this.SquareRootCache.length > 1000)
						{
							this.SquareRootCache.pop();
						}
						
						return TempSquareRoot;
					}
					catch(err)
					{
						throw{name:"SquareRootBigInt error",message:"SquareRootBigInt, line number "+err.lineNumber+": "+err.message}
					}
				},
				Pow3BigInt:function(IncomingBase, IncomingExp, IncomingMod)
				{
					try
					{	
						var Base = BigInt(IncomingBase);
						var Exp = BigInt(IncomingExp);
						var Mod = BigInt(IncomingMod);
						
						var Result = 1n;
					 
						Base = Base % Mod;
						
						while(Exp > 0n)
						{
							if(Exp & 1n)
							{
								Result = (Result * Base) % Mod;
							}
					 
							Exp = Exp >> 1n;
							Base = (Base * Base) % Mod;
						}
						
						return Result;
					}
					catch(err)
					{
						throw{name:"Pow3BigInt error",message:"Pow3BigInt, line "+err.lineNumber+": "+err.message}
					}
					finally
					{
						Base = null;
						Exp = null;
						Mod = null;
					}
				},
				RabinMillerTestBigInt:function(IncomingPotentialPrime, IncomingRandomisationFactor)
				{
					try
					{
						var PotentialPrime = BigInt(IncomingPotentialPrime)
						var RandomisationFactor = BigInt(IncomingRandomisationFactor)
						
						var IPPMinusOne = PotentialPrime-1n

						var MaxExponent = IPPMinusOne
						
						while( !(MaxExponent & 1n) )
						{
							MaxExponent = MaxExponent >> 1n;
						}
						
						if(this.Pow3BigInt(RandomisationFactor,MaxExponent,PotentialPrime) == 1n)
						{
							return true;
						}
						
						while( MaxExponent < IPPMinusOne )
						{
							if(this.Pow3BigInt(RandomisationFactor,MaxExponent,PotentialPrime) == IPPMinusOne)
							{		
								return true;
							}
							
							MaxExponent = MaxExponent << 1n;
						}
						
						return false;
					}
					catch(err)
					{
						throw{name:"RabinMillerTestBigInt error",message:"RabinMillerTestBigInt, line "+err.lineNumber+": "+err.message}
					}
					finally
					{
						PotentialPrime = null;
						RandomisationFactor = null;
						IPPMinusOne = null;
						MaxExponent = null;
					}
				},
				IsRabinMillerBigInt:function(IncomingPotentialPrime, IncomingNumberOfPasses=40)
				{
					try
					{
						
						var PotentialPrime = BigInt(IncomingPotentialPrime)
						
						var RandomBigInt = null;
						var IPPMinusOne = PotentialPrime-1n;
						
						for(var Iter = 0; Iter < IncomingNumberOfPasses;Iter++)
						{
							RandomBigInt = G_PRBIO.GetGeneratedBigIntBetweenRange(2n,IPPMinusOne);
							
							if( !this.RabinMillerTestBigInt(PotentialPrime,RandomBigInt) )
							{
								return false;
							}
						}
						
						return true;
					}
					catch(err)
					{
						throw{name:"IsRabinMillerBigInt error",message:"IsRabinMillerBigInt, line "+err.lineNumber+": "+err.message}
					}
					finally
					{
						PotentialPrime = null;
						RandomBigInt = null;
						IPPMinusOne = null;
					}
				},
				IsPrimeBigInt:function(IncomingPotentialPrime)
				{
					try
					{
						return this.IsRabinMillerBigInt(IncomingPotentialPrime)
					}
					catch(err)
					{
						throw{name:"IsPrimeBigInt error",message:"IsPrimeBigInt, line "+err.lineNumber+": "+err.message}
					}
				},
				FindRootPrimitivePrimeBigInt:function(IncomingPotentialPrime)
				{
					"use strict";
					try
					{
						if(IncomingPotentialPrime == undefined)
						{
							throw{name:"FindRootPrimitivePrimeBigInt error",message:"IncomingPotentialPrime is undefined"}
						}
						
						if(IncomingPotentialPrime == null)
						{
							throw{name:"FindRootPrimitivePrimeBigInt error",message:"IncomingPotentialPrime is null"}
						}
						
						var PotentialPrime = BigInt(IncomingPotentialPrime)
					
						if(!this.IsPrimeBigInt(PotentialPrime))
						{
							throw{name:"FindRootPrimitivePrimeBigInt error",message:"IncomingPotentialPrime is not Prime"}
						}
					 
						var IPPMinusOne = PotentialPrime - 1n;
						
						for(var Item = this.FindNextNearestPrimeBackwardsBigInt(IPPMinusOne);Item > 2n;Item = this.FindNextNearestPrimeBackwardsBigInt(Item-1))
						{
							if(this.Pow3BigInt(Iter, IPPMinusOne / Item, PotentialPrime) == 1n)
							{
								TempFlag = true;
								break;
							}
						}
						
						return 2n;
					}
					catch(err)
					{
						throw{name:"FindRootPrimitivePrimeBigInt error",message:"FindRootPrimitivePrimeBigInt, line "+err.lineNumber+": "+err.message}
					}
					finally
					{
						PotentialPrime = null;
						IPPMinusOne = null;
						PrimeFactorSet = null;
					}
				},
				FindNextNearestPrimeBackwardsBigInt:function(IncomingStartPoint)
				{
					"use strict";
					try
					{
						var PossPrime = BigInt(IncomingStartPoint)
						
						if((PossPrime & 1n) != 1n)
						{
							PossPrime = PossPrime - 1n;
						}
						
						//Sophie Germain prime
						//do{
							
							PossPrime = PossPrime - 2n;
						
							if(PossPrime < 3n)
							{
								return 0n;
							}
							
							while(!this.IsPrimeBigInt(PossPrime))
							{
								PossPrime = PossPrime - 2n;
								
								if((PossPrime % 5n) == 0n)
								{
									PossPrime = PossPrime - 2n;
								}
							}
						
						//}while(!this.IsPrimeBigInt( (PossPrime*2n) + 1n ) && !this.IsPrimeBigInt( (PossPrime-1n) / 2n ) );
						
						
						return PossPrime;
					}
					catch(err)
					{
						throw{name:"FindNextNearestPrimeBigInt error",message:"FindNextNearestPrimeBigInt, line "+err.lineNumber+": "+err.message}
					}
				},
				FindNextNearestPrimeBigInt:function(IncomingStartPoint)
				{
					"use strict";
					try
					{
						var PossPrime = BigInt(IncomingStartPoint)
						
						if(PossPrime < 3n)
						{
							PossPrime = 3n;
						}
						else if((PossPrime % 2n) == 0n)
						{
							PossPrime = PossPrime + 1n;
						}
						
						//Sophie Germain prime
						//do{
							PossPrime = PossPrime + 2n;
							
							while(!this.IsPrimeBigInt(PossPrime))
							{
								PossPrime = PossPrime + 2n;
								
								if((PossPrime % 5n) == 0n)
								{
									PossPrime = PossPrime + 2n;
								}
							}
						
						//}while(!this.IsPrimeBigInt( (PossPrime*2n) + 1n ) && !this.IsPrimeBigInt( (PossPrime-1n) / 2n ) );
						
						return PossPrime;
					}
					catch(err)
					{
						throw{name:"FindNextNearestPrimeBigInt error",message:"FindNextNearestPrimeBigInt, line "+err.lineNumber+": "+err.message}
					}
				},
				FindNextNearestPrimeBigIntTargetLength:function(IncomingStartPoint,SuggestedLength)
				{
					try
					{
						var Iter = BigInt(IncomingStartPoint)
						
						if(Iter < 3n)
						{
							Iter = 3n;
						}
						
						while(Iter.toString().length < SuggestedLength)
						{
							Iter = (Iter * 2n) ^ Iter
							Iter = (Iter * 2n) + 1n
						}
						
						return this.FindNextNearestPrimeBigInt(Iter);
					}
					catch(err)
					{
						throw{name:"FindNextNearestPrimeBigIntTargetLength error",message:"FindNextNearestPrimeBigIntTargetLength, line "+err.lineNumber+": "+err.message}
					}
				}

			}
}

G_Library.Add({Type:"Function",Functor:PrimeGeneratorBigInt,IsWebWorkerCompatible:true});

var G_PGBI = PrimeGeneratorBigInt();

G_Library.Add({Type:"Variable",Variable:G_PGBI,Constructor:PrimeGeneratorBigInt,VariableName:"G_PGBI",IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"PrimeBigInt.js"});
