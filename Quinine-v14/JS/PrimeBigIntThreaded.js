/*
	PrimeBigIntThreaded.js
	
	Version: 3.0
	Created: 11/07/2021
	Updated: 16/07/2021
	
	Depends upon:
		PseudoRandomBigInt.js
		PrimeBigInt.js
		Threaded.js
	
	Licence(s):
		
		Rest of code: MIT
	
	Updates:
	
		3.0
			Fixed OnResult and OnComplete calls
	
		2.0
			Overhauled for compatibility with the JavaScriptLibraryLoader
		
		1.0
			Added library

*/
		
function FindPrimeThreaded(TargetIncomingOptionalNumberOfWorkers)
{
	"use strict";
	
	try
	{
		var Temp = {
					HasResult:false,
					FPTWorkerManager:null,

					OnResultHookReference:null,
					LastWorkloadReference:null,
					//Check if we have a valid prime
					Initialise:function(IncomingNumberOfWorkers)
					{
						"use strict";
						try
						{
							this.FPTWorkerManager = WorkerManager(WorkerFunction,G_Library.GetWebWorkerLibrary(),IncomingNumberOfWorkers);
						}
						catch(err)
						{
							throw{name:"Initialise error",message:"Initialise: "+err.message}
						}
					},
					ConditionalHook:function(IncomingWorkload)
					{
						"use strict";
						if(IncomingWorkload.WorkloadResult == undefined || IncomingWorkload.WorkloadResult == null)
						{
							return false;
						}
						
						return IncomingWorkload.WorkloadResult;
					},
					OnCompleteHook:function(IncomingWorkloads)
					{
						"use strict";
						try
						{
							if(!this.HasResult)
							{
								this.FindNextNearestPrimeBigIntThreaded(this.LastWorkloadReference[this.LastWorkloadReference.length-1],this.OnResultHookReference)
							}
						}
						catch(err)
						{
							throw{name:"OnCompleteHook error",message:"OnCompleteHook: "+err.message}
						}
					},
					//If we have a valid prime
					OnResultHook:function(IncomingWorkload)
					{
						"use strict";
						try
						{
							if(IncomingWorkload.WorkloadStatus === 1 && !this.HasResult)
							{
								this.HasResult = true;
								this.OnResultHookReference(this.LastWorkloadReference[IncomingWorkload.WorkloadID][0]);
							}
						}
						catch(err)
						{
							throw{name:"OnResultHook error",message:"OnResultHook: "+err.message}
						}
					},
					//Generate a stack of primes to go through
					GenerateXNumberOfPossiblePrimes:function(IncomingStartPoint,XNumber)
					{
						"use strict";
						try
						{
							if(this.HasResult)
							{
								this.HasResult = false;
								this.FPTWorkerManager.ClearWorkload();
								return;
							}
							
							this.LastWorkloadReference = [];
							
							var PossPrime = IncomingStartPoint + 1n;
							
							if(PossPrime < 3n)
							{
								PossPrime = 3n;
							}
							else if((PossPrime&1n) == 0n)
							{
								PossPrime = PossPrime + 1n;
							}
							
							for(var Iter = 0;Iter < XNumber;Iter++)
							{
								PossPrime = PossPrime + 2n;
								
								if((PossPrime % 5n) == 0n)
								{
									PossPrime = PossPrime + 2n;
								}
								
								this.LastWorkloadReference[Iter] = [PossPrime];
							}
							
							function Temp(PossiblePrime)
							{
								var Result = G_PGBI.IsPrimeBigInt(PossiblePrime);
								
								//if(Result)
								//{
									//Sophie Germain prime check
									//Result = ( G_PGBI.IsPrimeBigInt( (PossiblePrime*2n) + 1n ) || G_PGBI.IsPrimeBigInt( (PossiblePrime-1n) / 2n ) )
								//}
								
								return Result;
							}
							
							this.FPTWorkerManager.ProcessWorkload(Temp,this.LastWorkloadReference);
						}
						catch(err)
						{
							throw{name:"GenerateXNumberOfPossiblePrimes error",message:"GenerateXNumberOfPossiblePrimes, line number "+err.lineNumber+": "+err.message}
						}
						finally
						{
							PossPrime = null;
							
						}
					},
					//Find the next nearest prime using multi-threading
					FindNextNearestPrimeBigIntThreaded:function(IncomingStartPoint,OnResultHook)
					{
						"use strict";
						try
						{
							this.HasResult = false;
							
							this.OnResultHookReference = OnResultHook;
							
							var BInt = BigInt(IncomingStartPoint);
							
							this.FPTWorkerManager.SetOnCompleteHook(this.OnCompleteHook.bind(this));
							this.FPTWorkerManager.SetConditionalHook(this.ConditionalHook.bind(this));
							this.FPTWorkerManager.SetOnResultHook(this.OnResultHook.bind(this));
							this.GenerateXNumberOfPossiblePrimes(BInt,100);
						}
						catch(err)
						{
							throw{name:"FindNextNearestPrimeBigIntThreaded error",message:"FindNextNearestPrimeBigIntThreaded: "+err.message}
						}
						finally
						{
							BInt = null;
						}
					},
					
					Destructor:function()
					{
						"use strict";
						this.FPTWorkerManager.Destructor();
					},
				};
				
		Temp.Initialise( ((TargetIncomingOptionalNumberOfWorkers === undefined) ? null : TargetIncomingOptionalNumberOfWorkers) );
		return Temp;
	}
	catch(err)
	{
		throw{name:"FindPrimeThreaded error",message:"FindPrimeThreaded, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:FindPrimeThreaded,IsWebWorkerCompatible:true});

var G_FindPrimeThreaded = FindPrimeThreaded();

G_Library.Add({Type:"Variable",Variable:G_Library,Constructor:FindPrimeThreaded,VariableName:"G_FindPrimeThreaded",IsWebWorkerCompatible:true});

function FindNextNearestPrimeBigIntThreaded(IncomingStartPoint,OnResultHook)
{
	"use strict";
	try
	{
		G_FindPrimeThreaded.FindNextNearestPrimeBigIntThreaded(IncomingStartPoint,OnResultHook);
	}
	catch(err)
	{
		throw{name:"FindNextNearestPrimeBigIntThreaded error",message:"FindNextNearestPrimeBigIntThreaded: "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:FindNextNearestPrimeBigIntThreaded,IsWebWorkerCompatible:true});

function FindNextNearestPrimeBigIntTargetLengthThreaded(IncomingStartPoint,SuggestedLength,OnResultHook)
{
	"use strict";
	try
	{
		var Iter = BigInt(IncomingStartPoint)
		
		if(Iter < 3n)
		{
			Iter = 3n;
		}
		
		while(Iter.toString().length < SuggestedLength)
		{
			Iter = (Iter * 2n) ^ Iter
			Iter = (Iter * 2n) + 1n
		}
		
		return FindNextNearestPrimeBigIntThreaded(Iter,OnResultHook);
	}
	catch(err)
	{
		throw{name:"FindNextNearestPrimeBigIntTargetLengthThreaded error",message:"FindNextNearestPrimeBigIntTargetLengthThreaded, line "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:FindNextNearestPrimeBigIntTargetLengthThreaded,IsWebWorkerCompatible:true});

function FindNextNearestPrimeBigIntTargetLengthSingleThread(IncomingStartPoint,SuggestedLength)
{
	"use strict";
	try
	{
		return G_PGBI.FindNextNearestPrimeBigIntTargetLength(IncomingStartPoint,SuggestedLength);
	}
	catch(err)
	{
		throw{name:"FindNextNearestPrimeBigIntTargetLengthSingleThread error",message:"FindNextNearestPrimeBigIntTargetLengthSingleThread, line "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:FindNextNearestPrimeBigIntTargetLengthSingleThread,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"PrimeBigIntThreaded.js"});
