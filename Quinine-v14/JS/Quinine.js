/*
	Quinine.js
	
	Created: 22/07/2021
	Updated: 22/07/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Detector.js
		Temporal.js
		lz-string.js
		Threaded.js
		PrimeBigIntThreaded.js
	
	Updates:
		
		1.0
			Added library

*/

var G_QuinineVersion = 14n;
var G_TimestampLength = 14;

function ClearType(TargetID)
{
	try
	{
		var Target = document.getElementById(TargetID);
		var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
		Target[Type] = "";
	}
	catch(err)
	{
		alert("ClearType: "+err.message);
	}
}

function ClearDecrypted()	{ ClearType("unencrypted");	}
function ClearEncrypted()	{ ClearType("encrypted");	}


function TimestampOnChange(IncomingElement)
{
	try
	{
		var Temp = IncomingElement.value;
		IncomingElement.value = Temp + "0".repeat(G_TimestampLength - Temp.length)
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function TimestampOnLoad(IncomingID)
{
	try
	{
		var IncomingElement = document.getElementById(IncomingID);
		var Temp = CreateTemporalObject();
		Temp.LoadCurrentTime();
		IncomingElement.value =	Temp.GetFixedTime("YYYYMMDDmmhhss");
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function PrimeLengthOnChange(PrimeLengthElement,PrimeBitsElementID,PrimeEstTimeID)
{
	try
	{
		var PBEI = document.getElementById(PrimeBitsElementID);
		var PETI = document.getElementById(PrimeEstTimeID);
		
		if(PrimeLengthElement.value == "")
		{
			PrimeLengthElement.value = 1;
		}
		
		var TempLen = Math.abs(parseInt(PrimeLengthElement.value));
		
		PrimeLengthElement.value = TempLen;
		
		PBEI.value = TempLen * 8;
		PETI.value = (TempLen / 100) / 2
		
	}
	catch(err)
	{
		alert("PrimeLengthOnChange: "+err.message);
	}
	finally
	{
		PBEI = null;
		PETI = null;
		TempLen = null;
	}
}

function FlipTimestamp(IncomingTimestamp,TemporalPrecision)
{
	try
	{
		var TempTO = CreateTemporalObject();
		var Temp = ""+IncomingTimestamp;
		
		if(Temp.length > G_TimestampLength)
		{
			Temp = Temp.substring(0,G_TimestampLength);
		}
		
		var TempPrecision = parseInt(TemporalPrecision)
		
		if(Temp.length > TempPrecision)
		{
			Temp = (TempPrecision > 0) ? Temp.substring(0,TempPrecision) : "";
		}
		
		if(Temp.length < G_TimestampLength)
		{
			Temp = Temp + "0".repeat(G_TimestampLength - Temp.length)
		}
		
		TempTO.SetFixedTime(Temp,"YYYYMMDDhhmmss");
		
		var NewTimestamp = TempTO.GetFixedTime("ssmmhhDDMMYYYY");
		
		
		while(NewTimestamp.charAt(0) == "0")
		{
			NewTimestamp = NewTimestamp.substring(1);
		}
		
		if(NewTimestamp == "")
		{
			NewTimestamp = "0";
		}
		
		return NewTimestamp;
	}
	catch(err)
	{
		throw{name:"FlipTimestamp error",message:"FlipTimestamp, line number "+err.lineNumber+": "+err.message}
	}
}

//Pos 0: Prime length
//Pos 1: Prime
//Pos 2: Timestamp
//Pos 3: Temporal precision
//Pos 4: Root primitive
function CreateQuininePrimeHolder(TargetIncomingXORMemory)
{
	try
	{
		
		var Temp = {
						//Pos 0: Prime length
						//Pos 1: Prime
						//Pos 2: Timestamp
						//Pos 3: Temporal precision
						//Pos 4: Root primitive
						FPTWorkerManager:null,
						XORMemory:CreateXORMemoryBigInt(5),
						DHUK:CreateDHUKEBI(),
						Initialise:function(IncomingXORMemory)
						{
							try
							{
								if(IncomingXORMemory != null)
								{
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,255n),0,255n);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,255n),1,255n);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(2,255n),2,255n);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(3,255n),3,255n);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(4,255n),4,255n);
								}
							}
							catch(err)
							{
								throw{name:"Initialise error",message:"Initialise, line number "+err.lineNumber+": "+err.message}
							}
						},
						GenerateDHKeyAgreement:function()
						{
							try
							{
								//Pos 0: timedate stamp (or other prime initialising variable)
								//Pos 1: target length of prime
								//Pos 2: prime number itself
								//Pos 3: root primitive
								var Temp = CreateXORMemoryBigInt(4);
								
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(2,254n),0,254n)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(0,254n),1,254n)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(1,254n),2,254n)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(4,254n),3,254n)
							
								return Temp;
							}
							catch(err)
							{
								throw{name:"GenerateDHKeyAgreement error",message:"GenerateDHKeyAgreement, line number "+err.lineNumber+": "+err.message}
							}
						},
						DisplayResponseCode:function(IncomingText)
						{
							try
							{
								var Target = document.getElementById("ResponseCode");
								var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
								Target[Type] = "";
								Target[Type] = IncomingText;
							}
							catch(err)
							{
								alert("ClearType: "+err.message);
							}
						},
						CallbackRootPrimitiveFunction:function(IncomingRootPrimitive)
						{
							try
							{
								alert("callback root prim");
								
								this.XORMemory.SetBigIntAt(IncomingRootPrimitive,4)
								this.DHUK.AcceptPrimeAndRootPrimitive(this.GenerateDHKeyAgreement());
								
								this.DHUK.GenerateSuitableSecret();
								this.DHUK.GenerateSecretAndSharedKey();
								
								var TempXOR = this.DHUK.GetSharedKeyAsXORMemory();
								
								TempXOR.AddBigInt(G_QuinineVersion);
								TempXOR.AddBigInt(1n);
								
								//Pos 0: IncomingSharedKey
								//Pos 1: IncomingIdenifier
								//Pos 2: Quinine version
								//Pos 3: Handshake stage
									//1 - initiation
									//2 - response
								
								var TempCompressor = CreateIntegerCompressor("£€",",");
								
								var MasqueValue = G_PRBIO.GetGeneratedBigInt();
								var ReturnString = TempCompressor.Compress(""+MasqueValue) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(0,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(1,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(2,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(3,MasqueValue));
								
								this.DisplayResponseCode(""+ReturnString);
								
								this.FPTWorkerManager.Destructor();
								this.FPTWorkerManager = null;
								
								alert("root prim finished");
							}
							catch(err)
							{
								throw{name:"CallbackRootPrimitiveFunction error",message:"CallbackRootPrimitiveFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
						CallbackPrimeFunction:function(IncomingPrime)
						{
							try
							{
								alert("callback prime function");
								
								if(this.FPTWorkerManager == null)
								{
									alert("root prim start");
								
									this.XORMemory.SetBigIntAt(IncomingPrime,1);
									
									this.FPTWorkerManager =  WorkerManager(WorkerFunction,G_Library.GetWebWorkerLibrary(),1);
									
									function Temp(KnownPrime)
									{
										return G_PGBI.FindRootPrimitiveBigInt(KnownPrime);
									}
									
									this.CallbackRootPrimitiveFunction(2n);
									
									//this.FPTWorkerManager.SetOnCompleteHook(this.CallbackRootPrimitiveFunction.bind(this));
									//this.FPTWorkerManager.ProcessWorkload(Temp,[[IncomingPrime]]);
									
									alert("root prim loaded");
									
								}
								else
								{
									throw{name:"CallbackPrimeFunction error",message:"Callback made for prime when root primitive hasn't finished for earlier prime."}
								}
									
							}
							catch(err)
							{
								throw{name:"CallbackPrimeFunction error",message:"CallbackPrimeFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
						
					};
		
		Temp.Initialise(TargetIncomingXORMemory);
		return Temp;
	}
	catch(err)
	{
		throw{name:"CreateQuininePrimeHolder error",message:"CreateQuininePrimeHolder, line number "+err.lineNumber+": "+err.message}
	}
}

var G_PrimeBeingGenerated = null;

function GeneratePrime(PrimeLengthElementID,TemporalElementID,TemporalPrecisionElementID)
{
	"use strict";
	try
	{
		if(G_PrimeBeingGenerated != null)
		{
			alert("Prime already currently being generated");
			return;
		}
		else
		{
			
			var PLEID = document.getElementById(PrimeLengthElementID);
			var TEID = document.getElementById(TemporalElementID);
			var TPEI = document.getElementById(TemporalPrecisionElementID);
			
			var ValidTimestamp = BigInt(FlipTimestamp(TEID.value,TPEI.value))
			var Length = parseInt(PLEID.value)
			var TemporalPrecision = parseInt(TPEI.value)

			//Pos 0: Prime length
			//Pos 1: Prime
			//Pos 2: Timestamp
			//Pos 3: Temporal precision
			var TempXORMemory = CreateXORMemoryBigInt(5)
			
			TempXORMemory.SetBigIntAt(BigInt(Length),0);
			TempXORMemory.SetBigIntAt(0n,1);
			TempXORMemory.SetBigIntAt(BigInt(ValidTimestamp),2);
			TempXORMemory.SetBigIntAt(BigInt(TemporalPrecision),3);
			TempXORMemory.SetBigIntAt(0n,4);
			
			G_PrimeBeingGenerated = CreateQuininePrimeHolder(TempXORMemory);
			
			FindNextNearestPrimeBigIntTargetLengthThreaded(ValidTimestamp,Length,G_PrimeBeingGenerated.CallbackPrimeFunction.bind(G_PrimeBeingGenerated));
			alert("Started Generating Prime");
		}
		
	}
	catch(err)
	{
		alert("GeneratePrime: "+err.message);
	}
	finally
	{
		PLEID = null;
		TEID = null;
		TPEI = null;
		
		ValidTimestamp = null;
		Length = null;
		TemporalPrecision = null;
		TempXORMemory = null;
	}
}
