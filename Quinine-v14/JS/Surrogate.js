/*

	Surrogate.js library [HollowPoint Fork]
	
	Version: 8.0
	Created: 21/11/2017
	Last Updated: 16/07/2021
	
	Credit(s):
		Mozilla Developer Network (surrogate bind function, surrogate indexOf) released under Mozilla license
		Paul Irish (performance.now Polyfill, date.now Polyfill) released under MIT
		Justin Johnson (object.id function)
	
	Purpose:
		Holds re-implementations of functionalities not necessarily found in all types of browser (cough Internet Explorer cough cough)
	
	Relies upon:
		JavaScriptLibraryLoader
		
	Notes:
		Code should not need to be modified or touched at any point, as this is code based on a public standardised subset
		It's recommended to encourage users to adopt newer browsers as these are only surrogates for older browser technology
	
	License(s):
		MIT (Paul Irish performance.now Polyfill)
		Mozilla License (surrogate bind, indexOf)
		CC-BY-SA (object.id function)
		Rest of the code is MIT
		
	Updates:
	
		9.1
			Added JSON surrogate for BigInt stringify
	
		9.0
			Overhauled Surrogate into an object in order to meet Web Worker compatibility requirements
	
		8.0
			Added object.id generator
			Added Blob compatibility support to aid Web Workers design
	
		7.1
			Cleaned up duplicate code (notably, Date.now surrogate) and modified the Paul Irish performance.now polyfill
	
		7.0
			Added performance.now polyfill support for older versions of IE (to help support randomisation)
	
		6.0
			Finally, after much aggro, added classList support for older versions of IE and newer versions of IE
			(I must have been insane when I did that)
	
		5.0
			Implemented date now surrogate (if missing from browsers)
	
		4.0
			Implemented contains surrogate (if missing from browsers)
		
		3.0
			Implemented .indexOf surrogate from Mozilla Developer Network public shim, modified to be readable
		
		2.0
			Imported .bind surrogate from CommonFunctions.js
	
		1.0
			Created Surrogate.js
	Todo:
		None
	
*/

function Surrogate()
{
	var Temp = {
					
					BindSurrogate:function()
					{
						//Code from the Mozilla Developer Network
						//Retweaked to be readable, uses tabs instead of spaces, code properly bracketed etc
						if(!Function.prototype.bind)
						{
							Function.prototype.bind = function(ObjectThis)
							{
								if(typeof this !== 'function')
								{
									throw new TypeError('Surrogate.js: Function.prototype.bind: function prototype not found, cannot construct/add bind.');
								}

								var ArrayArgs				= Array.prototype.slice.call(arguments, 1),
									FunctionToBind			= this,
									FunctionNoOperation		= function() {},
									FunctionBound			= function(){
																			return FunctionToBind.apply(
																										this instanceof FunctionNoOperation ? this : ObjectThis, ArrayArgs.concat(
																																													Array.prototype.slice.call(arguments)
																																													)
																										);
																		};

								if(this.prototype)
								{
									FunctionNoOperation.prototype = this.prototype; 
								}
								
								FunctionBound.prototype = new FunctionNoOperation();
								
								return FunctionBound;
							};
						}
					},
					IndexOfSurrogate:function()
					{
						//Code from the Mozilla Developer Network
						//Retweaked to be readable, uses tabs instead of spaces, code properly bracketed etc
						//Unnecessary comments removed (this should never need to be edited)
						if(!Array.prototype.indexOf)	
						{
							Array.prototype.indexOf = function(SubItem, Position)	
							{
								if(this == null)	
								{
									throw new TypeError("Array.prototype.indexOf() - can't convert `" + this + "` to object");
								}

								var	TempIndex = isFinite(Position) ? Math.floor(Position) : 0,
									ThisObject = this instanceof Object ? this : new Object(this),
									TempLength = isFinite(ThisObject.length) ? Math.floor(ThisObject.length) : 0;

								if(TempIndex >= TempLength)	
								{
									return -1;
								}

								if(TempIndex < 0)	
								{
									TempIndex = Math.max(TempLength + TempIndex, 0);
								} 
								
								if(SubItem === undefined)	
								{
									do	
									{
										if(TempIndex in ThisObject && ThisObject[TempIndex] === undefined)	
										{
											return TempIndex;
										}
									}
									while(++TempIndex < TempLength);
								}
								else	
								{
									do	
									{
										if(ThisObject[TempIndex] === SubItem)	
										{
											return TempIndex;
										}
									}
									while(++TempIndex < TempLength);
								}
								return -1;
							};
						}
					},
					ContainsSurrogate:function()
					{
						//Custom surrogate for contains if missing
						if(typeof String.prototype.contains === 'undefined')
						{
							String.prototype.contains = function(SubItem)
							{
								return this.indexOf(SubItem) != -1;
							};
						}
					},
					NowSurrogate:function()
					{					
						//Surrogate for IE8 and earlier
						if(!Date.now)
						{
							Date.now = function()
							{
								return new Date().getTime();
							}
						}	
					},
					ClassListSurrogate:function()
					{
						/*
							Microsoft IE extensibility project
							
							Because Microsoft is terrible(tm)
						*/

						if(!("classList" in document.documentElement))
						{
							var Temp = null;
							//We trick IE into instating getElementById, createElement, getElementByTagName
							//This uses some pretty complex insanity with JavaScript to get it to work
							//Notice that we reference the document.<functionname> in the bind call?
							//We're actually copying the original getElementById function code into Temp
							//Then we're binding the original code to our replacement function
							//So we end up calling the original code (via 'this')
							//Then we add our own code onto the end, so basically:
							//Copy old function
							//Create new function that references old function in the bind call (aka 'this')
							//Call 'this' in the new function, do our stuff on it, then return the result
							
							if(!!document.getElementById)
							{
								Temp = document.getElementById;
								document.getElementById = function(Tag)
								{
									var TempElement = this(Tag);
									addClassListToElement(TempElement);
									return TempElement;
								}.bind(Temp);
							}
							
							if(!!document.createElement)
							{
								Temp = document.createElement;
								document.createElement = function(Tag)
								{
									var TempElement = this(Tag);
									addClassListToElement(TempElement);
									return TempElement;
								}.bind(Temp);
							}
							
							if(!!document.getElementByTagName)
							{
								Temp = document.getElementByTagName;
								document.getElementByTagName = function(Tag)
								{
									var TempElement = this(Tag);
									addClassListToElement(TempElement);
									return TempElement;
								}.bind(Temp);
							}
						}
					},
					ReplaceAtSurrogate:function()
					{
						if(!String.prototype.replaceAt)
						{
							String.prototype.replaceAt=function(index, replacement)
							{
								return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
							}
						}
					},
					PerformanceSurrogate:function()
					{

						//Paul Irish performance.now polyfill (original code modified)
						if("performance" in window == false)
						{
							window.performance = {};
						}

						if("now" in window.performance == false)
						{
							var nowOffset = Date.now();

							if(performance.timing && performance.timing.navigationStart)
							{
								nowOffset = performance.timing.navigationStart
							}

							window.performance.now =	function now()
														{
															return Date.now() - nowOffset;
														}
						}
					},
					AddObjectIDSurrogate:function()
					{
						//Justin Johnson CC-BY-SA object.id code with formatting changes
						if(typeof Object.id == "undefined")
						{
							var id = 0;

							Object.id = function(IncomingObject){
																	if(typeof IncomingObject.__uniqueid == "undefined")
																	{
																		Object.defineProperty(IncomingObject, "__uniqueid", {
																																value: ++id,
																																enumerable: false,
																																writable: false
																															});
																	}

																	return IncomingObject.__uniqueid;
																};
						}
					},
					URLSurrogate:function()
					{
						window.URL = window.URL || window.webkitURL;
					},
					BigIntSurrogate:function()
					{
						if(!BigInt.prototype.toJSON)
						{
							BigInt.prototype.toJSON = function() { return this.toString()  }
						}
					},
					Initialise:function()
					{
						this.BindSurrogate();
						this.IndexOfSurrogate();
						this.ContainsSurrogate();
						this.NowSurrogate();
						this.ReplaceAtSurrogate();
						this.AddObjectIDSurrogate();
						this.BigIntSurrogate();
						
						if(!AmWebWorker())
						{
							this.ClassListSurrogate();
							this.PerformanceSurrogate();
							this.URLSurrogate();
						}
					}
				};
	
	Temp.Initialise();
	return Temp;
}

G_Library.Add({Type:"Function",Functor:Surrogate,IsWebWorkerCompatible:true});

G_Surrogate = Surrogate()

G_Library.Add({Type:"Variable",Variable:G_Surrogate,Constructor:Surrogate,VariableName:"G_Surrogate",IsWebWorkerCompatible:true});


//Some older browsers do not support get ElementById
//Based on code from javascript-coder, with formatting modifiers and an error statement included
function getElementByIdUniversal(ElementID)
{
	if(document.getElementById)
	{
		return document.getElementById(ElementID);
	}
	else if(document.all)
	{
		return window.document.all[ElementID];
	}
	else if(document.layers)
	{
		return window.document.layers[ElementID];
	}
	else
	{
		throw new TypeError("Surrogate.js: could not find a suitable 'getElementById' replacement!");
	}
}

G_Library.Add({Type:"Function",Functor:getElementByIdUniversal,IsWebWorkerCompatible:false});

function addClassListToElement(TargetElement)
{
	if(!!TargetElement && !TargetElement.classList)
	{
		TargetElement.classList = {
			contains:function(Value)
			{
				return !!~this.className.split(/\s+/).indexOf(Value);
			},
			replace:function(OldClass,NewClass)
			{
				if(this.className.length !== 0)
				{
					var Temp = this.className.split(/\s+/);
					
					for(var Iter = 0;Iter < Temp.length;Iter++)
					{
						if(Temp[Iter] === OldClass){Temp[Iter] = NewClass;}
					}
					this.className = Temp.join(" ");
				}
			},
			add:function()
			{
				var args = Array.prototype.slice.call(arguments),
					ContainsTest = this.className.split(/\s+/);
				
				for(var Iter = 0;Iter < args.length;Iter++)
				{
					if(ContainsTest.indexOf(args[Iter]) === -1)
					{
						if(this.className.length > 0)
						{
							this.className += " " + args[Iter];
						}
						else
						{
							this.className += args[Iter];
						}
					}
				}
			},
			remove:function()
			{
				var Temp = this.className.split(/\s+/);
				
				if(Temp.length > 0)
				{
					//We declare the arguments slice call late stage; there's no point slicing the arguments
					//If there's nothing in the className list to compare it to
					var args = Array.prototype.slice.call(arguments);
					for(var Iter = 0;Iter < args.length;Iter++)
					{
						for(var Iter2 = 0;Iter2 < Temp.length;Iter2++)
						{
							if(Temp[Iter2] === args[Iter]){Temp[Iter2] = "";}
						}
					}
					
					this.className = Temp.join(" ");
				}
			},
			toggle:function(CurrentClassName,OptionalForce)
			{
				if(OptionalForce !== true && OptionalForce !== false)
				{
					if(this.classList.contains(CurrentClassName))
					{
						this.classList.remove(CurrentClassName);
						return false;
					}
					else
					{
						this.classList.add(CurrentClassName);
						return true;
					}
				}
				else if(!!OptionalForce)
				{
					this.classList.add(CurrentClassName);
				}
				else
				{
					this.classList.remove(CurrentClassName);
				}
				
			}, 
			item:function(Iter)
			{
				 return this.className.split(/\s+/)[Iter] || null;
			}
		};
		
		TargetElement.classList.add = TargetElement.classList.add.bind(TargetElement);
		TargetElement.classList.remove = TargetElement.classList.remove.bind(TargetElement);
		TargetElement.classList.toggle = TargetElement.classList.toggle.bind(TargetElement);
		TargetElement.classList.contains = TargetElement.classList.contains.bind(TargetElement);
		TargetElement.classList.item = TargetElement.classList.item.bind(TargetElement);
		TargetElement.classList.replace = TargetElement.classList.replace.bind(TargetElement);
	}
}

G_Library.Add({Type:"Function",Functor:addClassListToElement,IsWebWorkerCompatible:true});

function CreateBlob(IncomingParts, IncomingProperties)
{
	var Parts = IncomingParts || [];
	var Properties = IncomingProperties || {};
	var LegacyMode = false;
	
	try
	{
		return new Blob(Parts, Properties);
	}
	catch(err)
	{
		if(err.name !== "TypeError" || AmWebWorker())
		{
			throw{name:"CreateBlob error",message:"CreateBlob: "+err.message}
		}
		
		LegacyMode = true;
	}
	
	try
	{
		if(LegacyMode)
		{
			BlobBuilder = window.BlobBuilder || window.MSBlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
			TempBuilder = new BlobBuilder();
			
			for(var Iter = 0; Iter < Parts.length; Iter++)
			{
				TempBuilder.append(Parts[Iter]);
			}
			
			return TempBuilder.getBlob(Properties.type);
		}
	}
	catch(err)
	{
		throw{name:"CreateBlob error",message:"CreateBlob: "+err.message}
	}
	
	return null;
}

G_Library.Add({Type:"Function",Functor:CreateBlob,IsWebWorkerCompatible:true});
