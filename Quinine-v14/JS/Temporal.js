/*
	Temporal.js
	
	Created: 27/06/2021
	Updated: 23/07/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Updates:
		
		
		2.0
			Patched infinite loop bug
			Updated pad zeroes to be more efficient
			Added GetFixedTime
			Changed LoadFixedTime to SetFixedTime for consistent naming conventions
			
		
		1.0
			Added library

*/

function TemporalStorageObject()
{
	"use strict";
	return {
				Year:0,
				Month:0,
				Day:0,
				Hour:0,
				Minute:0,
				Second:0,
				
				CopyFromTemporalStorageObject:function(IncomingTemporalStorageObject)
				{
					try
					{
						this.Year = IncomingTemporalStorageObject.Year + 0
						this.Month = IncomingTemporalStorageObject.Month + 0
						this.Day = IncomingTemporalStorageObject.Day + 0
						this.Hour = IncomingTemporalStorageObject.Hour + 0
						this.Minute = IncomingTemporalStorageObject.Minute + 0
						this.Second = IncomingTemporalStorageObject.Second + 0
					}
					catch(err)
					{
						throw{name:"CopyFromTemporalStorageObject error",message:"CopyFromTemporalStorageObject, line number "+err.lineNumber+": "+err.message}
					}
				},
				LoadFromDateObject:function(IncomingDateObject)
				{
					try
					{
						this.Year = IncomingDateObject.getUTCFullYear();
						this.Month = IncomingDateObject.getUTCMonth()+1;
						this.Day = IncomingDateObject.getUTCDate()
						this.Hour = IncomingDateObject.getUTCHours()
						this.Minute = IncomingDateObject.getUTCMinutes()
						this.Second = IncomingDateObject.getUTCSeconds()
					}
					catch(err)
					{
						throw{name:"LoadFromDateObject error",message:"LoadFromDateObject, line number "+err.lineNumber+": "+err.message}
					}
				},
				PadZeroes:function(IncomingString,PadLength)
				{
					try
					{
						
						if(IncomingString.length < PadLength)
						{
							return "0".repeat(PadLength - IncomingString.length) + IncomingString
						}
						
						return IncomingString;
					}
					catch(err)
					{
						throw{name:"PadZeroes error",message:"PadZeroes, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetYearString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Year,4);
					}
					catch(err)
					{
						throw{name:"GetYearString error",message:"GetYearString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetMonthString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Month,2);
					}
					catch(err)
					{
						throw{name:"GetMonthString error",message:"GetMonthString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetDayString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Day,2);
					}
					catch(err)
					{
						throw{name:"GetDayString error",message:"GetDayString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetHourString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Hour,2);
					}
					catch(err)
					{
						throw{name:"GetHourString error",message:"GetHourString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetMinuteString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Minute,2);
					}
					catch(err)
					{
						throw{name:"GetMinuteString error",message:"GetMinuteString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetSecondString:function()
				{
					try
					{
						return this.PadZeroes(""+this.Second,2);
					}
					catch(err)
					{
						throw{name:"GetSecondString error",message:"GetSecondString, line number "+err.lineNumber+": "+err.message}
					}
				},
				GetAsFormat:function(IncomingFormat)
				{
					try
					{
						var FormatDupe = ""+IncomingFormat
						
						var Temp = this.GetYearString()
						
						while(FormatDupe.indexOf("YYYY") > -1)
						{
							FormatDupe = FormatDupe.replace("YYYY",Temp)
						}
						
						Temp = this.GetMonthString()
						
						while(FormatDupe.indexOf("MM") > -1)
						{
							FormatDupe = FormatDupe.replace("MM",Temp)
						}
						
						Temp = this.GetDayString()
						
						while(FormatDupe.indexOf("DD") > -1)
						{
							FormatDupe = FormatDupe.replace("DD",Temp)
						}
						
						Temp = this.GetHourString()
						
						while(FormatDupe.indexOf("hh") > -1)
						{
							FormatDupe = FormatDupe.replace("hh",Temp)
						}
						
						Temp = this.GetMinuteString()
						
						while(FormatDupe.indexOf("mm") > -1)
						{
							FormatDupe = FormatDupe.replace("mm",Temp)
						}
						
						Temp = this.GetSecondString()
						
						while(FormatDupe.indexOf("ss") > -1)
						{
							FormatDupe = FormatDupe.replace("ss",Temp)
						}
						
						return FormatDupe;
					}
					catch(err)
					{
						throw{name:"GetAsFormat error",message:"GetAsFormat, line number "+err.lineNumber+": "+err.message}
					}
					finally
					{
						Temp = null;
					}
				},
				SetAsFormat(IncomingString,IncomingFormatString)
				{
					try
					{
						if(IncomingString.length != IncomingFormatString.length)
						{
							throw{name:"SetAsFormat error",message:"IncomingString length does not match IncomingFormatString length"}
						}
						
						var TargetString = ""
						var TempPos = 0
						
						var TempTSO = TemporalStorageObject()
						
						TargetString = "YYYY"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Year = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						TargetString = "MM"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Month = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						TargetString = "DD"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Day = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						TargetString = "hh"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Hour = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						TargetString = "mm"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Minute = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						TargetString = "ss"
						TempPos = IncomingFormatString.indexOf(TargetString);
						
						if(TempPos > -1)
						{
							TempTSO.Second = parseInt(IncomingString.substring(TempPos,TempPos+TargetString.length))
						}
						
						this.CopyFromTemporalStorageObject(TempTSO)
					}
					catch(err)
					{
						throw{name:"SetAsFormat error",message:"SetAsFormat, line number "+err.lineNumber+": "+err.message}
					}
					finally
					{
						TargetString = null;
						TempPos = null;
						TempTSO = null;
					}
				}
			}
}

G_Library.Add({Type:"Function",Functor:TemporalStorageObject,IsWebWorkerCompatible:true});

function CreateTemporalObject()
{
	"use strict";
	try
	{
		return {
					TSO:TemporalStorageObject(),
					LoadCurrentTime:function()
					{
						try
						{
							this.TSO.LoadFromDateObject(new Date());
						}
						catch(err)
						{
							throw{name:"LoadCurrentTime error",message:"LoadCurrentTime, line number "+err.lineNumber+": "+err.message}
						}
					},
					SetFixedTime:function(IncomingString,IncomingFormatString)
					{
						try
						{
							this.TSO.SetAsFormat(IncomingString,IncomingFormatString);
						}
						catch(err)
						{
							throw{name:"SetFixedTime error",message:"SetFixedTime, line number "+err.lineNumber+": "+err.message}
						}
					},
					GetFixedTime:function(IncomingFormatString)
					{
						try
						{
							return this.TSO.GetAsFormat(IncomingFormatString);
						}
						catch(err)
						{
							throw{name:"GetFixedTime error",message:"GetFixedTime, line number "+err.lineNumber+": "+err.message}
						}
					}
				};
	}
	catch(err)
	{
		throw{name:"CreateTemporalObject error",message:"CreateTemporalObject, line number "+err.lineNumber+": "+err.message}
	}
}


G_Library.Add({Type:"Function",Functor:CreateTemporalObject,IsWebWorkerCompatible:true});
