/*
	DiffieHellmanUpgradedKeyExchangeBigInt.js
	
	Created: 28/06/2021
	Updated: 29/12/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		XORMemoryBigInt.js
		PrimeBigInt.js
		Temporal.js
	
	Updates:
		
		2.1
			Corrected Initialise error message to mention line number
			
		
		2.0
			Added CreateDHUKEBI
			Added Initialise
			Added GeneratePrimeAndRootPrimitive
			Added GenerateSuitableSecret
			Added GenerateSecretAndSharedKey
			Added GenerateSharedSecret
			Added GetSharedKeyAsXORMemory
			Added TestDHUKEBI
			Patched "GetBigAt" typos to GetBigIntAt
		
		1.0
			Added library

*/

function CreateDHUKEBI(TargetXORMemory)
{
	"use strict";
	
	try
	{
		var Temp = {
					//All of these variables **must** be BigInts
					//Pos 0: ModPrime
					//Pos 1: ModPrime length
					//Pos 2: BaseRootPrimitive
					//Pos 3: SecretKey
					//Pos 4: SharedKey
					//Pos 5: Prime initialising variable (EG Timedate)
					//Pos 6: SharedSecret
					//Pos 7: OtherIdentifier
					//Pos 8: OwnIdentifier
					XORMemory:CreateXORMemoryBigInt(9),
					InternalMasqueValue:G_PRBIO.GetGeneratedBigInt(),
					
					//IncomingXORMemory must contain two pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime [approximate]
					Initialise:function(IncomingXORMemory)
					{
						try
						{
							if(typeof(IncomingXORMemory) === "object")
							{
								this.GeneratePrimeAndRootPrimitive(IncomingXORMemory)
								this.GenerateSuitableSecret()
								this.GenerateSecretAndSharedKey()
							}
							
							this.XORMemory.SetBigIntAt(G_PRBIO.GetGeneratedBigInt() ^ G_PRBIO.GetGeneratedBigInt(),8)
						}
						catch(err)
						{
							throw{name:"Initialise (CreateDHUKEBI) error",message:"Initialise (CreateDHUKEBI), line number "+err.lineNumber+": "+err.message}
						}
					},
					//IncomingXORMemory must contain four pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime
					//Pos 2: prime number itself
					//Pos 3: root primitive
					AcceptPrimeAndRootPrimitive:function(IncomingXORMemory)
					{
						console.log("AcceptPrimeAndRootPrimitive");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"AcceptPrimeAndGenerateRootPrimitive error",message:"IncomingXORMemory is not an object."}
							}
							
							//Pos 5: Prime initialising variable
							//Pos 1: ModPrime length
							//Pos 0: ModPrime
							//Pos 2: BaseRootPrimitive
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,this.InternalMasqueValue),5,this.InternalMasqueValue) //Prime initialising variable
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,this.InternalMasqueValue),1,this.InternalMasqueValue) //modulus prime len
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(2,this.InternalMasqueValue),0,this.InternalMasqueValue) //modulus prime
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(3,this.InternalMasqueValue),2,this.InternalMasqueValue) //BaseRootPrimitive
						}
						catch(err)
						{
							throw{name:"AcceptPrimeAndGenerateRootPrimitive error",message:"AcceptPrimeAndGenerateRootPrimitive, "+err.lineNumber+": "+err.message}
						}
					},
					//IncomingXORMemory must contain two pieces of info
					//Pos 0: timedate stamp (or other prime initialising variable)
					//Pos 1: target length of prime [approximate]
					GeneratePrimeAndRootPrimitive:function(IncomingXORMemory)
					{
						"use strict";
						console.log("GeneratePrimeAndRootPrimitive");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"GeneratePrimeAndRootPrimitive error",message:"IncomingXORMemory is not an object."}
							}
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 5: Prime initialising variable
							
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,this.InternalMasqueValue),5,this.InternalMasqueValue) //PIV
							this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,this.InternalMasqueValue),1,this.InternalMasqueValue) //MP len
							this.XORMemory.SetBigIntAt(G_PGBI.FindNextNearestPrimeBigIntTargetLength(IncomingXORMemory.GetBigIntAt(0),IncomingXORMemory.GetBigIntAt(1)),0) //MP
							
							//Was FindRootPrimitiveBigInt
							this.XORMemory.SetBigIntAt(G_PGBI.FindRootPrimitivePrimeBigInt(this.XORMemory.GetBigIntAt(0)),2) //BRP
						}
						catch(err)
						{
							throw{name:"GeneratePrimeAndRootPrimitive error",message:"GeneratePrimeAndRootPrimitive, "+err.lineNumber+": "+err.message}
						}
					},
					//Must call GeneratePrimeAndRootPrimitive before calling this
					GenerateSuitableSecret:function()
					{
						"use strict";
						console.log("GenerateSuitableSecret");
						
						try
						{
							//Pos 3: SecretKey
							this.XORMemory.SetBigIntAt(G_PRBIO.GetGeneratedBigInt(),3);
							
							//This should generate a batch of individual ints into XOR int memory storage
							//And then append those numbers together to form a BigInt
							
							
							//Pos 1: ModPrime length
							while(this.XORMemory.GetBigIntAt(3).toString().length < (this.XORMemory.GetBigIntAt(1).toString().length+1) )
							{
								//Pos 3: SecretKey
								this.XORMemory.SetBigIntAt(this.XORMemory.GetBigIntAt(3) ^ G_PRBIO.GetGeneratedBigInt(),3)
							};
							
							return;
						}
						catch(err)
						{
							throw{name:"GenerateSuitableSecret error",message:"GenerateSuitableSecret, "+err.lineNumber+": "+err.message}
						}
					},
					//GenerateSuitableSecret must be called before this
					GenerateSecretAndSharedKey:function()
					{
						"use strict";
						console.log("GenerateSecretAndSharedKey");
						try
						{
							//Pos 0: ModPrime
							//Pos 2: BaseRootPrimitive
							//Pos 3: SecretKey
							//Pos 4: SharedKey
							
							//Base^SecretKey mod ModPrime
							this.XORMemory.SetBigIntAt(G_PGBI.Pow3BigInt(this.XORMemory.GetBigIntAt(2), this.XORMemory.GetBigIntAt(3), this.XORMemory.GetBigIntAt(0)),4)
						}
						catch(err)
						{
							throw{name:"GenerateSecretAndSharedKey error",message:"GenerateSecretAndSharedKey, "+err.lineNumber+": "+err.message}
						}
					},
					//GenerateSecretAndSharedKey must be called before this
					//IncomingXORMemory must contain two pieces of info
					//Pos 0: IncomingSharedKey
					//Pos 1: IncomingIdenifier
					GenerateSharedSecret:function(IncomingXORMemory,OptionalDemasqueValue)
					{
						"use strict";
						console.log("GenerateSharedSecret");
						try
						{
							if(typeof(IncomingXORMemory) !== "object")
							{
								throw{name:"GenerateSharedSecret error",message:"IncomingXORMemory is not an object."};
							}
							
							//Pos 0: ModPrime
							//Pos 1: ModPrime length
							//Pos 2: BaseRootPrimitive
							//Pos 3: SecretKey
							//Pos 4: SharedKey
							//Pos 5: Prime initialising variable (EG Timedate)
							//Pos 6: SharedSecret
							//Pos 7: OtherIdentifier
							
							//SharedSecret = IncomingSharedKey ^ SecretKey % ModPrime
							
							if(OptionalDemasqueValue === undefined)
							{
								this.XORMemory.SetBigIntAt(G_PGBI.Pow3BigInt(IncomingXORMemory.GetBigIntAt(0),this.XORMemory.GetBigIntAt(3),this.XORMemory.GetBigIntAt(0)),6)
								this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1),7);
							}
							else
							{
								this.XORMemory.SetBigIntAt(G_PGBI.Pow3BigInt(IncomingXORMemory.GetBigIntAt(0,OptionalDemasqueValue),this.XORMemory.GetBigIntAt(3),this.XORMemory.GetBigIntAt(0)),6)
								this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1),7,OptionalDemasqueValue);
							}
						}
						catch(err)
						{
							throw{name:"GenerateSharedSecret error",message:"GenerateSharedSecret, "+err.lineNumber+": "+err.message}
						}
						
					},
					
					//GenerateSecretAndSharedKey must be called before this
					//Pos 0: IncomingSharedKey
					//Pos 1: IncomingIdenifier
					GetSharedKeyAsXORMemory:function()
					{
						try
						{
							var Temp = CreateXORMemoryBigInt(2);
							
							//Pos 4: SharedKey
							Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(4,this.InternalMasqueValue),0,this.InternalMasqueValue);
							//Pos 8: OurIdentifier
							Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(8,this.InternalMasqueValue),1,this.InternalMasqueValue);
							return Temp;
						}
						catch(err)
						{
							throw{name:"GetSharedKeyAsXORMemory error",message:"GetSharedKeyAsXORMemory, "+err.lineNumber+": "+err.message}
						}
					},
					ExportSharedKeyAsJSON:function()
					{
						try
						{
							return this.GetSharedKeyAsXORMemory();
						}
						catch(err)
						{
							throw{name:"ExportSharedKeyAsJSON error",message:"ExportSharedKeyAsJSON, "+err.lineNumber+": "+err.message}
						}
					}
					
				};
				
			Temp.Initialise(TargetXORMemory);
			
			return Temp;
	}
	catch(err)
	{
		throw{name:"CreateDHUKEBI error",message:"CreateDHUKEBI, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:CreateDHUKEBI,IsWebWorkerCompatible:true});

function TestDHUKEBI()
{
	try
	{
		//Pos 0: timedate stamp (or other prime initialising variable)
		//Pos 1: target length of prime
		
		var TDP1 = CreateXORMemoryBigInt(2)
		var TDP2 = CreateXORMemoryBigInt(2)
		
		//ssmmhhDDMMYYYY
		//20202020102020
		//10 length prime
		
		TDP1.SetBigIntAt(20211227202315n,0);
		TDP2.SetBigIntAt(20211227202315n,0);
		
		TDP1.SetBigIntAt(10n,1);
		TDP2.SetBigIntAt(10n,1);
		
		
		console.log("Key A start");
		var KeyA = CreateDHUKEBI(TDP1);
		console.log("Key A finished.\r\nKey B start");
		var KeyB = CreateDHUKEBI(TDP2);
		console.log("Key B finished");
		
		console.log("KeyA Prime is: "+KeyA.XORMemory.GetBigIntAt(0));
		console.log("KeyB Prime is: "+KeyB.XORMemory.GetBigIntAt(0));
		
		//Pos 0: IncomingSharedKey
		//Pos 1: IncomingIdenifier
		console.log("Client A start");
		var ClientA = KeyA.GetSharedKeyAsXORMemory()
		console.log("Client A finished. Client B start");
		var ClientB = KeyB.GetSharedKeyAsXORMemory()
		console.log("Client B finished");
		
		var FakeIDA = 2323232324n;
		var FakeIDB = 2323232321n;
		
		ClientA.SetBigIntAt(FakeIDA,1);
		ClientB.SetBigIntAt(FakeIDB,1);
		
		KeyA.GenerateSharedSecret(ClientB);
		KeyB.GenerateSharedSecret(ClientA);
	
	
		return KeyA.XORMemory.GetBigIntAt(6) == KeyB.XORMemory.GetBigIntAt(6);
		//alert("SharedSecret for A is: "+KeyA.XORMemory.GetBigIntAt(6)+". SharedSecret for B is: "+KeyB.XORMemory.GetBigIntAt(6));
	}
	catch(err)
	{
		throw{name:"TestDHUKEBI error",message:"TestDHUKEBI, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:TestDHUKEBI,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"DiffieHellmanUpgradedKeyExchangeBigInt.js"});
