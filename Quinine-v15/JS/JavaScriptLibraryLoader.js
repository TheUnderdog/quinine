/*
	JavaScriptLibraryLoader.js
	
	Version: 1.0
	Created: 06/07/2021
	Updated: 11/07/2021
	
	Depends upon:
		
		None
	
	Licence(s):
		
		Rest of code: MIT
	
	Updates:
	
		3.0
			Modified to use the dynamic data object model to avoid legacy feature creep in function design
	
		2.0
			Modified LibraryLoaderObject to store WebWorker compatibility (so specific parts can avoid being loaded)
		
		1.0
			Added library

*/


/*

	Note: for backwards compatibility interpreters should simply ignore any unrecognised types rather than erroring
	Object Datatype (Extensible):
		
		"Header"
			Indicates the end of a specified library file, EG JavaScript.js
			{
				Type:"Header",
				Filename:"Library.js"
			}
		 
		"LibraryLoader"
			Indicates a special case scenario where the LibraryLoader must be declared in a specific order
			Order is: Function definition, variable of library loader, libraryloader call
			{
				Type:"LibraryLoader",
				VariableName:"VariableName", //Variable name in text
				Constructor:Functor, //Constructor for the LibraryLoader
				IsWebWorkerCompatible:false //Whether it can be safely called/used by a Web Worker
			}
		
		"Comment"
			Indicates this is a comment
			{
				Type:"Comment",
				Text:"Comment goes here"
			}
		
		"Function"
			Indicates this is a function
			{
				Type:"Function",
				Functor:Functor, //The actual functor
				IsWebWorkerCompatible:false, //Whether it can be safely called/used by a Web Worker. Default is false
			}
		
		"Variable"
			Indicates this is a variable (including functions assigned to variables)
			{
				Type:"Variable",
				Variable:undefined, //The current variable
				Constructor:undefined, //The original constructor (if applicable) used to make it
				VariableName:"VariableName" //The variable name in the form of text
			}
		
		
		"Literal"
			Indicates the following code should be printed literally with no attempt at interpretation
			{
				Type:"Literal",
				Text:"Literal code goes here"
			}
*/

function LibraryLoaderObject(TargetOwnName)
{
	"use strict";
	try
	{
	
		var Temp = {
						OwnName:"",
						ChronologicalList:[],
						WebWorkerLibrary:null,
						IsFunction:function(T)
						{
							return typeof(T) === "function";
						},
						IsObject:function(T)
						{
							return (typeof T === "object");
						},
						
						Initialise:function(IncomingOwnName)
						{
							this.OwnName = IncomingOwnName;
						},
						GetFunctionName:function(IncomingFunctor)
						{
							var TargetFunct = "function ";
							var FunctorString = ""+IncomingFunctor;
							return FunctorString.substring(FunctorString.indexOf(TargetFunct)+TargetFunct.length,FunctorString.indexOf("("))
						},
						GenerateDataObjectQuine:function(IncomingDataObject)
						{
							var ObjectText = "{"
							
							for(var Key in IncomingDataObject)
							{
								ObjectText += ""+Key+":"
								
								if(Key == "Variable")
								{
									ObjectText += IncomingDataObject.VariableName + ",";
								}
								else if(Key == "VariableName")
								{
									ObjectText += "\""+IncomingDataObject.VariableName+"\",";
								}
								else if(Key == "Filename")
								{
									ObjectText += "\""+IncomingDataObject.Filename+"\",";
								}
								else if(IncomingDataObject[Key] === undefined)
								{
									console.log("JavaScriptLibraryLoader: GenerateDataObjectQuine: Key is undefined: "+Key)
									ObjectText += "undefined,";
								}
								else if(typeof(IncomingDataObject[Key]) === "boolean")
								{
									ObjectText += IncomingDataObject[Key] + ","
								}
								else if(typeof(IncomingDataObject[Key]) === "string")
								{
									ObjectText += "\"" + IncomingDataObject[Key] + "\","
								}
								else if(typeof(IncomingDataObject[Key]) === "function")
								{
									ObjectText += this.GetFunctionName(IncomingDataObject[Key]) + ","
								}
								else
								{
									console.log("JavaScriptLibraryLoader: GenerateDataObjectQuine: Could not identify Key: "+Key)
								}
							}
							
							if(ObjectText.indexOf(",") > -1)
							{
								//Remove the trailing comma
								ObjectText = ObjectText.substring(0,ObjectText.length-1);
							}
							
							return ObjectText + "}"
						},
						GenerateQuine:function(IncomingDataObject)
						{
							return "\n"+this.OwnName+".Add("+this.GenerateDataObjectQuine(IncomingDataObject)+");\n";
						},
						GenerateSelfDeclaration:function()
						{
							return this.ChronologicalList[0]+"\r\n\r\nvar "+this.OwnName+" = "+GetFunctionName(this.ChronologicalList[0])+"(\""+this.OwnName+"\");\r\n\r\n";
						},
						GenerateObjectDeclaration:function(IncomingDataObject)
						{
							try
							{
								if(IncomingDataObject.Type == "Header")
								{
									return ""; //Currently not supported, will need to be added
								}
								else if(IncomingDataObject.Type == "Comment")
								{
									return ""; //Currently not supported, will need to be added
								}
								else if(IncomingDataObject.Type == "LibraryLoader")
								{
									//Constructor
									//Variable
									//Quine
									
									var LL = ""+IncomingDataObject.Constructor+"\r\n\r\n";
									LL += "var "+IncomingDataObject.VariableName+" = "+this.GetFunctionName(IncomingDataObject.Constructor)+"();\r\n\r\n";
									return LL + ""+this.OwnName+".Add("+this.GenerateDataObjectQuine(IncomingDataObject)+");\r\n\r\n"
								}
								else if(IncomingDataObject.Type == "Function")
								{
									return ""+IncomingDataObject.Functor;
								}
								else if(IncomingDataObject.Type == "Variable")
								{
									if(IncomingDataObject.Constructor != undefined)
									{
										if(typeof(IncomingDataObject.Constructor) == "function")
										{
											return "var "+IncomingDataObject.VariableName+" = "+this.GetFunctionName(IncomingDataObject.Constructor)+"();\r\n\r\n";
										}
										else if(typeof(IncomingDataObject.Constructor) == "string")
										{
											return "var "+IncomingDataObject.VariableName+" = "+IncomingDataObject.Constructor+";\r\n\r\n";
										}
										else
										{
											console.log("JavaScriptLibraryLoader: GenerateObjectDeclaration: Variable if statement: Constructor section: Constructor ["+IncomingDataObject.Constructor+"] neither a function nor a string. Is: "+typeof(IncomingDataObject.Constructor)+" instead.")
										}
									}
									else
									{
										return "var "+IncomingDataObject.VariableName+" = "+IncomingDataObject.Variable+";\r\n\r\n";
									}
								}
								else if(IncomingDataObject.Type == "Literal")
								{
									return ""+IncomingDataObject.Text;
								}
								
							}
							catch(err)
							{
								throw{name:"GenerateObjectDeclaration error",message:"GenerateObjectDeclaration, line number "+err.lineNumber+": "+err.message}
							}
						},
						GenerateWebWorkerLibrary:function()
						{
							try
							{
								var LibraryText = "";
								
								for(var Iter = 0;Iter < this.ChronologicalList.length;++Iter)
								{
									//Needs an omission feature for non-compatible code
									
									LibraryText += this.GenerateObjectDeclaration(this.ChronologicalList[Iter]);
									LibraryText += this.GenerateQuine(this.ChronologicalList[Iter]);
								}
								
								return LibraryText;
							}
							catch(err)
							{
								throw{name:"GenerateWebWorkerLibrary error",message:"GenerateWebWorkerLibrary, line number "+err.lineNumber+": "+err.message}
							}
						},
						GetWebWorkerLibrary:function()
						{
							try
							{
								if(this.WebWorkerLibrary == null)
								{
									this.WebWorkerLibrary = this.GenerateWebWorkerLibrary();
								}
								
								return this.WebWorkerLibrary;
							}
							catch(err)
							{
								throw{name:"GetWebWorkerLibrary error",message:"GetWebWorkerLibrary, line number "+err.lineNumber+": "+err.message}
							}
						},
						FlushUpdates:function()
						{
							try
							{
								if(this.WebWorkerLibrary != null)
								{
									this.WebWorkerLibrary = this.GenerateWebWorkerLibrary();
								}
							}
							catch(err)
							{
								throw{name:"FlushUpdates error",message:"FlushUpdates, line number "+err.lineNumber+": "+err.message}
							}
						},
						Add:function(IncomingDataObject)
						{
							try
							{
								this.ChronologicalList.push(IncomingDataObject);
								this.FlushUpdates();
							}
							catch(err)
							{
								throw{name:"Add error",message:"Add, line number "+err.lineNumber+": "+err.message}
							}
						},
						GetList:function()
						{
							return this.ChronologicalList;
						}
				
					};
					
		Temp.Initialise(TargetOwnName);
		return Temp;
	}
	catch(err)
	{
		throw{name:"LibraryLoaderObject error",message:"LibraryLoaderObject, line number "+err.lineNumber+": "+err.message}
	}
}

var G_Library = LibraryLoaderObject("G_Library");

G_Library.Add({Type:"LibraryLoader",VariableName:"G_Library",Constructor:LibraryLoaderObject,IsWebWorkerCompatible:true});


function AmWebWorker(T)
{
	"use strict";
	
	if(typeof WorkerGlobalScope !== 'undefined' && self instanceof WorkerGlobalScope)
	{
		return true;
	}
	else
	{
		return false;
	}
}

G_Library.Add({Type:"Function",Functor:AmWebWorker,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"JavaScriptLibraryLoader.js"});
