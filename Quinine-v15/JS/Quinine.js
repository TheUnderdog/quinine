/*
	Quinine.js
	
	Created: 22/07/2021
	Updated: 29/12/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Detector.js
		Temporal.js
		lz-string.js
		Threaded.js
		PrimeBigIntThreaded.js
		IntegerCompression.js
		DiffieHellmanUpgradedKeyExchangeBigInt.js
	
	Updates:
	
		2.0
			Added G_AdditionalCompressorChars to standise between functions and class
			Added G_RemovedCompressorChars to standardise between functions and class
			
		
		1.0
			Added library
	
	Todo:
		Merge functions as far as possible into class
		
		
		

*/

var G_QuinineVersion = 15n;
var G_TimestampLength = 14;
var G_AdditionalCompressorChars = "£€";
var G_RemovedCompressorChars = ", ";

function ClearType(TargetID)
{
	try
	{
		var Target = document.getElementById(TargetID);
		var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
		Target[Type] = "";
	}
	catch(err)
	{
		alert("ClearType: "+err.message);
	}
}

function ClearDecrypted()	{ ClearType("unencrypted");	}
function ClearEncrypted()	{ ClearType("encrypted");	}

function TimestampOnChange(IncomingElement)
{
	try
	{
		var Temp = IncomingElement.value;
		IncomingElement.value = Temp + "0".repeat(G_TimestampLength - Temp.length)
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function TimestampOnLoad(IncomingID)
{
	try
	{
		var IncomingElement = document.getElementById(IncomingID);
		var Temp = CreateTemporalObject();
		Temp.LoadCurrentTime();
		IncomingElement.value =	Temp.GetFixedTime("YYYYMMDDhhmmss");
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function PrimeLengthOnChange(PrimeLengthElement,PrimeBitsElementID,PrimeEstTimeID)
{
	try
	{
		var PBEI = document.getElementById(PrimeBitsElementID);
		var PETI = document.getElementById(PrimeEstTimeID);
		
		if(PrimeLengthElement.value == "")
		{
			PrimeLengthElement.value = 1;
		}
		
		var TempLen = Math.abs(parseInt(PrimeLengthElement.value));
		
		PrimeLengthElement.value = TempLen;
		
		PBEI.value = TempLen * 8;
		PETI.value = (TempLen / 100) / 2
		
	}
	catch(err)
	{
		alert("PrimeLengthOnChange: "+err.message);
	}
	finally
	{
		PBEI = null;
		PETI = null;
		TempLen = null;
	}
}

function FlipTimestamp(IncomingTimestamp,TemporalPrecision)
{
	try
	{
		var TempTO = CreateTemporalObject();
		var Temp = ""+IncomingTimestamp;
		
		if(Temp.length > G_TimestampLength)
		{
			Temp = Temp.substring(0,G_TimestampLength);
		}
		
		var TempPrecision = TemporalPrecision
		
		if(Temp.length > TempPrecision)
		{
			Temp = (TempPrecision > 0) ? Temp.substring(0,TempPrecision) : "";
		}
		
		if(Temp.length < G_TimestampLength)
		{
			Temp = Temp + "0".repeat(G_TimestampLength - Temp.length)
		}
		
		TempTO.SetFixedTime(Temp,"YYYYMMDDhhmmss");
		
		var NewTimestamp = TempTO.GetFixedTime("ssmmhhDDMMYYYY");
		
		while(NewTimestamp.charAt(0) == "0")
		{
			NewTimestamp = NewTimestamp.substring(1);
		}
		
		if(NewTimestamp == "")
		{
			NewTimestamp = "0";
		}
		
		return NewTimestamp;
	}
	catch(err)
	{
		throw{name:"FlipTimestamp error",message:"FlipTimestamp, line number "+err.lineNumber+": "+err.message}
	}
}

//Pos 0: Prime length
//Pos 1: Prime
//Pos 2: Timestamp
//Pos 3: Temporal precision
//Pos 4: Root primitive
function CreateQuininePrimeHolder(TargetIncomingXORMemory)
{
	try
	{
		
		var Temp = {
						
						FPTWorkerManager:null,
						
						//Pos 0: Prime length
						//Pos 1: Prime
						//Pos 2: Timestamp
						//Pos 3: Temporal precision
						//Pos 4: Root primitive
						XORMemory:CreateXORMemoryBigInt(5),
						DHUK:CreateDHUKEBI(),
						PrimeGenerated:false,
						InternalMasqueValue:G_PRBIO.GetGeneratedBigInt(),
						Initialise:function(IncomingXORMemory)
						{
							try
							{
								if(IncomingXORMemory != null)
								{
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,this.InternalMasqueValue),0,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,this.InternalMasqueValue),1,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(2,this.InternalMasqueValue),2,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(3,this.InternalMasqueValue),3,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(4,this.InternalMasqueValue),4,this.InternalMasqueValue);
								}
							}
							catch(err)
							{
								throw{name:"Initialise error",message:"Initialise, line number "+err.lineNumber+": "+err.message}
							}
						},
						//Generates the key required to perform an agreement
						GenerateDHKeyAgreement:function()
						{
							try
							{
								//Pos 0: timedate stamp (or other prime initialising variable)
								//Pos 1: target length of prime
								//Pos 2: prime number itself
								//Pos 3: root primitive
								var Temp = CreateXORMemoryBigInt(4);
								
								//Pos 2: Timestamp
								//Pos 0: Prime length
								//Pos 1: Prime
								//Pos 4: Root primitive
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(2,this.InternalMasqueValue),0,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(0,this.InternalMasqueValue),1,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(1,this.InternalMasqueValue),2,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(4,this.InternalMasqueValue),3,this.InternalMasqueValue)
							
								return Temp;
							}
							catch(err)
							{
								throw{name:"GenerateDHKeyAgreement error",message:"GenerateDHKeyAgreement, line number "+err.lineNumber+": "+err.message}
							}
						},
						DisplayResponseCode:function(IncomingText)
						{
							try
							{
								var Target = document.getElementById("ResponseCode");
								var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
								Target[Type] = "";
								Target[Type] = IncomingText;
							}
							catch(err)
							{
								alert("ClearType: "+err.message);
							}
						},
						CallbackRootPrimitiveFunction:function(IncomingRootPrimitive)
						{
							try
							{
								//alert("callback root prim");
								
								this.XORMemory.SetBigIntAt(IncomingRootPrimitive,4);
								this.DHUK.AcceptPrimeAndRootPrimitive(this.GenerateDHKeyAgreement());
								
								this.DHUK.GenerateSuitableSecret();
								this.DHUK.GenerateSecretAndSharedKey();
								
								var TempXOR = this.DHUK.GetSharedKeyAsXORMemory();
								
								TempXOR.AddBigInt(G_QuinineVersion);
								TempXOR.AddBigInt(1n);
								
								//Pos 0: IncomingSharedKey
								//Pos 1: IncomingIdenifier
								//Pos 2: Quinine version
								//Pos 3: Handshake stage
									//1 - initiation
								
								var MasqueValue = G_PRBIO.GetGeneratedBigInt();
								
								var TempCompressor = CreateIntegerCompressor(G_AdditionalCompressorChars,G_RemovedCompressorChars);
								
								var ReturnString = TempCompressor.Compress(""+MasqueValue) + ",";
								
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(0,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(1,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(2,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(3,MasqueValue));
								
								this.DisplayResponseCode(""+ReturnString);
								this.PrimeGenerated = true;
								this.FPTWorkerManager.Destructor();
								this.FPTWorkerManager = null;
							}
							catch(err)
							{
								throw{name:"CallbackRootPrimitiveFunction error",message:"CallbackRootPrimitiveFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
						CallbackPrimeFunction:function(IncomingPrime)
						{
							try
							{
								
								if(this.FPTWorkerManager == null)
								{								
									this.XORMemory.SetBigIntAt(IncomingPrime,1);
									
									this.FPTWorkerManager =  WorkerManager(WorkerFunction,G_Library.GetWebWorkerLibrary(),1);
									
									function Temp(KnownPrime)
									{
										//Was FindRootPrimitiveBigInt
										return G_PGBI.FindRootPrimitivePrimeBigInt(KnownPrime);
									}
									
									this.CallbackRootPrimitiveFunction(2n);
									
									//this.FPTWorkerManager.SetOnCompleteHook(this.CallbackRootPrimitiveFunction.bind(this));
									//this.FPTWorkerManager.ProcessWorkload(Temp,[[IncomingPrime]]);
									
									//alert("root prim loaded");
									
								}
								else
								{
									throw{name:"CallbackPrimeFunction error",message:"Callback made for prime when root primitive hasn't finished for earlier prime."}
								}
									
							}
							catch(err)
							{
								throw{name:"CallbackPrimeFunction error",message:"CallbackPrimeFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
					};
		
		Temp.Initialise(TargetIncomingXORMemory);
		return Temp;
	}
	catch(err)
	{
		throw{name:"CreateQuininePrimeHolder error",message:"CreateQuininePrimeHolder, line number "+err.lineNumber+": "+err.message}
	}
}

var G_PrimeBeingGenerated = null;

function GeneratePrime(PrimeLengthElementID,TemporalElementID,TemporalPrecisionElementID)
{
	"use strict";
	try
	{
		if(G_PrimeBeingGenerated != null)
		{
			alert("Prime already currently being generated (reload page if you want to stop and generate another).");
			return;
		}
		else
		{
			
			var PLEID = document.getElementById(PrimeLengthElementID);
			var TEID = document.getElementById(TemporalElementID);
			var TPEI = document.getElementById(TemporalPrecisionElementID);
			
			var ValidTimestamp = BigInt(FlipTimestamp(TEID.value,TPEI.value))
			var Length = BigInt(PLEID.value)
			var TemporalPrecision = BigInt(TPEI.value)

			//Pos 0: Prime length
			//Pos 1: Prime
			//Pos 2: Timestamp
			//Pos 3: Temporal precision
			//Pos 4: Root Primitive
			var TempXORMemory = CreateXORMemoryBigInt(5)
			
			TempXORMemory.SetBigIntAt(Length,0);
			TempXORMemory.SetBigIntAt(0n,1);
			TempXORMemory.SetBigIntAt(ValidTimestamp,2);
			TempXORMemory.SetBigIntAt(TemporalPrecision,3);
			TempXORMemory.SetBigIntAt(0n,4);
			
			G_PrimeBeingGenerated = CreateQuininePrimeHolder(TempXORMemory);
			
			FindNextNearestPrimeBigIntTargetLengthThreaded(ValidTimestamp,Length,G_PrimeBeingGenerated.CallbackPrimeFunction.bind(G_PrimeBeingGenerated));
			alert("Started Generating Prime");
		}
		
	}
	catch(err)
	{
		alert("GeneratePrime: "+err.message);
	}
	finally
	{
		PLEID = null;
		TEID = null;
		TPEI = null;
		
		ValidTimestamp = null;
		Length = null;
		TemporalPrecision = null;
		TempXORMemory = null;
	}
}

function ProcessResponseCode(ReceivedResponseCodeElementID)
{
	try
	{
		//"use strict";
		var RRCEID = document.getElementById(ReceivedResponseCodeElementID);
		//alert("RRCVal: "+RRCEID.value)
		
		if(RRCEID.value == "")
		{
			throw{name:"ProcessResponseCode error",message:"RRCEID.value (The Received Response Code) must not be blank"}
		}
		
		var SplitValues = (""+RRCEID.value).split(",")
		
		if(SplitValues.length != 5)
		{
			throw{name:"ProcessResponseCode error",message:"SplitValues count does not match that of a sent key agreement code. Supplied SplitValues count is: "+SplitValues.length}
		}
		
		var TempCompressor = CreateIntegerCompressor(G_AdditionalCompressorChars,G_RemovedCompressorChars);
		
		var Returns = [0,0,0,0];
		
		var MasqueValue = BigInt(TempCompressor.Decompress(SplitValues[0]))
		Returns[0] = BigInt(TempCompressor.Decompress(SplitValues[1]))
		Returns[1] = BigInt(TempCompressor.Decompress(SplitValues[2]))
		Returns[2] = BigInt(TempCompressor.Decompress(SplitValues[3]))
		Returns[3] = BigInt(TempCompressor.Decompress(SplitValues[4]))
		
		var TempXORMemory = CreateXORMemoryBigInt(4)
		TempXORMemory.SetBigIntAt(Returns[0],0,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[1],1,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[2],2,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[3],3,MasqueValue)
		
		//Pos 0: IncomingSharedKey
		//Pos 1: IncomingIdenifier
		//Pos 2: Quinine version
		//Pos 3: Handshake stage
			//1 - initiation
			
		if(TempXORMemory.GetBigIntAt(2) !== G_QuinineVersion)
		{
			var HelpfulAddon = ""
			if(TempXORMemory.GetBigIntAt(2) > G_QuinineVersion)
			{
				HelpfulAddon = "theirs is newer than yours."
			}
			else
			{
				HelpfulAddon = "yours is newer than theirs."
			}
			
			alert("Error: Quinine version mismatch. You must use matching versions (newest recommended). Yours is version "+G_QuinineVersion+" and theirs is "+TempXORMemory.GetBigIntAt(2)+". This could be due to data corruption, a bug, tampering or "+HelpfulAddon);
			return;
		}
		
		if(TempXORMemory.GetBigIntAt(3) == 1n)
		{
			//The other person is opening a handshake with us
			
			if(G_PrimeBeingGenerated === null)
			{
				alert("You must generate a prime first before you can process a response code.");
				return;
			}
			
			if(G_PrimeBeingGenerated.PrimeGenerated === false)
			{
				alert("Wait until a prime is generated before processing a response code.");
				return;
			}
			
			//If the prime is still generating, fall out and inform the user to wait
			//If prime is generated, perform agreement
			
			//Pos 0: IncomingSharedKey
			//Pos 1: IncomingIdenifier
			var IncomingXORMemory = CreateXORMemoryBigInt(2);
			
			var TempMasque = G_PRBIO.GetGeneratedBigInt();
			
			IncomingXORMemory.SetBigIntAt(TempXORMemory.GetBigIntAt(0,TempMasque),0,TempMasque)
			IncomingXORMemory.SetBigIntAt(TempXORMemory.GetBigIntAt(1,TempMasque),1,TempMasque)
			
			G_PrimeBeingGenerated.DHUK.GenerateSharedSecret(IncomingXORMemory)
			
			var SSID = document.getElementById("SharedSecret");
			
			SSID.innerHTML = "SharedSecret: "+G_PrimeBeingGenerated.DHUK.XORMemory.GetBigIntAt(6);
			
		}
		else
		{
			throw{name:"ProcessResponseCode error",message:"Unexpected handshake stage, was given: "+TempXORMemory.SetBigIntAt(3)}
		}
	}
	catch(err)
	{
		alert("ProcessResponseCode: "+err.message);
	}
	finally
	{
		
	}
}
