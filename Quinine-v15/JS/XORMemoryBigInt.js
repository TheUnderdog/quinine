/*

	XORMemoryBigInt.js library [HollowPoint fork]
	
	Version: 6.1
	Created: 08/07/2018
	Last Updated: 30/06/2021
	
	Purpose:
		To provide encrypted memory storage (at least anything's better than storing it plaintext)
	
	Relies upon:
		PseudoRandomBigInt.js
		CommonEncryptions.js
		
	Notes:
		None
		
	Updates:
	
		6.1
			Patched missing bracket
			Updated CryptoFullUnicodeString to XMBI_CryptoFullUnicodeString
			Patched CreateXORMemoryModulatedCopy to call CreateXORMemoryBigInt instead of CreateXORMemory
			Patched CreateXORMemoryModulatedCopy (BigInt and Original) to call TempXORMemory's garbage length and not 'this' garbage length as it was incorrectly sized for the DeMasque transfer
	
		6.0
			Added entire XORMemoryBigInt class to offer BigInt XOR memory support
	
		5.0
			BigInt overhaul for compatibility with the HollowPoint project
			Cleaned up excessive/unnecessary return statements
			Cleaned up erroneous error handling code
			Expanded error functionality
			
	
		4.1
			Patched bug in CopyFromAndClean where Length wasn't checked against null, resulting in inconsistent behaviour
	
		4.0
			Refactored Hops calculation.
				All calculations were merely summing Hops and HopsOffset together before multiplying.
				Instead, Hops needs to be multiplied, with HopsOffset added to the final calculation.
				To make updating hops calculation in future easier, GetCalc and GetCalcFull have been added as functions.
				
			Fixed GetLength
				It was returning a value 1 unit too low due to improper Hops calculation
				Then 1 unit too high due to iter incrementation order
	
		3.0
			Added SetIntAt
				a bad practice as it can allow memory comparison attacks between operations but necessary to enable 
				anti-keylogger, anti-mouselogging protections. Remodulate has been built to solve the comparison issue.
			
			Added CreateXORMemoryModulatedCopy allows copies of the data re-encrypted with new settings (designed to aid Remodulate)
				It does it directly, which should minimise the exposure time
				
			Added CopyFromAndClean
				Allows copying from one XOR memory module to another (one-way to avoid encouraging bad security practices)
			
			Added Remodulate to allow memory, encryption shuffling to help thwart memory analysis attacks
				This also compensates for SetIntAt's pitfalls
	
		2.0
			Renamed EncryptionsObject from CF to CEO to have a standard naming convention with ArmourDillo
			Added LoadString given there's likely to be a need for it (even if not necessarily secure)
			Added GetGarbageMemoryAsIntArray
			Added GetGarbageMemoryAsString - both functions in order to verify memory is indeed garbage and unreadable
			Added String counterpart functions to IntArray functions (strings are immutable, less secure [stays in memory] and thus NOT advised)
			Switched from Iter++ to ++Iter for efficiency

		1.0
			Added the XORMemory library header
			
	Todo:
		None
	
*/




function CreateXORMemory()
{
	"use strict";
	return {
				CEO:CreateCommonEncryptionsObject(),
				GarbageMemory:null,
				GarbageKey:null,
				Hops:null,
				HopsOffset:null,
				Length:null,
				XMBI_CryptoFullUnicodeString:"¬!\"£$%^&*()_+`1234567890-=\t\nqwertyuiop[]asdfghjkl;'#\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:@~|ZXCVBNM<>?\u2600\u2601\u2602\u2603\u2604\u2605\u2606\u2607\u2608\u2609\u260A\u260B\u260C\u260D\u260E\u260F\u2610\u2611\u2612\u2613\u2614\u2615\u2616\u2617\u2618\u2619\u261A\u261B\u261C\u261D\u261E\u261F\u2620\u2621\u2622\u2623\u2624\u2625\u2626\u2627\u2628\u2629\u262A\u262B\u262C\u262D\u262E\u262F\u2630\u2631\u2632\u2633\u2634\u2635\u2636\u2637\u2638\u2639\u263A\u263B\u263C\u263D\u263E\u263F\u2640\u2641\u2642\u2643\u2644\u2645\u2646\u2647\u2648\u2649\u264A\u264B\u264C\u264D\u264E\u264F\u2650\u2651\u2652\u2653\u2654\u2655\u2656\u2657\u2658\u2659\u265A\u265B\u265C\u265D\u265E\u265F\u2660\u2661\u2662\u2663\u2664\u2665\u2666\u2667\u2668\u2669\u266A\u266B\u266C\u266D\u266E\u266F\u2670\u2671\u2672\u2673\u2674\u2675\u2676\u2677\u2678\u2679\u267A\u267B\u267C\u267D\u267E\u267F\u2680\u2681\u2682\u2683\u2684\u2685\u2686\u2687\u2688\u2689\u268A\u268B\u268C\u268D\u268E\u268F\u2690\u2691\u2692\u2693\u2694\u2695\u2696\u2697\u2698\u2699\u269A\u269B\u269C\u269D\u269E\u269F\u26A0\u26A1\u26A2\u26A3\u26A4\u26A5\u26A6\u26A7\u26A8\u26A9\u26AA\u26AB\u26AC\u26AD\u26AE\u26AF\u26B0\u26B1\u26B2\u26B3\u26B4\u26B5\u26B6\u26B7\u26B8\u26B9\u26BA\u26BB\u26BC\u26BD\u26BE\u26BF\u26C0\u26C1\u26C2\u26C3\u26C4\u26C5\u26C6\u26C7\u26C8\u26C9\u26CA\u26CB\u26CC\u26CD\u26CE\u26CF\u26D0\u26D1\u26D2\u26D3\u26D4\u26D5\u26D6\u26D7\u26D8\u26D9\u26DA\u26DB\u26DC\u26DD\u26DE\u26DF\u26EA\u26EB\u26EC\u26ED\u26EE\u26EF\u26F0\u26F1\u26F2\u26F3\u26F4\u26F5\u26F6\u26F7\u26F8\u26F9\u26FA\u26FB\u26FC\u26FD\u26FE\u26FF",
				GenerateRandomString:function(PermittedCharactersString,LengthOfString)
				{
					try
					{
						var Collector = "";
						var Rando = 0;
						
						for(var Iter = 0;Iter < LengthOfString;Iter++)
						{
							Rando = G_PRBIO.GetGeneratedBigIntAsNumber()
							Collector += PermittedCharactersString.charAt(Rando%PermittedCharactersString.length);
						}
						
						return Collector;
						
					}
					catch(err)
					{
						throw{name:"GenerateRandomString error",message:"GenerateRandomString, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateCryptoIntBetweenRanges:function(From,To)
				{
					try
					{
						return G_PRBIO.GetGeneratedBigIntBetweenRangeAsNumber(From,To);
						
					}
					catch(err)
					{
						throw{name:"GenerateCryptoIntBetweenRanges error",message:"GenerateCryptoIntBetweenRanges, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateRandomFullUnicode:function(LengthOfRandom)
				{
					try
					{
						return this.GenerateRandomString(this.XMBI_CryptoFullUnicodeString,LengthOfRandom);
					}
					catch(err)
					{
						throw{name:"GenerateRandomFullUnicode error",message:"GenerateRandomFullUnicode, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateRandomFullKeyboard:function(LengthOfRandom)
				{
					try
					{
						return this.GenerateRandomString("¬!\"£$%^&*()_+`1234567890-=\t\nqwertyuiop[]asdfghjkl;'#\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:@~|ZXCVBNM<>?",LengthOfRandom);
					}
					catch(err)
					{
						throw{name:"GenerateRandomFullKeyboard error",message:"GenerateRandomFullKeyboard, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetCalcFull:function(IncomingHopsOffset,IncomingHops,IncomingFactor,OptionalOutByOneOffset)
				{
					try
					{
						return IncomingHopsOffset+(IncomingHops*(IncomingFactor-(!!OptionalOutByOneOffset)))+(!!OptionalOutByOneOffset);	
					}
					catch(err)
					{
						throw{name:"GetCalcFull error",message:"GetCalcFull, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetCalc:function(Factor,OptionalOutByOneOffset)
				{
					try
					{
						return this.GetCalcFull(this.HopsOffset,this.Hops,Factor,OptionalOutByOneOffset);
					}
					catch(err)
					{
						throw{name:"GetCalc error",message:"GetCalc, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateGarbageMemory:function(Len,OptionalHops,OptionalHopsOffset)
				{
					
					try
					{
						var OH = (G_Detector.IsNumber(OptionalHops)) ? OptionalHops : this.GenerateCryptoIntBetweenRanges(2,7);
						var OHO = (G_Detector.IsNumber(OptionalHopsOffset)) ? OptionalHopsOffset : this.GenerateCryptoIntBetweenRanges(1,7);
						
						if(OH == OHO && !OptionalHops && !OptionalHopsOffset)
						{
							OHO += 1;	
						}
						
						var Value = this.GetCalcFull(OHO,OH,Len,true);
						
						this.Hops = OH;
						this.HopsOffset = OHO;
						this.GarbageMemory = this.CEO.StringToArrayOfInts(this.GenerateRandomFullUnicode(Value)); //Replace with full int range??
						this.GarbageKey = this.CEO.StringToArrayOfInts(this.GenerateRandomFullUnicode(Value));
					}
					catch(err)
					{
						throw{name:"GenerateGarbageMemory error",message:"GenerateGarbageMemory, line number "+err.lineNumber+": "+err.message};
					}
				},
				ExpandGarbageMemory:function(ByAmount)
				{
					try
					{
						if(ByAmount == undefined || ByAmount == null || ByAmount < 1)
						{
							throw{name:"ExpandGarbageMemory error",message:"ByAmount is invalid (undefined, null or less than one)."};
						}
						
						if(this.GarbageMemory == null)
						{
							this.GenerateGarbageMemory(ByAmount);
							return;
						}
						
						var ByValue = (this.GetCalc(ByAmount)-this.HopsOffset);
						
						
						var TempGarbageMemoryExpansion = this.CEO.StringToArrayOfInts(this.GenerateRandomFullUnicode(ByValue)),
							TempGarbageKeyExpansion = this.CEO.StringToArrayOfInts(this.GenerateRandomFullUnicode(ByValue));
						
						this.CEO.AppendIntArrays(this.GarbageMemory,TempGarbageMemoryExpansion);
						this.CEO.AppendIntArrays(this.GarbageKey,TempGarbageKeyExpansion);
						
						if(this.Length != null)
						{
							this.Length = null;
							this.GetLength();
						}
					}
					catch(err)
					{
						throw{name:"ExpandGarbageMemory error",message:"ExpandGarbageMemory, line number "+err.lineNumber+": "+err.message};
					}
					
				},
				OverwriteIntArray:function(IncomingIntArray,OptionalDemaskValue)
				{
					try
					{
						//Fully replace the array so nothing gets left behind
						this.GarbageMemory = this.CEO.StringToArrayOfInts(this.GenerateRandomFullKeyboard(this.GarbageMemory.length));
						this.GarbageKey = this.CEO.StringToArrayOfInts(this.GenerateRandomFullKeyboard(this.GarbageMemory.length));
						var AccessPoint = 0;
						
						if(!OptionalDemaskValue)
						{
							for(var Iter = 0;Iter < IncomingIntArray.length;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								this.GarbageMemory[AccessPoint] = this.CEO.XOR(IncomingIntArray[Iter],this.GarbageKey[AccessPoint]);
							}
						}
						else
						{
							for(var Iter = 0;Iter < IncomingIntArray.length;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								this.GarbageMemory[AccessPoint] = this.CEO.XOR(this.CEO.XOR(IncomingIntArray[Iter],OptionalDemaskValue),this.GarbageKey[AccessPoint]);
							}	
						}
					}
					catch(err)
					{
						throw{name:"OverwriteIntArray error",message:"OverwriteIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				LoadIntArray:function(IncomingIntArray,OptionalHops,OptionalHopsOffset,OptionalDemaskValue)
				{
					try
					{
						if(IsInvalid(IncomingIntArray) || IncomingIntArray.length == undefined)
						{
							throw{name:"LoadIntArray error",message:"LoadIntArray: IncomingIntArray is not valid"};
						}
						
						this.Length = null;
						this.GenerateGarbageMemory(IncomingIntArray.length,OptionalHops,OptionalHopsOffset);
						this.OverwriteIntArray(IncomingIntArray,OptionalDemaskValue);
					}
					catch(err)
					{
						throw{name:"LoadIntArray error",message:"LoadIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				LoadString:function(IncomingString,OptionalHops,OptionalHopsOffset)
				{
					try
					{
						this.LoadIntArray(this.CEO.StringToArrayOfInts(IncomingString),OptionalHops,OptionalHopsOffset);
					}
					catch(err)
					{
						throw{name:"LoadString error",message:"LoadString, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetLength:function()
				{
					try
					{
						if(this.Length != null)
						{
							return this.Length;	
						}
						
						if(this.Hops == null || this.HopsOffset == null || this.GarbageMemory == null || this.GarbageMemory.length == null)
						{
							//Some sort of invalid state
							this.Length = null;
							return -1;
						}
						
						var AccessPoint = this.GetCalc(0),
							Iter = 0;
						
						while(AccessPoint < this.GarbageMemory.length)
						{
							++Iter;
							AccessPoint = this.GetCalc(Iter)
						}
						
						this.Length = Iter;
						return Iter;
					}
					catch(err)
					{
						throw{name:"GetLength error",message:"GetLength, line number "+err.lineNumber+": "+err.message};
					}
				},
				//After completing all operations that use SetIntAt, it's HIGHLY RECOMMENDED to call Remodulate()
				//By default Remodulate will be called after every SetIntAt, but this might be too resource intensive
				//It's recommended to do all bulk standing SetIntAt operations and then Remodulate immediately after
				SetIntAt:function(Value,Position,OptionalDemaskValue,OptionalDisableSecurityAggression)
				{
					try
					{
						var AccessPoint = this.GetCalc(Position);
						if(AccessPoint >= this.GarbageMemory.length)
						{
							
							throw{name:"SetIntAt error",message:"Position out of bounds. Position is: "+Position+" Length: "+this.GetLength()};
						}
						
						if(!OptionalDemaskValue)
						{
							 this.GarbageMemory[AccessPoint] = this.CEO.XOR(Value,this.GarbageKey[AccessPoint]);
						}
						else
						{
							 this.GarbageMemory[AccessPoint] = this.CEO.XOR(this.CEO.XOR(Value,OptionalDemaskValue),this.GarbageKey[AccessPoint]);
						}
						
						if(!OptionalDisableSecurityAggression)
						{
							this.Remodulate();
						}
					}
					catch(err)
					{
						throw{name:"SetIntAt error",message:"SetIntAt, line number "+err.lineNumber+": "+err.message};
					}
				},
				AddInt:function(Value,OptionalDemaskValue,OptionalDisableSecurityAggression)
				{
					try
					{
						this.ExpandGarbageMemory(1);
						//Remodulate is called implicitly
						this.SetIntAt(Value,this.GetLength()-1,OptionalDemaskValue,OptionalDisableSecurityAggression);
					}
					catch(err)
					{
						throw{name:"AddInt error",message:"AddInt, line number "+err.lineNumber+": "+err.message};
					}
				},
				AddChar:function(ValueString,OptionalDemaskValue,OptionalDisableSecurityAggression)
				{
					try
					{
						this.AddInt(this.CEO.StringToArrayOfInts(ValueString)[0],OptionalDemaskValue,OptionalDisableSecurityAggression);
					}
					catch(err)
					{
						throw{name:"AddChar error",message:"AddChar, line number "+err.lineNumber+": "+err.message};
					}
				},
				//A mask value basically returns the int, but masked by a specific XOR'd value
				GetIntAt:function(Position,OptionalMaskValue)
				{
					try
					{
						var AccessPoint = this.GetCalc(Position);
						
						if(AccessPoint >= this.GarbageMemory.length)
						{
							throw{name:"GetIntAt error",message:"Position out of bounds."};
						}
						
						if(!OptionalMaskValue)
						{
							return this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]);
						}
						else
						{
							return this.CEO.XOR(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]),OptionalMaskValue);
						}
					}
					catch(err)
					{
						throw{name:"GetIntAt error",message:"GetIntAt, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetAsIntArray:function(OptionalMaskValue)
				{
					try
					{
						var Collector = [];
						var AccessPoint = 0;
						
						if(!OptionalMaskValue)
						{
							for(var Iter = 0;true;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								
								if(AccessPoint >= this.GarbageMemory.length)
								{
									break;	
								}
								Collector.push(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]));
							}
						}
						else
						{
							for(var Iter = 0;true;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								if(AccessPoint >= this.GarbageMemory.length)
								{
									break;	
								}
								Collector.push(this.CEO.XOR(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]),OptionalMaskValue));
							}
						}
						
						return Collector;
					}
					catch(err)
					{
						throw{name:"GetAsIntArray error",message:"GetAsIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetAsString:function()
				{
					try
					{
						return this.CEO.ArrayOfIntsToString(this.GetAsIntArray());
					}
					catch(err)
					{
						throw{name:"GetAsString error",message:"GetAsString, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetGarbageMemoryAsIntArray:function()
				{
					try
					{
						return this.GarbageMemory.slice(0);	
					}
					catch(err)
					{
						throw{name:"GetGarbageMemoryAsIntArray error",message:"GetGarbageMemoryAsIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetGarbageMemoryAsString:function()
				{
					try
					{
						return this.CEO.ArrayOfIntsToString(this.GetGarbageMemoryAsIntArray());
					}
					catch(err)
					{
						throw{name:"GetGarbageMemoryAsString error",message:"GetGarbageMemoryAsString, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetValidPosition:function(PossiblePosition)
				{
					try
					{
						var Temp = this.GetLength();
						
						if(Temp < 1)
						{
							throw{name:"GetValidPosition error",message:"XORObject is not in a valid state (can't get length: less than 1)."}	
						}
						
						return PossiblePosition%Temp;
					}
					catch(err)
					{
						throw{name:"GetValidPosition error",message:"GetValidPosition, line number "+err.lineNumber+": "+err.message};
					}
				},
				//We don't provide a hard/exact copy because this encourages bad practices in the usage of XORMemory
				//The data is still the same, what it's stored in and where is different
				CreateXORMemoryModulatedCopy:function(OptionalHops,OptionalHopsOffset)
				{
					try
					{
						//Create a new instance of an XORMemory object
						var TempXORMemory = CreateXORMemory();
						
						//Get our current length
						var Length = this.GetLength();
						
						if(Length < 1)
						{
							//Some sort of length error occurred, quit
							throw{name:"CreateXORMemoryModulatedCopy error",message:"XORObject is not in a valid state (can't get length: less than 1)."}	
						}
						
						//Generate new garbage memory into our object
						TempXORMemory.GenerateGarbageMemory(Length,OptionalHops,OptionalHopsOffset);
						
						
						//Generate a value masque to cover the transfer of data between objects
						var DeMasque = this.CEO.StringToArrayOfInts(this.GenerateRandomFullUnicode(TempXORMemory.GarbageMemory.length)),
							TempXORAccessPoint = TempXORMemory.GetCalc(0),
							TempXORPosition = 0;
						
						for(var TempXORIter = 0; TempXORIter < TempXORMemory.GarbageMemory.length;++TempXORIter)
						{
							if(TempXORIter == TempXORAccessPoint)
							{
								TempXORMemory.GarbageMemory[TempXORIter] = this.CEO.XOR(this.GetIntAt(TempXORPosition),TempXORMemory.GarbageKey[TempXORIter]);
								++TempXORPosition;
								TempXORAccessPoint = TempXORMemory.GetCalc(TempXORPosition);
							}
							else
							{
								TempXORMemory.GarbageMemory[TempXORIter] = this.CEO.XOR(DeMasque[TempXORIter],TempXORMemory.GarbageKey[TempXORIter]);
							}
						}
						
						return TempXORMemory;

					}
					catch(err)
					{
						throw{name:"CreateXORMemoryModulatedCopy error",message:"CreateXORMemoryModulatedCopy, line number "+err.lineNumber+": "+err.message};
					}
				},
				CopyFromAndClean:function(IncomingXORMemoryObject)
				{
					try
					{
						if(IncomingXORMemoryObject == undefined || IncomingXORMemoryObject == null)
						{
							throw{name:"CopyFromAndClean error",message:"IncomingXORMemoryObject is invalid."};
						}
						
						//Why are we using .slice(0) and '0+'?
						//It forces JavaScript to create new memory spaces for both
						//.slice does a 'stand in' operation on an int array, creating an exact copy
						//0+ forces an integer operation to return a summed value which is the same as the original
						//Which in turn creates a new memory storage object rather than 'handing over' a reference
						//An attacker could be tracking the original reference, so we have to create a new one
						//In order to try to throw them off
						
						if(IncomingXORMemoryObject.GarbageMemory != null)
						{
							this.GarbageMemory = IncomingXORMemoryObject.GarbageMemory.slice(0);
							IncomingXORMemoryObject.GarbageMemory = null;
						}
						
						if(IncomingXORMemoryObject.GarbageKey != null)
						{
							this.GarbageKey = IncomingXORMemoryObject.GarbageKey.slice(0);
							IncomingXORMemoryObject.GarbageKey = null;
						}
						
						if(IncomingXORMemoryObject.Hops != null)
						{
							this.Hops = (0+IncomingXORMemoryObject.Hops);
							IncomingXORMemoryObject.Hops = null;
						}
						
						if(IncomingXORMemoryObject.HopsOffset != null)
						{
							this.HopsOffset = (0+IncomingXORMemoryObject.HopsOffset);
							IncomingXORMemoryObject.HopsOffset = null;
						}
						
						if(IncomingXORMemoryObject.Length != null)
						{
							this.Length = (0+IncomingXORMemoryObject.Length);
							IncomingXORMemoryObject.Length = null;
						}
					}
					catch(err)
					{
						throw{name:"CopyFromAndClean error",message:"CopyFromAndClean, line number "+err.lineNumber+": "+err.message};
					}
				},
				Remodulate:function()
				{
					try
					{
						this.CopyFromAndClean(this.CreateXORMemoryModulatedCopy());
					}
					catch(err)
					{
						throw{name:"Remodulate error",message:"Remodulate, line number "+err.lineNumber+": "+err.message};
					}					
				}
			};
}

G_Library.Add({Type:"Function",Functor:CreateXORMemory,IsWebWorkerCompatible:true});

function CreateXORMemoryBigInt(OptionalTargetLength)
{
	"use strict";
	var Temp = {
				CEO:CreateCommonEncryptionsObject(),
				GarbageMemory:null,
				GarbageKey:null,
				Hops:null,
				HopsOffset:null,
				Length:null,
				XMBI_CryptoFullUnicodeString:"¬!\"£$%^&*()_+`1234567890-=\t\nqwertyuiop[]asdfghjkl;'#\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:@~|ZXCVBNM<>?\u2600\u2601\u2602\u2603\u2604\u2605\u2606\u2607\u2608\u2609\u260A\u260B\u260C\u260D\u260E\u260F\u2610\u2611\u2612\u2613\u2614\u2615\u2616\u2617\u2618\u2619\u261A\u261B\u261C\u261D\u261E\u261F\u2620\u2621\u2622\u2623\u2624\u2625\u2626\u2627\u2628\u2629\u262A\u262B\u262C\u262D\u262E\u262F\u2630\u2631\u2632\u2633\u2634\u2635\u2636\u2637\u2638\u2639\u263A\u263B\u263C\u263D\u263E\u263F\u2640\u2641\u2642\u2643\u2644\u2645\u2646\u2647\u2648\u2649\u264A\u264B\u264C\u264D\u264E\u264F\u2650\u2651\u2652\u2653\u2654\u2655\u2656\u2657\u2658\u2659\u265A\u265B\u265C\u265D\u265E\u265F\u2660\u2661\u2662\u2663\u2664\u2665\u2666\u2667\u2668\u2669\u266A\u266B\u266C\u266D\u266E\u266F\u2670\u2671\u2672\u2673\u2674\u2675\u2676\u2677\u2678\u2679\u267A\u267B\u267C\u267D\u267E\u267F\u2680\u2681\u2682\u2683\u2684\u2685\u2686\u2687\u2688\u2689\u268A\u268B\u268C\u268D\u268E\u268F\u2690\u2691\u2692\u2693\u2694\u2695\u2696\u2697\u2698\u2699\u269A\u269B\u269C\u269D\u269E\u269F\u26A0\u26A1\u26A2\u26A3\u26A4\u26A5\u26A6\u26A7\u26A8\u26A9\u26AA\u26AB\u26AC\u26AD\u26AE\u26AF\u26B0\u26B1\u26B2\u26B3\u26B4\u26B5\u26B6\u26B7\u26B8\u26B9\u26BA\u26BB\u26BC\u26BD\u26BE\u26BF\u26C0\u26C1\u26C2\u26C3\u26C4\u26C5\u26C6\u26C7\u26C8\u26C9\u26CA\u26CB\u26CC\u26CD\u26CE\u26CF\u26D0\u26D1\u26D2\u26D3\u26D4\u26D5\u26D6\u26D7\u26D8\u26D9\u26DA\u26DB\u26DC\u26DD\u26DE\u26DF\u26EA\u26EB\u26EC\u26ED\u26EE\u26EF\u26F0\u26F1\u26F2\u26F3\u26F4\u26F5\u26F6\u26F7\u26F8\u26F9\u26FA\u26FB\u26FC\u26FD\u26FE\u26FF",
				GenerateRandomString:function(PermittedCharactersString,LengthOfString)
				{
					try
					{
						var Collector = "";
						var Rando = 0;
						
						for(var Iter = 0;Iter < LengthOfString;Iter++)
						{
							Rando = G_PRBIO.GetGeneratedBigIntAsNumber()
							Collector += PermittedCharactersString.charAt(Rando%PermittedCharactersString.length);
						}
						
						return Collector;
						
					}
					catch(err)
					{
						throw{name:"GenerateRandomString error",message:"GenerateRandomString, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateCryptoIntBetweenRanges:function(From,To)
				{
					try
					{
						return G_PRBIO.GetGeneratedBigIntBetweenRangeAsNumber(From,To);
						
					}
					catch(err)
					{
						throw{name:"GenerateCryptoIntBetweenRanges error",message:"GenerateCryptoIntBetweenRanges, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateRandomFullUnicode:function(LengthOfRandom)
				{
					try
					{
						return this.GenerateRandomString(this.XMBI_CryptoFullUnicodeString,LengthOfRandom);
					}
					catch(err)
					{
						throw{name:"GenerateRandomFullUnicode error",message:"GenerateRandomFullUnicode, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateRandomFullKeyboard:function(LengthOfRandom)
				{
					try
					{
						return this.GenerateRandomString("¬!\"£$%^&*()_+`1234567890-=\t\nqwertyuiop[]asdfghjkl;'#\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:@~|ZXCVBNM<>?",LengthOfRandom);
					}
					catch(err)
					{
						throw{name:"GenerateRandomFullKeyboard error",message:"GenerateRandomFullKeyboard, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetCalcFull:function(IncomingHopsOffset,IncomingHops,IncomingFactor,OptionalOutByOneOffset)
				{
					try
					{
						return IncomingHopsOffset+(IncomingHops*(IncomingFactor-(!!OptionalOutByOneOffset)))+(!!OptionalOutByOneOffset);	
					}
					catch(err)
					{
						throw{name:"GetCalcFull error",message:"GetCalcFull, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetCalc:function(Factor,OptionalOutByOneOffset)
				{
					try
					{
						return this.GetCalcFull(this.HopsOffset,this.Hops,Factor,OptionalOutByOneOffset);
					}
					catch(err)
					{
						throw{name:"GetCalc error",message:"GetCalc, line number "+err.lineNumber+": "+err.message};
					}
				},
				GenerateGarbageMemory:function(Len,OptionalHops,OptionalHopsOffset)
				{
					
					try
					{
						var OH = (G_Detector.IsNumber(OptionalHops)) ? OptionalHops : this.GenerateCryptoIntBetweenRanges(2,7);
						var OHO = (G_Detector.IsNumber(OptionalHopsOffset)) ? OptionalHopsOffset : this.GenerateCryptoIntBetweenRanges(1,7);
						
						if(OH == OHO && !OptionalHops && !OptionalHopsOffset)
						{
							OHO += 1;	
						}
						
						var TargetLength = this.GetCalcFull(OHO,OH,Len,true);
						
						this.Hops = OH;
						this.HopsOffset = OHO;
						
						this.GarbageMemory = G_PRBIO.GetGeneratedBigIntArray(TargetLength);
						this.GarbageKey = G_PRBIO.GetGeneratedBigIntArray(TargetLength);
					}
					catch(err)
					{
						throw{name:"GenerateGarbageMemory error",message:"GenerateGarbageMemory, line number "+err.lineNumber+": "+err.message};
					}
				},
				
				ExpandGarbageMemory:function(ByAmount)
				{
					try
					{
						if(ByAmount == undefined || ByAmount == null || ByAmount < 1)
						{
							throw{name:"ExpandGarbageMemory error",message:"ByAmount is invalid (undefined, null or less than one)."};
						}
						
						if(this.GarbageMemory == null)
						{
							this.GenerateGarbageMemory(ByAmount);
							return;
						}
						
						var TargetLength = (this.GetCalc(ByAmount)-this.HopsOffset);
						
						
						var TempGarbageMemoryExpansion = G_PRBIO.GetGeneratedBigIntArray(TargetLength),
							TempGarbageKeyExpansion = G_PRBIO.GetGeneratedBigIntArray(TargetLength);
						
						this.CEO.AppendIntArrays(this.GarbageMemory,TempGarbageMemoryExpansion);
						this.CEO.AppendIntArrays(this.GarbageKey,TempGarbageKeyExpansion);
						
						if(this.Length != null)
						{
							this.Length = null;
							this.GetLength();
						}
					}
					catch(err)
					{
						throw{name:"ExpandGarbageMemory error",message:"ExpandGarbageMemory, line number "+err.lineNumber+": "+err.message};
					}
					
				},
				OverwriteIntArray:function(IncomingIntArray,OptionalDemaskValue)
				{
					try
					{
						//Fully replace the array so nothing gets left behind
						this.GarbageMemory = G_PRBIO.GetGeneratedBigIntArray(this.GarbageMemory.length);
						this.GarbageKey = G_PRBIO.GetGeneratedBigIntArray(this.GarbageMemory.length);
						var AccessPoint = 0;
						
						if(!OptionalDemaskValue)
						{
							for(var Iter = 0;Iter < IncomingIntArray.length;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								this.GarbageMemory[AccessPoint] = this.CEO.XOR(IncomingIntArray[Iter],this.GarbageKey[AccessPoint]);
							}
						}
						else
						{
							for(var Iter = 0;Iter < IncomingIntArray.length;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								this.GarbageMemory[AccessPoint] = this.CEO.XOR(this.CEO.XOR(IncomingIntArray[Iter],OptionalDemaskValue),this.GarbageKey[AccessPoint]);
							}	
						}
					}
					catch(err)
					{
						throw{name:"OverwriteIntArray error",message:"OverwriteIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				LoadBigIntArray:function(IncomingBigIntArray,OptionalHops,OptionalHopsOffset,OptionalDemaskValue)
				{
					try
					{
						if(IsInvalid(IncomingBigIntArray) || IncomingBigIntArray.length == undefined)
						{
							throw{name:"LoadBigIntArray error",message:"LoadBigIntArray: IncomingIntArray is not valid"};
						}
						
						this.Length = null;
						this.GenerateGarbageMemory(IncomingBigIntArray.length,OptionalHops,OptionalHopsOffset);
						this.OverwriteIntArray(IncomingBigIntArray,OptionalDemaskValue);
					}
					catch(err)
					{
						throw{name:"LoadBigIntArray error",message:"LoadBigIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetLength:function()
				{
					try
					{
						if(this.Length != null)
						{
							return this.Length;	
						}
						
						if(this.Hops == null || this.HopsOffset == null || this.GarbageMemory == null || this.GarbageMemory.length == null)
						{
							//Some sort of invalid state
							this.Length = null;
							return -1;
						}
						
						var AccessPoint = this.GetCalc(0),
							Iter = 0;
						
						while(AccessPoint < this.GarbageMemory.length)
						{
							++Iter;
							AccessPoint = this.GetCalc(Iter)
						}
						
						this.Length = Iter;
						return Iter;
					}
					catch(err)
					{
						throw{name:"GetLength error",message:"GetLength, line number "+err.lineNumber+": "+err.message};
					}
				},
				//After completing all operations that use SetIntAt, it's HIGHLY RECOMMENDED to call Remodulate()
				//By default Remodulate will be called after every SetIntAt, but this might be too resource intensive
				//It's recommended to do all bulk standing SetIntAt operations and then Remodulate immediately after
				SetBigIntAt:function(IncomingBigInt,Position,OptionalDemaskValue,OptionalDisableSecurityAggression)
				{
					try
					{
						var AccessPoint = this.GetCalc(Position);
						if(AccessPoint >= this.GarbageMemory.length)
						{
							
							throw{name:"SetBigIntAt error",message:"Position out of bounds. Position is: "+Position+" Length: "+this.GetLength()};
						}
						
						if(!OptionalDemaskValue)
						{
							this.GarbageMemory[AccessPoint] = this.CEO.XOR(IncomingBigInt,this.GarbageKey[AccessPoint]);
						}
						else
						{
							this.GarbageMemory[AccessPoint] = this.CEO.XOR(this.CEO.XOR(IncomingBigInt,OptionalDemaskValue),this.GarbageKey[AccessPoint]);
						}
						
						if(!OptionalDisableSecurityAggression)
						{
							this.Remodulate();
						}
					}
					catch(err)
					{
						throw{name:"SetBigIntAt error",message:"SetBigIntAt, line number "+err.lineNumber+": "+err.message};
					}
				},
				AddBigInt:function(IncomingBigInt,OptionalDemaskValue,OptionalDisableSecurityAggression)
				{
					try
					{
						this.ExpandGarbageMemory(1);
						//Remodulate is called implicitly
						this.SetBigIntAt(IncomingBigInt,this.GetLength()-1,OptionalDemaskValue,OptionalDisableSecurityAggression);
					}
					catch(err)
					{
						throw{name:"AddBigInt error",message:"AddBigInt, line number "+err.lineNumber+": "+err.message};
					}
				},
				//A mask value basically returns the int, but masked by a specific XOR'd value
				GetBigIntAt:function(Position,OptionalMaskValue)
				{
					try
					{
						var AccessPoint = this.GetCalc(Position);
						
						if(AccessPoint >= this.GarbageMemory.length)
						{
							throw{name:"GetBigIntAt error",message:"Position out of bounds."};
						}
						
						if(!OptionalMaskValue)
						{
							return this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]);
						}
						else
						{
							return this.CEO.XOR(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]),OptionalMaskValue);
						}
					}
					catch(err)
					{
						throw{name:"GetBigIntAt error",message:"GetBigIntAt, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetAsBigIntArray:function(OptionalMaskValue)
				{
					try
					{
						var Collector = [];
						var AccessPoint = 0;
						
						if(!OptionalMaskValue)
						{
							for(var Iter = 0;true;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								
								if(AccessPoint >= this.GarbageMemory.length)
								{
									break;	
								}
								Collector.push(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]));
							}
						}
						else
						{
							for(var Iter = 0;true;++Iter)
							{
								AccessPoint = this.GetCalc(Iter);
								if(AccessPoint >= this.GarbageMemory.length)
								{
									break;	
								}
								Collector.push(this.CEO.XOR(this.CEO.XOR(this.GarbageMemory[AccessPoint],this.GarbageKey[AccessPoint]),OptionalMaskValue));
							}
						}
						
						return Collector;
					}
					catch(err)
					{
						throw{name:"GetAsBigIntArray error",message:"GetAsBigIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetGarbageMemoryAsBigIntArray:function()
				{
					try
					{
						return this.GarbageMemory.slice(0);	
					}
					catch(err)
					{
						throw{name:"GetGarbageMemoryAsIntArray error",message:"GetGarbageMemoryAsIntArray, line number "+err.lineNumber+": "+err.message};
					}
				},
				GetValidPosition:function(PossiblePosition)
				{
					try
					{
						var Temp = this.GetLength();
						
						if(Temp < 1)
						{
							throw{name:"GetValidPosition error",message:"XORObject is not in a valid state (can't get length: less than 1)."}	
						}
						
						return PossiblePosition%Temp;
					}
					catch(err)
					{
						throw{name:"GetValidPosition error",message:"GetValidPosition, line number "+err.lineNumber+": "+err.message};
					}
				},
				//We don't provide a hard/exact copy because this encourages bad practices in the usage of XORMemory
				//The data is still the same, what it's stored in and where is different
				CreateXORMemoryModulatedCopy:function(OptionalHops,OptionalHopsOffset)
				{
					try
					{
						//Create a new instance of an XORMemory object
						var TempXORMemory = CreateXORMemoryBigInt();
						
						//Get our current length
						var Length = this.GetLength();
						
						if(Length < 1)
						{
							//Some sort of length error occurred, quit
							throw{name:"CreateXORMemoryModulatedCopy error",message:"XORObject is not in a valid state (can't get length: less than 1)."}	
						}
						
						//Generate new garbage memory into our object
						TempXORMemory.GenerateGarbageMemory(Length,OptionalHops,OptionalHopsOffset);
						
						//Generate a value masque to cover the transfer of data between objects
						var DeMasque = G_PRBIO.GetGeneratedBigIntArray(TempXORMemory.GarbageMemory.length),
							TempXORAccessPoint = TempXORMemory.GetCalc(0),
							TempXORPosition = 0;
						
						for(var TempXORIter = 0; TempXORIter < TempXORMemory.GarbageMemory.length;++TempXORIter)
						{
							if(TempXORIter == TempXORAccessPoint)
							{
								TempXORMemory.GarbageMemory[TempXORIter] = this.CEO.XOR(this.GetBigIntAt(TempXORPosition),TempXORMemory.GarbageKey[TempXORIter]);
								++TempXORPosition;
								TempXORAccessPoint = TempXORMemory.GetCalc(TempXORPosition);
							}
							else
							{
								
								TempXORMemory.GarbageMemory[TempXORIter] = this.CEO.XOR(DeMasque[TempXORIter],TempXORMemory.GarbageKey[TempXORIter]);
							}
						}
						
						return TempXORMemory;

					}
					catch(err)
					{
						throw{name:"CreateXORMemoryModulatedCopy error",message:"CreateXORMemoryModulatedCopy, line number "+err.lineNumber+": "+err.message};
					}
				},
				CopyFromAndClean:function(IncomingXORMemoryObject)
				{
					try
					{
						if(IncomingXORMemoryObject == undefined || IncomingXORMemoryObject == null)
						{
							throw{name:"CopyFromAndClean error",message:"IncomingXORMemoryObject is invalid."};
						}
						
						//Why are we using .slice(0) and '0+'?
						//It forces JavaScript to create new memory spaces for both
						//.slice does a 'stand in' operation on an int array, creating an exact copy
						//0+ forces an integer operation to return a summed value which is the same as the original
						//Which in turn creates a new memory storage object rather than 'handing over' a reference
						//An attacker could be tracking the original reference, so we have to create a new one
						//In order to try to throw them off
						
						if(IncomingXORMemoryObject.GarbageMemory != null)
						{
							this.GarbageMemory = IncomingXORMemoryObject.GarbageMemory.slice(0);
							IncomingXORMemoryObject.GarbageMemory = null;
						}
						
						if(IncomingXORMemoryObject.GarbageKey != null)
						{
							this.GarbageKey = IncomingXORMemoryObject.GarbageKey.slice(0);
							IncomingXORMemoryObject.GarbageKey = null;
						}
						
						if(IncomingXORMemoryObject.Hops != null)
						{
							this.Hops = (0+IncomingXORMemoryObject.Hops);
							IncomingXORMemoryObject.Hops = null;
						}
						
						if(IncomingXORMemoryObject.HopsOffset != null)
						{
							this.HopsOffset = (0+IncomingXORMemoryObject.HopsOffset);
							IncomingXORMemoryObject.HopsOffset = null;
						}
						
						if(IncomingXORMemoryObject.Length != null)
						{
							this.Length = (0+IncomingXORMemoryObject.Length);
							IncomingXORMemoryObject.Length = null;
						}
					}
					catch(err)
					{
						throw{name:"CopyFromAndClean error",message:"CopyFromAndClean, line number "+err.lineNumber+": "+err.message};
					}
				},
				Remodulate:function()
				{
					try
					{
						this.CopyFromAndClean(this.CreateXORMemoryModulatedCopy());
					}
					catch(err)
					{
						throw{name:"Remodulate error",message:"Remodulate, line number "+err.lineNumber+": "+err.message};
					}					
				},
				ExportAsJSON:function()
				{
					try
					{
						/*
						GarbageMemory //list (of BigInt)
						GarbageKey //list (of BigInt)
						Hops //int
						HopsOffset //int
						Length //int
						*/
						
						var TempObject = {
											GarbageMemory:this.GarbageMemory,
											GarbageKey:this.GarbageKey,
											Hops:this.Hops,
											HopsOffset:this.HopsOffset,
											Length:this.Length
										};
						
						return JSON.stringify(TempObject);
						
					}
					catch(err)
					{
						throw{name:"Export error",message:"Export, line number "+err.lineNumber+": "+err.message};
					}
				},
			};
			
		if(OptionalTargetLength != undefined && OptionalTargetLength != null)
		{
			Temp.GenerateGarbageMemory(OptionalTargetLength);
		}
		return Temp;
}


G_Library.Add({Type:"Function",Functor:CreateXORMemoryBigInt,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"XORMemoryBigInt.js"});
