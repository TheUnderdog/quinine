/*

	CommonEncryptions.js library
	
	Version: 2.1
	Created: 08/07/2018
	Last Updated: 08/07/2021
	
	Purpose:
		Provide a common access object for common encryption operations
	
	Relies upon:
		JavaScriptLibraryLoader.js
		
	Notes:
		None
		
	Updates:
	
		2.1
			Added JavaScriptLibraryLoader support

		2.0
			
			Added error handling to all functions
			Clarified error message
			Removed unnecessary return null statements from error functions

		1.0
			Added the CommonEncryptions library header
			
	Todo:
		None
	
*/

function CreateCommonEncryptionsObject()
{
	"use strict";
	
	return {
				StringToArrayOfChars:function(IncomingString)
				{
					try
					{
						return IncomingString.split();
					}
					catch(err)
					{
						throw{name:"StringToArrayOfChars error",message:"StringToArrayOfChars: "+err.message}
					}
				},
				IntToChar:function(IncomingInt)
				{
					try
					{
						return String.fromCharCode(IncomingInt);
					}
					catch(err)
					{
						throw{name:"IntToChar error",message:"IntToChar: "+err.message}
					}
				},
				CharToInt:function(IncomingChar)
				{
					try
					{
						return IncomingChar.charCodeAt(0);
					}
					catch(err)
					{
						throw{name:"CharToInt error",message:"CharToInt: "+err.message}
					}
				},
				AppendIntArrays:function(IntArray1,IntArray2)
				{
					try
					{
						if(IntArray1 == undefined || IntArray1 == null)
						{
							throw{name:"AppendIntArrays error",message:"IntArray1 is invalid."}
						}
						
						if(IntArray2 == undefined || IntArray2 == null)
						{
							throw{name:"AppendIntArrays error",message:"IntArray2 is invalid."}
						}
						
						for(var Iter = 0;Iter < IntArray2.length;++Iter)
						{
							IntArray1.push(IntArray2[Iter]);	
						}
						return IntArray1;
					}
					catch(err)
					{
						throw{name:"AppendIntArrays error",message:"AppendIntArrays: "+err.message}
					}
				},
				StringToArrayOfInts:function(IncomingString)
				{
					try
					{
						var Collector = [];
						for(var Iter = 0; Iter < IncomingString.length;Iter++)
						{
							Collector.push(IncomingString.charCodeAt(Iter));
						}
						return Collector;
					}
					catch(err)
					{
						throw{name:"StringToArrayOfInts error",message:"StringToArrayOfInts: "+err.message}
					}
				},
				ArrayOfIntsToString:function(IncomingArrayOfInts)
				{
					try
					{
						var Collector = "";
						for(var Iter = 0; Iter < IncomingArrayOfInts.length;Iter++)
						{
							Collector += String.fromCharCode(IncomingArrayOfInts[Iter]);
						}
						return Collector;
					
					}
					catch(err)
					{
						throw{name:"ArrayOfIntsToString error",message:"ArrayOfIntsToString: "+err.message}
					}
				},
				XOR:function(Int1,Int2)
				{
					try
					{
						if(Int1 == undefined || Int1 == null)
						{
							throw{name:"XOR error",message:"Int1 is undefined/null",lineNumber:134}
						}
						
						if(Int2 == undefined || Int2 == null)
						{
							throw{name:"XOR error",message:"Int1 is undefined/null",lineNumber:139}
						}
						
						return (Int1 ^ Int2);
					}
					catch(err)
					{
						throw{name:"XOR error",message:"XOR: "+err.message}
					}
				},
				ModulusIncrease:function(IntToEncode,IntOffset,MinInt,MaxInt)
				{
					try
					{
						if(IntToEncode < 0 || IntToEncode > (MaxInt-1))
						{
							throw {name:"ModulusIncrease error",message:"IntToEncode out of bounds."};
						}
						if(IntOffset < 0)
						{
							throw {name:"ModulusIncrease error",message:"IntOffset less than 0."};
						}
						return ((MinInt+(IntToEncode+IntOffset))%MaxInt);
					}
					catch(err)
					{
						throw{name:"ModulusIncrease error",message:"ModulusIncrease: "+err.message}
					}
				},
				ModulusDecrease:function(IntToDecode,IntOffset,MinInt,MaxInt)
				{
					try
					{
						if(IntToDecode < 0 || IntToDecode > (MaxInt-1))
						{
							throw {name:"ModulusDecrease error",message:"IntToDecode out of bounds."};
						}
						
						if(IntOffset < 0)
						{
							throw {name:"ModulusDecrease error",message:"IntOffset less than 0."};
						}
						
						var FirstPass = (Math.abs(IntOffset-IntToDecode)%MaxInt);
						var SecondPass = (MaxInt-FirstPass)-MinInt;
						
						return SecondPass;
					}
					catch(err)
					{
						throw{name:"ModulusDecrease error",message:"ModulusDecrease: "+err.message}
					}
				},
				XORArrayOfInts:function(FirstIntArray,SecondIntArray)
				{
					try
					{
						if(FirstIntArray.length != SecondIntArray.length)
						{
							throw{name:"XORArrayOfInts error",message:"input arrays are not the same size!"};
						}

						var XORCollector = [];

						for(var Iter = 0;Iter < FirstIntArray.length;++Iter)
						{
							XORCollector.push(this.XOR(FirstIntArray[Iter],SecondIntArray[Iter]));
						}

						return XORCollector;
					}
					catch(err)
					{
						throw{name:"XORArrayOfInts error",message:"XORArrayOfInts: "+err.message}
					}
				},
				ChunkSubstrings:function(IncomingString, ChunkSize)
				{
					try
					{
						const NumberOfChunks = Math.ceil(IncomingString.length / ChunkSize);
						const Collector = new Array(NumberOfChunks);

						for(var Iter = 0, Offset = 0; Iter < NumberOfChunks; ++Iter, Offset += ChunkSize)
						{
							Collector[Iter] = IncomingString.substr(Offset, ChunkSize);
						}
						return Collector;
					}
					catch(err)
					{
						throw{name:"ChunkSubstrings error",message:"ChunkSubstrings: "+err.message}
					}
				},
				SelfLoopFunctionGeneric:function(SelfFeedingFunction,DataSource,NumberOfTimes)
				{
					try
					{
						var Temp = SelfFeedingFunction(DataSource);
						for(var Iter = 0; Iter < NumberOfTimes;Iter++)
						{
							Temp = SelfFeedingFunction(Temp);
						}
						return Temp;
					}
					catch(err)
					{
						throw{name:"SelfLoopFunctionGeneric error",message:"SelfLoopFunctionGeneric: "+err.message}
					}
				},
				XORStack:function(IncomingInt,XORAmount,Times)
				{
					try
					{
						var Result = this.XOR(IncomingInt,XORAmount);
						for(var Iter = 0;Iter < Times;Iter++)
						{
							Result = this.XOR(Result,XORAmount+Iter)
						}
						return Result;
					
					}
					catch(err)
					{
						throw{name:"XORStack error",message:"XORStack: "+err.message}
					}
				},
				XORUnstack:function(IncomingInt,XORAmount,Times)
				{
					try
					{
						var Result = this.XOR(IncomingInt,(XORAmount+Times));
						for(var Iter = (Times-1);Iter >= 0;Iter--)
						{
							Result = this.XOR(Result,XORAmount-Iter);
						}
						return Result;
					
					}
					catch(err)
					{
						throw{name:"XORUnstack error",message:"XORUnstack: "+err.message}
					}
				}
			}
}

G_Library.Add({Type:"Function",Functor:CreateCommonEncryptionsObject,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"CommonEncryptions.js"});
