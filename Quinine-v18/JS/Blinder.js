/*
	Blinder.js
	
	Created: 30/12/2021
	Updated: 30/12/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		CommonEncryptions.js
		JavaScriptLibraryLoader.js
	
	Updates:
		
		1.0
			Added library

*/

function CreateBlinder()
{
	"use strict";
	try
	{
		var Temp = {
						CEO:CreateCommonEncryptionsObject(),
						Initialise:function()
						{
							"use strict";
							try
							{
								
							}
							catch(err)
							{
								throw{name:"Initialise (CreateBlinder) error",message:"Initialise (CreateBlinder), line number "+err.lineNumber+": "+err.message}
							}
						},
						SplitEvenString:function(IncomingString)
						{
							try
							{
								return [IncomingString.substring(0,IncomingString.length/2),IncomingString.substring(IncomingString.length/2,IncomingString.length)];
							}
							catch(err)
							{
								throw{name:"SplitEvenString error",message:"SplitEvenString, line number "+err.lineNumber+": "+err.message}
							}
						},
						
						FindHighestValue:function(ArrayOfInts)
						{
							"use strict";
							try
							{
								var Max = 1;
								
								for(var Iter = 0;Iter < ArrayOfInts.length;Iter++)
								{
									if(ArrayOfInts[Iter] > Max)
									{
										Max = ArrayOfInts[Iter];
									}
								}
								
								return Max;
							}
							catch(err)
							{
								throw{name:"FindHighestValue error",message:"FindHighestValue, line number "+err.lineNumber+": "+err.message}
							}
						},
						XORSandwichString:function(IncomingString,OptionalMinimumLength)
						{
							"use strict";
							try
							{
								//OptionalMinimumLength
								var Halved = Math.floor(IncomingString.length / 2);
								var MinLength = (!OptionalMinimumLength) ? Halved : OptionalMinimumLength;
								
								if(MinLength < 0)
								{
									MinLength = Halved;
								}
								else if(MinLength > Halved)
								{
									MinLength = Halved;
								}
								
								var ArrayOfInts = this.CEO.StringToArrayOfInts(IncomingString);
								var IntCap = this.FindHighestValue(ArrayOfInts);
								var OffsetIter = (IntCap-2);
								var Iter = 0;
								var NewArrayOfInts = [];
								var HalfLength = 0;
								var FirstPrintableASCII = 32;
								var LastPrintableASCII = 126 - FirstPrintableASCII;
								
								while(true)
								{
									OffsetIter = FirstPrintableASCII + ((OffsetIter + Iter) % LastPrintableASCII);
								
									if(ArrayOfInts.length % 2 !== 0)
									{
										ArrayOfInts.push(OffsetIter)
									}
								
									NewArrayOfInts = []
									HalfLength = ArrayOfInts.length/2;
								
									for(var Iter2 = 0;Iter2 < HalfLength;Iter2 += 2)
									{
										NewArrayOfInts.push(FirstPrintableASCII + (Math.abs(ArrayOfInts[Iter2] ^ ArrayOfInts[Iter2+1]) % LastPrintableASCII))
									}
									
									if(NewArrayOfInts.length < MinLength)
									{
										if(Iter > 0)
										{
											return this.CEO.ArrayOfIntsToString(ArrayOfInts);
										}
									}
									
									ArrayOfInts = NewArrayOfInts;
									Iter += 1;
								}
							}
							catch(err)
							{
								throw{name:"XORSandwichString error",message:"XORSandwichString, line number "+err.lineNumber+": "+err.message}
							}
						},
						
						XORScramblerBigInt:function(IncomingBigInt,OptionalFixedLength,OptionalMustBePositive)
						{
							"use strict";
							try
							{
								var FixedLength = (OptionalFixedLength === undefined) ? false : !!OptionalFixedLength;
								var MustBePositive = (OptionalMustBePositive === undefined) ? false : !!OptionalMustBePositive;
								var OriginalLength = (""+IncomingBigInt).length
								
								var TempBigInt = BigInt(IncomingBigInt);
								
								var Result = 1n, X = 0n, Y = 0n, Z = 0n;

								while(TempBigInt)
								{
									X ^= (TempBigInt % 2n);
									Y ^= (TempBigInt / 2n);
									Z ^= (TempBigInt / 3n);
									
									Result = (X ^ Y ^ Z);
									
									TempBigInt = TempBigInt / 2n;
								}
								
								if(Result < 0n)
								{
									if(MustBePositive)
									{
										Result = Result - (Result * 2);
									}
									else
									{
										OriginalLength += 1;
									}
								}
								
								if(!FixedLength)
								{
									return Result;
								}
								
								var TempString = ""+TempBigInt;
								
								if(TempString.length == OriginalLength)
								{
									return Result
								}
								else if(TempString.length > OriginalLength)
								{
									return BigInt(TempString[0,OriginalLength-1]);
								}
								else
								{
									while(TempString.length < OriginalLength)
									{
										TempString = TempString+""+this.XORScramblerBigInt(BigInt(TempString),false,true);
									}
									
									return BigInt(TempString[0,OriginalLength-1]);
								}
								
							}
							catch(err)
							{
								throw{name:"XORScramblerBigInt error",message:"XORScramblerBigInt, line number "+err.lineNumber+": "+err.message}
							}
						},
						XORLengthHider:function(IncomingBigInt)
						{
							"use strict";
							try
							{
								var TempBigInt = BigInt(IncomingBigInt)
								var BigIntAbs = (TempBigInt < 0) ? (TempBigInt * 2n) : TempBigInt;
								var Len = BigInt((""+TempBigInt).length)
							
								var Modulo = (IncomingBigInt % (Len-1n)) + 1n;
								
								return BigInt(str.substring(0, Modulo));
							}
							catch(err)
							{
								throw{name:"XORLengthHider error",message:"XORLengthHider, line number "+err.lineNumber+": "+err.message}
							}
						},
						XORSandwichBigInt:function(IncomingBigInt, MinimumLength, MustReturnBePostive)
						{
							"use strict";
							try
							{
								var PreviousScramble = this.XORScramblerBigInt(IncomingBigInt);
								var NextScramble = PreviousScramble;
								var OffsetIter = 1n;
								
								do
								{
									var TempString = (""+PreviousScramble);
									
									if(TempString.length % 2 !== 0)
									{
										TempString += ""+OffsetIter
									}
									
									
									NextScramble = ""+this.XORScramblerBigInt(BigInt(this.SplitEvenString(TempString)))
									
									OffsetIter += this.XORScramblerBigInt(1n);
								}
								while(NextScramble.length > MinimumLength);
								
								return PreviousScramble;
							}
							catch(err)
							{
								throw{name:"XORSandwichBigInt error",message:"XORSandwichBigInt, line number "+err.lineNumber+": "+err.message}
							}
						}
					};
					
		Temp.Initialise();
		return Temp;
	}
	catch(err)
	{
		throw{name:"CreateBlinder error",message:"CreateBlinder, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:CreateBlinder,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"Blinder.js"});

