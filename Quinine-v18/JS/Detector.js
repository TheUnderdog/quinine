/*

	Detector.js library
	
	Version: 7.0
	Created: 08/12/2017
	Last Updated: 15/08/2021
	
	Purpose:
		Holds object detection functions (not to be confused with the SystemDetector.js for system properties)
	
	Relies upon:
		JavaScriptLibraryLoader.js
		
	Notes:
		None
		
	Updates:
	
	
		7.0
			Overhauled into an object for compatibility with JavaScriptLibraryLoader
		
		6.0
			Imported IsJavaScriptArray
			Added IsJavaScriptFunction

		5.0
			Phased out Error.js reliance
	
		4.1
			Added CheckAllArgumentsAreDOMElements to bulk process DOM elements
	
		4.0
			Added Element based detectors.
	
		3.0
			Transferred an entire slew of Is_____ functions from Error.js to here
	
		2.0
			Transferred an entire slew of Is_____ functions from CommonFunctions.js to here

		1.0
			Added the CommonFunctions library header
			
	Todo:
		None
	
*/

function Detector()
{
	"use strict";
	return {
				IsInvalid:function(T){ return (T === undefined) || (T === null); },
				IsValid:function(T){ return !this.IsInvalid(T); },

				IsEmpty:function(T){ return this.IsInvalid(T) || (T === ""); },
				IsNotEmpty:function(T){ return !this.IsEmpty(T); },

				IsNumber:function(T){ return this.IsNotEmpty(T) && !isNaN(T); },
				IsNotNumber:function(T){ return !this.IsNumber(T); },

				IsNumeric:function(T){ return this.IsNumber(T); },
				IsNotNumeric:function(T){ return this.IsNotNumber(T); },

				IsEven:function(Num){ return Num % 2 == 0; },
				IsOdd:function(Num){ return !this.IsEven(Num); },

				IsString1:function(T){ return (typeof T === 'string') || (T instanceof String); },
				IsString2:function(T){ return (Object.prototype.toString.call(T) === '[object String]'); },
				IsString:function(T){ return this.IsValid(T) && (this.IsString1(T) || this.IsString2(T)); },
				IsNotString:function(T){ return !this.IsString(T); },

				IsDate:function(T){ return this.IsValid(T) && T instanceof Date; },
				IsNotDate:function(T){ return !this.IsDate(T); },

				IsValidElementID:function(T)
				{
					if(AmWebWorker())
					{
						throw{name:"IsValidElementID error",message:"WebWorker cannot invoke IsValidElementID as document is not a valid namespace."}
					}
					return this.IsValid(T) && this.IsValid(document.getElementById(T)); 
				},
				
				IsNotValidElementID:function(T) { return !this.IsValidElementID(T); },

				IsValidDOMElement:function(T){ return this.IsValid(T) && T instanceof HTMLElement; },
				IsNotValidDOMElement:function(T){ return !this.IsValidDOMElement(T); },

				IsJavaScriptArray:function(T){ return this.IsValid(T) && Array.isArray(T); },
				IsJavaScriptFunction:function(T){ return typeof(T) === "function"; },
				IsJavaScriptObject:function(T){ return (typeof T === "object");},

				/*
					Multi-argument processing checks 
					
					Note, the "CheckAllArguments..." functions are variac functions, that can take any number of arguments
				*/

				CheckAllArguments:function(Function,AppendMessage)
				{
					var Args = Array.prototype.slice.call(arguments);
					
					for(var Iter = 2;Iter < Args.length;Iter++)
					{
						if(Function(Args[Iter]))
						{
							alert("Detector.js: CheckAllArguments: Args["+Iter+"]"+AppendMessage+" Args["+Iter+"]: "+Args[Iter]);
							return false;
						}
					}
					return true;
				},

				CheckAllArgumentsValid:function()
				{
					var Pre = [];
					Pre.push(this.IsInvalid.bind(this));
					Pre.push(" is not valid.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsNotEmpty:function()
				{
					var Pre = [];
					Pre.push(this.IsEmpty.bind(this));
					Pre.push(" is empty.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsAreNumbers:function()
				{
					var Pre = [];
					Pre.push(this.IsNotNumber.bind(this));
					Pre.push(" is not a number.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsAreNotNumbers:function()
				{
					var Pre = [];
					Pre.push(this.IsNumber.bind(this));
					Pre.push(" is a number.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsAreStrings:function()
				{
					var Pre = [];
					Pre.push(this.IsNotString.bind(this));
					Pre.push(" is not a string.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsAreNotStrings:function()
				{
					var Pre = [];
					Pre.push(this.IsString.bind(this));
					Pre.push(" is a string.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				CheckAllArgumentsAreDOMElements:function()
				{
					var Pre = [];
					Pre.push(this.IsString.bind(this));
					Pre.push(" is not a DOM element.");
					var Args = Pre.concat(Array.prototype.slice.call(arguments));
					return CheckAllArguments.apply(null,Args);
				},

				IsItemInArrayLightweight:function(Item,ArrayList)
				{
					return (ArrayList.indexOf(Item) > -1);
				},

				IsArrayOfItemsInArray:function(ArrayOfItems,InThisArray)
				{
					var Item = null;
					var Answer = null;
					for(var Iter = 0;Iter < ArrayOfItems.length;Iter++)
					{
						Item = ArrayOfItems[Iter];
						Answer = InThisArray.indexOf(Item);
						if(Answer > -1)
						{
							return [Iter,Answer];
						}
					}
					return null;
				},

				IsStringOfCharsInString:function(StringOfChars,InThisString)
				{
					var Item = null;
					var Answer = null;
					for(var Iter = 0;Iter < StringOfChars.length;Iter++)
					{
						Item = StringOfChars.charAt(Iter);
						Answer = InThisString.indexOf(Item);
						if(Answer > -1)
						{
							return [Iter,Answer];
						}
					}
					return null;
				},

				//Roughly checks to see if a variable name is valid
				IsValidVariableName:function(VarName)
				{
					if(this.IsNotString(VarName))
					{
						return false;
					}

					if(VarName.length < 1)
					{
						return false;
					}
					
					var Item = VarName.charAt(0);
					
					if(!this.IsItemInArrayLightweight(Item,"_qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"))
					{
						return false;
					}
					
					var Answer = this.IsStringOfCharsInString(VarName,";:.,<>(){}[]/?\\\"\'!£$%^&*@#~|");
					
					if(Answer != null)
					{
						return false;
					}
					
					return true;
				}

			};
}

G_Library.Add({Type:"Function",Functor:Detector,IsWebWorkerCompatible:true});

var G_Detector = Detector();

G_Library.Add({Type:"Variable",Variable:G_Detector,Constructor:Detector,VariableName:"G_Detector",IsWebWorkerCompatible:true});

