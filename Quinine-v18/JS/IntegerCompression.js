/*
	IntegerCompression.js
	
	Created: 28/06/2021
	Updated: 25/12/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Detector.js
		JavaScriptLibraryLoader.js
	
	Updates:
		
		2.0
			Fixed bug in Compressor that used Iter instead of Temp variable
			
		1.0
			Added library

*/

"use strict";

function CreateIntegerCompressor(TargetExpandedCharset,TargetRemovedCharset)
{
	"use strict";
	try
	{
		var Temp = {
						ValidCharList:[],
						InternalCodeList:[],
						DigitLengthCap:0,
						FindInValidCharList:function(TargetChar)
						{
							"use strict";
							try
							{
								for(var Iter = 0;Iter < this.ValidCharList.length;Iter++)
								{
									if(this.ValidCharList[Iter] === TargetChar)
									{
										return Iter;
									}
								}
								
								return -1;
							}
							catch(err)
							{
								throw{name:"FindInValidCharList error",message:"FindInValidCharList, line number "+err.lineNumber+": "+err.message}
							}
						},
						Initialise:function(IncomingExpandedCharset,IncomingRemovedCharset)
						{
							"use strict";
							try
							{
								this.ValidCharList = [];
								
								for(var CharCodeIterFirst = 32; CharCodeIterFirst < 127;CharCodeIterFirst++)
								{
									this.ValidCharList.push( String.fromCharCode(CharCodeIterFirst) );
								}
								
								if(G_Detector.IsString(IncomingExpandedCharset))
								{
									for(var CharCodeIterFirst = 0; CharCodeIterFirst < IncomingExpandedCharset.length; CharCodeIterFirst++)
									{
										this.ValidCharList.push(IncomingExpandedCharset.charAt(CharCodeIterFirst));
									}
								}
								
								if(G_Detector.IsString(IncomingRemovedCharset))
								{
									var TempIter = -1, Update = false;
									for(var CharCodeIterFirst = 0; CharCodeIterFirst < IncomingRemovedCharset.length; CharCodeIterFirst++)
									{
										TempIter = this.FindInValidCharList(IncomingRemovedCharset.charAt(CharCodeIterFirst));
										
										if(TempIter > -1)
										{
											this.ValidCharList[TempIter] = null;
											Update = true;
										}
									}
									
									if(Update)
									{
										var Collector = [];
										for(var Iter = 0; Iter < this.ValidCharList.length;Iter++)
										{
											if(this.ValidCharList[Iter] !== null)
											{
												Collector.push(this.ValidCharList[Iter])
											}
										}
										this.ValidCharList = Collector;
									}
								}
								
								for(var CharCodeIterFirst = 0; CharCodeIterFirst < this.ValidCharList.length;CharCodeIterFirst++)
								{
									for(var CharCodeIterSecond = 0; CharCodeIterSecond < this.ValidCharList.length;CharCodeIterSecond++)
									{
										this.InternalCodeList.push( this.ValidCharList[CharCodeIterFirst] + this.ValidCharList[CharCodeIterSecond] );
									}
								}
								
								this.DigitLengthCap = (""+this.InternalCodeList.length).length;
							}
							catch(err)
							{
								throw{name:"Initialise error",message:"Initialise, line number "+err.lineNumber+": "+err.message}
							}
						},
						ScanForValidNumber:function(IncomingIntString)
						{
							try
							{
								if(IncomingIntString.length === 0)
								{
									throw{name:"ScanForValidNumber error",message:"IncomingIntString is 0."}
								}
								
								if(IncomingIntString.length === 1)
								{
									return IncomingIntString;
								}
								
								let SearchSpace = (IncomingIntString.length > this.DigitLengthCap) ? this.DigitLengthCap : IncomingIntString.length;
								
								let TempString = IncomingIntString.slice(IncomingIntString.length-SearchSpace);
								
								while(SearchSpace > 1)
								{
									if(TempString.charAt(0) === "0")
									{
										SearchSpace--;
									}
									else if(parseInt(TempString) >= this.InternalCodeList.length)
									{
										SearchSpace--;
									}
									else
									{
										return TempString;
									}
									
									TempString = IncomingIntString.slice(IncomingIntString.length-SearchSpace);
								}
								
								return TempString;
							}
							catch(err)
							{
								throw{name:"Compress error",message:"Compress, line number "+err.lineNumber+": "+err.message}
							}
						},
						Compress:function(IncomingInteger)
						{
							try
							{
								var IntString = ""+IncomingInteger;
								var Temp = "";
								var Collector = "";
								
								while(IntString.length > 0)
								{
									Temp = this.ScanForValidNumber(IntString);
									IntString = IntString.slice(0,IntString.length-Temp.length);
									Collector = this.InternalCodeList[parseInt(Temp)] + Collector;
								}
								
								return Collector;
							}
							catch(err)
							{
								throw{name:"Compress error",message:"Compress, line number "+err.lineNumber+": "+err.message}
							}
						},
						FindCodeInListfunction:function(IncomingCode)
						{
							try
							{
								if(IncomingCode.length != 2)
								{
									throw{name:"FindCodeInListfunction error",message:"IncomingCode is not length of two."}
								}
								
								for(var Iter = 0;Iter < this.InternalCodeList.length;Iter++)
								{
									if(this.InternalCodeList[Iter] === IncomingCode)
									{
										return Iter;
									}
								}
								
								return -1;
							}
							catch(err)
							{
								throw{name:"FindCodeInListfunction error",message:"FindCodeInListfunction, line number "+err.lineNumber+": "+err.message}
							}
						},
						Decompress:function(IncomingString)
						{
							try
							{
								if( (IncomingString.length & 1) == 1)
								{
									throw{name:"Decompress error",message:"Length is odd numbered. Only even strings can be decompressed."}
								}
								
								var Collector = "";
								var Temp = -1;
								
								for(var First = 0, Last = 2; First < IncomingString.length; First = First + 2, Last = Last + 2)
								{
									Temp = this.FindCodeInListfunction( IncomingString.slice(First,Last) );
									
									if(Temp == -1)
									{
										throw{name:"Decompress error",message:"Code \""+IncomingString.slice(First,Last)+"\" could not be found in the list."};
									}
									
									Collector += ""+Temp;
									
								}
								
								return Collector;
							}
							catch(err)
							{
								throw{name:"Decompress error",message:"Decompress, line number "+err.lineNumber+": "+err.message}
							}
							
						}
						
					};
					
		Temp.Initialise(TargetExpandedCharset,TargetRemovedCharset);
		return Temp;
	}
	catch(err)
	{
		throw{name:"CreateIntegerCompressor error",message:"CreateIntegerCompressor, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:CreateIntegerCompressor,IsWebWorkerCompatible:true});

var G_IntegerCompressor = CreateIntegerCompressor("£€");

G_Library.Add({Type:"Variable",Variable:G_IntegerCompressor,Constructor:CreateIntegerCompressor,VariableName:"G_IntegerCompressor",IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"IntegerCompression.js"});
