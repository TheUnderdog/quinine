/*
	QuinineUI.js
	
	Created: 02/02/2022
	Updated: 02/02/2022
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Blinder.js
		HashingObject.js
		Temporal.js
		Quinine.js
	
	Updates:
		
		1.0
			Added library
	
	Todo:
			

*/

function PrimeLengthOnChange(PrimeLengthElement,PrimeBitsElementID,PrimeEstTimeID)
{
	try
	{
		var PBEI = document.getElementById(PrimeBitsElementID);
		var PETI = document.getElementById(PrimeEstTimeID);
		
		if(PrimeLengthElement.value == "")
		{
			PrimeLengthElement.value = 1;
		}
		
		var TempLen = Math.abs(parseInt(PrimeLengthElement.value));
		
		PrimeLengthElement.value = TempLen;
		
		PBEI.value = (TempLen * 8);
		PETI.value = (TempLen / 100) / 2
		
	}
	catch(err)
	{
		alert("PrimeLengthOnChange: "+err.message);
	}
	finally
	{
		PBEI = null;
		PETI = null;
		TempLen = null;
	}
}

var G_PrimeBeingGenerated = null;

function GeneratePrime(PrimeLengthElementID,TemporalElementID,TemporalPrecisionElementID)
{
	"use strict";
	try
	{
		if(G_PrimeBeingGenerated != null)
		{
			alert("Prime already currently being generated (reload page if you want to stop and generate another).");
			return;
		}
		else
		{
			
			var PLEID = document.getElementById(PrimeLengthElementID);
			var TEID = document.getElementById(TemporalElementID);
			var TPEI = document.getElementById(TemporalPrecisionElementID);
			
			var ValidTimestamp = BigInt(FlipTimestamp(TEID.value,TPEI.value))
			var Length = BigInt(PLEID.value)
			var TemporalPrecision = BigInt(TPEI.value)

			//Pos 0: Prime length
			//Pos 1: Prime
			//Pos 2: Timestamp
			//Pos 3: Temporal precision
			//Pos 4: Root Primitive
			var TempXORMemory = CreateXORMemoryBigInt(5)
			
			TempXORMemory.SetBigIntAt(Length,0);
			TempXORMemory.SetBigIntAt(0n,1);
			TempXORMemory.SetBigIntAt(ValidTimestamp,2);
			TempXORMemory.SetBigIntAt(TemporalPrecision,3);
			TempXORMemory.SetBigIntAt(0n,4);
			
			G_PrimeBeingGenerated = CreateQuininePrimeHolder(TempXORMemory);
			
			FindNextNearestPrimeBigIntTargetLengthThreaded(ValidTimestamp,Length,G_PrimeBeingGenerated.CallbackPrimeFunction.bind(G_PrimeBeingGenerated));
			alert("Started Generating Prime");
		}
		
	}
	catch(err)
	{
		alert("GeneratePrime: "+err.message);
	}
	finally
	{
		PLEID = null;
		TEID = null;
		TPEI = null;
		
		ValidTimestamp = null;
		Length = null;
		TemporalPrecision = null;
		TempXORMemory = null;
	}
}

function ProcessResponseCode(ReceivedResponseCodeElementID)
{
	try
	{
		//"use strict";
		var RRCEID = document.getElementById(ReceivedResponseCodeElementID);
		//alert("RRCVal: "+RRCEID.value)
		
		if(RRCEID.value == "")
		{
			throw{name:"ProcessResponseCode error",message:"RRCEID.value (The Received Response Code) must not be blank"}
		}
		
		var SplitValues = (""+RRCEID.value).split(",")
		
		if(SplitValues.length != 5)
		{
			throw{name:"ProcessResponseCode error",message:"SplitValues count does not match that of a sent key agreement code. Supplied SplitValues count is: "+SplitValues.length}
		}
		
		var TempCompressor = CreateIntegerCompressor(G_AdditionalCompressorChars,G_RemovedCompressorChars);
		
		var Returns = [0,0,0,0];
		
		var MasqueValue = BigInt(TempCompressor.Decompress(SplitValues[0]))
		Returns[0] = BigInt(TempCompressor.Decompress(SplitValues[1]))
		Returns[1] = BigInt(TempCompressor.Decompress(SplitValues[2]))
		Returns[2] = BigInt(TempCompressor.Decompress(SplitValues[3]))
		Returns[3] = BigInt(TempCompressor.Decompress(SplitValues[4]))
		
		var TempXORMemory = CreateXORMemoryBigInt(4)
		TempXORMemory.SetBigIntAt(Returns[0],0,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[1],1,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[2],2,MasqueValue)
		TempXORMemory.SetBigIntAt(Returns[3],3,MasqueValue)
		
		//Pos 0: IncomingSharedKey
		//Pos 1: IncomingIdenifier
		//Pos 2: Quinine version
		//Pos 3: Handshake stage
			//1 - initiation
			
		if(TempXORMemory.GetBigIntAt(2) !== G_QuinineVersion)
		{
			var HelpfulAddon = ""
			if(TempXORMemory.GetBigIntAt(2) > G_QuinineVersion)
			{
				HelpfulAddon = "theirs is newer than yours."
			}
			else
			{
				HelpfulAddon = "yours is newer than theirs."
			}
			
			alert("Error: Quinine version mismatch. You must use matching versions (newest recommended). Yours is version "+G_QuinineVersion+" and theirs is "+TempXORMemory.GetBigIntAt(2)+". This could be due to data corruption, a bug, tampering or "+HelpfulAddon);
			return;
		}
		
		if(TempXORMemory.GetBigIntAt(3) == 1n)
		{
			//The other person is opening a handshake with us
			
			if(G_PrimeBeingGenerated === null)
			{
				alert("You must generate a prime first before you can process a response code.");
				return;
			}
			
			if(G_PrimeBeingGenerated.PrimeGenerated === false)
			{
				alert("Wait until a prime is generated before processing a response code.");
				return;
			}
			
			//If the prime is still generating, fall out and inform the user to wait
			//If prime is generated, perform agreement
			
			//Pos 0: IncomingSharedKey
			//Pos 1: IncomingIdenifier
			var IncomingXORMemory = CreateXORMemoryBigInt(2);
			
			var TempMasque = G_PRBIO.GetGeneratedBigInt();
			
			IncomingXORMemory.SetBigIntAt(TempXORMemory.GetBigIntAt(0,TempMasque),0,TempMasque)
			IncomingXORMemory.SetBigIntAt(TempXORMemory.GetBigIntAt(1,TempMasque),1,TempMasque)
			
			G_PrimeBeingGenerated.DHUK.GenerateSharedSecret(IncomingXORMemory)
			
			var Blinder = CreateBlinder()		
			var HashObject = CreateHashingObject();
			
			var HVC = document.getElementById("HashVerificationCode");
			var HVCType = (G_Detector.IsEmpty(HVC.innerHTML)) ? "value": "innerHTML";
			
			HVC[HVCType] = Blinder.XORSandwichString(""+G_PrimeBeingGenerated.DHUK.XORMemory.GetBigIntAt(6),10);
			
			var SSID = document.getElementById("SharedSecret");
			var SSIDType = (G_Detector.IsEmpty(SSID.innerHTML)) ? "value": "innerHTML";
			
			SSID[SSIDType] = HashObject.GetSHA512Blinded(""+G_PrimeBeingGenerated.DHUK.XORMemory.GetBigIntAt(6));
		}
		else
		{
			throw{name:"ProcessResponseCode error",message:"Unexpected handshake stage, was given: "+TempXORMemory.SetBigIntAt(3)}
		}
	}
	catch(err)
	{
		alert("ProcessResponseCode: "+err.message);
	}
	finally
	{
		
	}
}
