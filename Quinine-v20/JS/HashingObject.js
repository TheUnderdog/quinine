/*

	HashingObject.js library
	
	Created: 30/12/2021
	Updated: 02/02/2022
	
	Version: 2.0
	 
	Relies upon:
		
		Hashing.js
		Blinder.js
	
	Updates:
		
		2.0
			Added blinder functions

		1.0
			Added the HashingObject library header

*/

function CreateHashingObject()
{
	return {
				InternalBlinder:CreateBlinder(),
		
				MD5:new Hashes.MD5,
				SHA1:new Hashes.SHA1,
				SHA256:new Hashes.SHA256,
				SHA512:new Hashes.SHA512,
				RMD160:new Hashes.RMD160,
				
				GetMD5:function(IncomingString){return this.MD5.hex(IncomingString);},
				GetSHA1:function(IncomingString){return this.SHA1.hex(IncomingString);},
				GetSHA256:function(IncomingString){return this.SHA256.hex(IncomingString);},
				GetSHA512:function(IncomingString){return this.SHA512.hex(IncomingString);},
				GetRMD160:function(IncomingString){return this.RMD160.hex(IncomingString);},
				
				GetMD5Blinded:function(IncomingString,OptionalMinimumLength)
				{
					try
					{
						return this.GetMD5(this.InternalBlinder.XORSandwichString(IncomingString,OptionalMinimumLength));
					}
					catch(err)
					{
						throw{name:"GetMD5Blinded error",message:"GetMD5Blinded: "+err.message}
					}
				},
				GetSHA1Blinded:function(IncomingString,OptionalMinimumLength)
				{
					try
					{
						return this.GetSHA1(this.InternalBlinder.XORSandwichString(IncomingString,OptionalMinimumLength));
					}
					catch(err)
					{
						throw{name:"GetSHA1Blinded error",message:"GetSHA1Blinded: "+err.message}
					}
				},
				GetSHA256Blinded:function(IncomingString,OptionalMinimumLength)
				{
					try
					{
						return this.GetSHA256(this.InternalBlinder.XORSandwichString(IncomingString,OptionalMinimumLength));
					}
					catch(err)
					{
						throw{name:"GetSHA256Blinded error",message:"GetSHA256Blinded: "+err.message}
					}
				},
				GetSHA512Blinded:function(IncomingString,OptionalMinimumLength)
				{
					try
					{
						return this.GetSHA512(this.InternalBlinder.XORSandwichString(IncomingString,OptionalMinimumLength));
					}
					catch(err)
					{
						throw{name:"GetSHA512Blinded error",message:"GetSHA512Blinded: "+err.message}
					}
				},
				GetRMD160Blinded:function(IncomingString,OptionalMinimumLength)
				{
					try
					{
						return this.GetRMD160(this.InternalBlinder.XORSandwichString(IncomingString,OptionalMinimumLength));
					}
					catch(err)
					{
						throw{name:"GetRMD160Blinded error",message:"GetRMD160Blinded: "+err.message}
					}
				}
			}
	
}


G_Library.Add({Type:"Function",Functor:CreateHashingObject,IsWebWorkerCompatible:false});

G_Library.Add({Type:"Header",Filename:"CreateHashingObject.js"});
