/*
	Blurb.js
	
	Created: 05/02/2022
	Updated: 05/02/2022
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		PseudoRandomBigInt.js
	
	Updates:
		
		1.0
			Added library
	
	Todo:
			

*/


var G_InterestingBlurbs = [

"Silver can kill bacteria and is useful in water filtration.",
"Fire can be started using a clear water bottle to focus sunlight onto a dark piece of paper.",
"Excluding single digit primes, all primes end in 1, 3, 7 or 9.",
"You can survive 3 hours without shelter, 3 days without water, 3 weeks without food.",
"Pee onto a rag or clothing in desert climates and use it to help cool yourself down.",
"Salt water will dehydrate you - desalinate it with an osmosis filter or distillation first!",
"Disinfecting water won't remove chemicals, only biological organisms.",
"Rain is generally clean except for the airbourne pollutants, such as aluminium.",
"Pepsi partnered with Senonymx to use dead baby cells (HEK-293) for flavouring; the US SEC approved this as being \"normal\".",
"Coke-Cola have been accused of employing paramilitary thugs to beat up union workers in Columbia.",
"The UK government owns the patent on the AstraZeneca COVID-19 shot.",
"The British Medical Journal published a report on leaked documents relating to RNA instability in mRNA shots.",
"Where plants grow, usually a water source is nearby.",
"Civilisation is usually built up along a major river, so want to find civilisation? Follow the river downstream!",
"Most types of mushroom are poisonious, so if you're not sure on identification, don't eat any.",
"Human poop must be dried before it can be used as fertiliser. It can also be used as fuel for fires.",
"Most primitive, small scale solar distillers expend more sweat in work than water in returns. Also they deplete the spot they're in of moisture unless the area is watered.",
"Sand, a cloth and dirt form the most basic form of water filtration.",
"A large pot with a small hole, a load of sand and a small pot can be used to make a basic 'fridge' with water added to the sand helping it to lower temperatures.",
"Barbituates (pain killers) are made from apple and urine.",
"A crossbow can typically penetrate bullet resistant armour.",
"Special forces stab someone in the lungs so they're unable to scream for help.",
"Unlike any other medical treatment, vaccines have exemptions to lawsuits.",
"Boiling water will kill biological contaminants, but will concentrate chemical ones.",
"Fish tend to contain mercury, especially if they're from the Gulf of Mexico.",
"Flint from a stone, and steel, can be struck together to form sparks to light a fire.",
"Different dangerous animals require different tactics: standing still, running away, or acting aggressive.",
"Don't swim against the current or you'll exhaust yourself.",
"Squirrels have caused more blackouts than terrorists.",
"Iodine can be used to disinfect wounds and disinfect water, although too much is toxic.",
"Hypochlorite bleach can be produced from salt, water and eletrolysis; beware the chlorine fumes and vent outside safely!",
"The weak points of the human body are the eyes, throat, and gut below the rib cage.",
"Creatures of the night get disorientated with loud noises and bright lights.",
"Throwing knives are perfectly balanced so they don't tumble when thrown.",
"Running water is generally cleaner than stationary water.",
"If you run out of toilet paper, use a wet cloth; be sure to sanitise it though!",
"Blackberries can be turned into jam and thus preserved over winter.",
"Honey has antibacterial properties.",
"Body armour loses integrity after each hit and is more likely to fail on successive hits.",
"Tank armour plating is angled to increase the relative thickness and thus protection.",
"Electrolysis can be used to produce oxygen if done correctly.",
"A 14 year old kid once made a working nuclear reactor.",
"You can make thermite using iron oxide and aluminum.",
"You can make plastic string from a plastic bottle using a knife and some wood.",
"Birds can remember people's faces.",
"If birds fly off it suggests movement nearby - it could be a warning of danger!",
"Pressure lighters use pressure to create fire.",
"Barbed arrows are designed to make it difficult to remove from a wound.",
"Longbowmen were considered more dangerous than knights.",
"Gunpowder is 1 part charcoal : 8 parts sulphur : 16 parts potassium nirate (saltpeter).",
"Potassium nirate (saltpeter) can be found in dung and rotting material.",
"Black powder is 3 parts saltpeter : 2 parts charcoal : 1 part sulphur.",
"Pointed sticks can be used to help clean the bits between teeth.",
"Primrose is edible.",
"Thistle's root is edible.",
"Majority of Mature Fireweed is edible.",
"Majority of Young Fireweed is edible, including it's root.",
"Dandelion is edible, although it's roots taste bitter.",
"Nettle is edible, and fire removes the stingers.",
"Dead Nettle (the species) is edible and has no stingers.",
"Daisy's petals, yellow centre and stem are edible.",
"Ox-eye Daisy's petals, yellow centre and stem are edible.",
"Hawthorne berries (remove the inner stone first!) and leaves are edible.",
"Rowan berries are toxic when raw, but can be cooked to remove the toxin.",
"Garlic Mustard's leaves, stem, flower head and vertical seed pods are all edible.",
"Red Clover's flower head and leaves are edible.",
"Knapweed and Greater Knapweed's pink flowers are edible.",
"Burdoch Leaves leaf, stem and root are edible.",
"Only the ripe, black berries of elderberry are edible.",
"Pineapple Weed's flowerhead, stem and feathery leaves are edible.",
"Rosehip's berries innards contain irritant hairs that must be removed prior to eating the flesh.",
"Crab Apples must be chopped up and cooked until water evaporates before being eaten.",
"Pine Needles can be steamed in tea for Vitamin C.",
"Pine Nuts are edible.",
"Only after a Poppy has dried up and died, are the Poppy Seeds edible.",
"Fallen, rotten Walnut 'fruit' has ripened walnuts in it's centre.",
"Acorn can be cut open to reveal a nut inside. If it's bitter, boil it for 30 mins, or soak in cold water for 8 hours.",
"Blackberry bush leaves - if you remove the prickles underneath - can be eaten.",
"It's best to wash raw plants with clean water first to remove any parasite eggs that might be on them.",
"Distilled water can leech electrolytes from your body if you do not add some minerals to it first.",
"Use salt to cure meat and extend it's shelf-life.",
"Smoke meat to extend it's shelf-life.",
"Only 50% of dirt can be compost for growing plants.",
"Button Cap Mushrooms from stores can be grown in 100% compost with coffee grounds, sealed in a jar.",
"It's possible to regrow carrots from their tops.",
"It's possible to grow potatoes from store bought ones.",
"It's possible to grow garlic from store bought fresh garlic cloves.",
"It's possible to grow mint from store bought plants.",
"It's possible to grow pinto plants from store bought dried pinto beans.",
"You can dehydrate fruits to make them last longer.",
"Use a saucepan lid to greatly improve heat efficiency and reduce water losses.",
"Store bought grown uncut celery can be grown in water saturated soil.",
"The leaf stems of garlic can be eaten by drying then boiling, similar to chives.",
"A hobo stove can be made from one small can, one big can, a can opener, a paper towel and alcohol.",
"It's more efficient to keep the lid on a saucepan."

							];
G_Library.Add({Type:"Variable",Variable:G_InterestingBlurbs,VariableName:"G_InterestingBlurbs",IsWebWorkerCompatible:true});

function LoadBlurbs(IncomingTargetElementID)
{
	try
	{
		var BlurbID = document.getElementById(IncomingTargetElementID);
		var BlurbIDType = (G_Detector.IsEmpty(BlurbID.innerHTML)) ? "value": "innerHTML";
		
		BlurbID[BlurbIDType] = G_InterestingBlurbs[G_PRBIO.GetGeneratedBigIntBetweenRange(0,G_InterestingBlurbs.length-1)];
	}
	catch(err)
	{
		alert("LoadBlurbs: "+err.message);
	}
}

G_Library.Add({Type:"Function",Functor:LoadBlurbs,IsWebWorkerCompatible:false});

G_Library.Add({Type:"Header",Filename:"InterestingBlurbs.js"});
