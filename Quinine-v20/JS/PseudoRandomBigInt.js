/*

	PseudoRandomBigInt.js library
	
	Version: 3.1
	Created: 26/06/2021
	Updated: 21/07/2021
	
	Purpose:
		To generate as cryptographically secure as possible randomisation via resource conflict and thread complexity
	
	Relies upon:
		Detector.js [v5.0 or later]
		Surrogate.js [v7.0 or later]
		CommonEncryptions.js [v1.0 or later]

	Updates:
	
		4.0
			Added GetGeneratedBigIntArrayCrypto to use a Crypto context
			Patched typo in GetGeneratedBigIntBetweenRangeAsNumber from Start to IncomingStart
	
		3.1
			Patched comma bug
			Replaced append with push
	
		3.0
			Added GetGeneratedBigIntAsNumber to generate number friendly output
			Added GetGeneratedBigIntBetweenRangeAsNumber to generate number friendly outputs
			Added GetGeneratedBigIntArray to generate arrays of big ints
	
		2.0
			
			Added GetGeneratedBigIntBetweenRange
			Added Initialisation
			Added GenerateRandomBigInt
			Added GetGeneratedBigInt
			Patched bug in BigInt generator where Start and Latest timestamp was the wrong way around
			Standardised 'ClearThreads' as 'RemoveThreads' for consistency
			Added AddThread
			Added AddThreads
			Added RemoveThread
			Added ClearThreads
			Added Destructor
	
		1.0
			Added the PseudoRandomBigInt library header
			
	Todo:
		None
	
*/

function PseudoRandomBigIntObject(TargetNumberOfThreads=null)
{
	//"use strict";
	try
	{	
		var Temp = {
					StartTimeStamp:	+ new Date() + performance.now(),
					LatestTimeStamp:+ new Date() + performance.now(),
					UniqueScuffID:0n,
					RandomBigInt:0n,
					GeneratedBigInts:[],
					MaximumArraySize:200,
					MinInterval:134,
					MaxInterval:345,
					MouseX:0,
					MouseY:0,
					AsyncThreadsIDs:[],
					CEO:CreateCommonEncryptionsObject(),
					MaxSafeInt:BigInt(Number.MAX_SAFE_INTEGER),
					Initialise:function(IncomingNumberOfThreads)
					{
						try
						{
							if(!AmWebWorker())
							{
								this.GenerateNavigatorScuff();
								this.AddMouseListeners();
							}
							
							var TimeWaste = this.StartTimeStamp % 43131;
							
							//Intentional busy work to skewer timings
							for(var Iter = 0; Iter < TimeWaste; Iter++)
							{
								
							}
							
							this.RandomBigInt = this.GenerateRandomBigInt(true);
							
							var TempThreads = IncomingNumberOfThreads;
							
							if(!G_Detector.IsNumber(TempThreads))
							{
								if(AmWebWorker())
								{
									TempThreads = 4;
								}
								else
								{
									TempThreads = 20;
								}
							}
							
							this.AddThreads(TempThreads);
						}
						catch(err)
						{
							throw{name:"Initialise error",message:"Initialise, line numer "+err.lineNumber+": "+err.message}
						}
					},
					SetMouseXY:function()
					{
						try
						{
							if(!AmWebWorker())
							{
								this.MouseX = window.event.pageX;
								this.MouseY = window.event.pageY;
							}
							else
							{
								this.MouseX = this.MouseX+1%63454;
								this.MouseY = this.MouseY+3%73454;
							}
						}
						catch(err)
						{
							throw{name:"SetMouseXY error",message:"SetMouseXY, line numer "+err.lineNumber+": "+err.message}
						}
					},
					AddMouseListeners:function()
					{
						try
						{
							document.addEventListener("mousemove", this.SetMouseXY.bind(this), false);
							document.addEventListener("mouseenter", this.SetMouseXY.bind(this), false);
							document.addEventListener("mouseleave", this.SetMouseXY.bind(this), false);
						}
						catch(err)
						{
							throw{name:"AddMouseListeners error",message:"AddMouseListeners, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GenerateNavigatorScuff:function()
					{
						try
						{
							if(!AmWebWorker())
							{
								var Helmsman = window.navigator;
								
								var CE = 0,
									DM = 0,
									DNT = 0,
									HC = 0,
									TP = 0,
									OL = 0,
									UA = "a";
								
								if(Helmsman.cookieEnabled !== undefined)
								{
									CE = (Helmsman.cookieEnabled) ? 1 : 0;
								}
								
								if(Helmsman.deviceMemory !== undefined)
								{
									DM = Helmsman.deviceMemory + 0;
								}
								
								if(Helmsman.doNotTrack !== undefined)
								{
									DNT = (Helmsman.doNotTrack) ? 1 : 0;
								}
								
								if(Helmsman.hardwareConcurrency !== undefined)
								{
									HC = Helmsman.hardwareConcurrency;
								}
								
								if(Helmsman.maxTouchPoints !== undefined)
								{
									TP = Helmsman.maxTouchPoints;
								}
								
								if(Helmsman.onLine !== undefined)
								{
									OL = (Helmsman.onLine) ? 1 : 0;
								}
								
								if(Helmsman.userAgent !== undefined)
								{
									UA = ""+Helmsman.userAgent;
								}
								
								var AOI = this.CEO.StringToArrayOfInts(UA);
								var Total = 0;
								
								for(var Iter = 0; Iter < (AOI.length-1); Iter++)
								{
									var TempAbs = Math.abs(AOI[Iter]);
									var TempAbs2 = Math.abs(AOI[Iter+1]);
									
									Total = (Total + TempAbs) ^ TempAbs2
								}
								
								Total = Total << CE
								Total = Total + DM
								Total = Total + (8 * DNT)
								Total = Total ^ (HC << 3)
								Total = Total + (17 * TP)
								Total = Total + OL
								
								this.UniqueScuffID = BigInt(Total)
								
								AOI = null;
								Total = null;
								CE = null;
								DM = null;
								DNT = null;
								HC = null;
								TP = null;
								OL = null;
								UA = null;
							}
						}
						catch(err)
						{
							throw{name:"GenerateNavigatorScuff error",message:"GenerateNavigatorScuff, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GenerateScuff:function()
					{
						try
						{
							if(AmWebWorker())
							{
								return 1354654n;
							}
							
							var WIW = 1121;
							var WIH = 877;
							
							WIW = window.innerWidth;
							WIH = window.innerHeight;
							
							var XORdWH = WIW * WIH;
							var XORdMouse = this.MouseX * this.MouseY;
							
							return BigInt(XORdWH ^ XORdMouse);
						}
						catch(err)
						{
							throw{name:"GenerateScuff error",message:"GenerateScuff, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GenerateRandomBigInt:function(OptionalDoNotStore)
					{
						try
						{
							this.LatestTimeStamp = + new Date() + Math.round(performance.now());
					
							let TimeDifference = BigInt(this.LatestTimeStamp - this.StartTimeStamp);
							
							let BitShiftedRandom = this.RandomBigInt << 1n
							let BitShiftedTime = TimeDifference << 1n
							
							let Temp = (TimeDifference ^ BitShiftedRandom) + (BitShiftedTime * this.UniqueScuffID) / this.GenerateScuff()
							
							Temp = Temp >> 1n
							
							this.RandomBigInt = Temp;
							
							if(!OptionalDoNotStore)
							{
								if(this.GeneratedBigInts.length >= this.MaximumArraySize)
								{
									this.GeneratedBigInts.shift();
								}
								
								if(this.GeneratedBigInts[this.GenerateRandomBigInt.length-1] != this.RandomBigInt)
								{
									this.GeneratedBigInts.push(this.RandomBigInt);
								}
							}
							
							return this.RandomBigInt;
						}
						catch(err)
						{
							throw{name:"GenerateRandomBigInt error",message:"GenerateRandomBigInt, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigInt:function()
					{
						try
						{
							if(this.GeneratedBigInts.length > 0)
							{
								return this.GeneratedBigInts.shift();
							}
							else
							{
								return this.GenerateRandomBigInt(true);
							}
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigInt error",message:"GetGeneratedBigInt, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigIntArrayCrypto:function(IncomingTargetLength)
					{
						try
						{
							var BIA = new BigUint64Array(IncomingTargetLength)
							Crypto.getRandomValues(BIA)
							
							var Collector = [];
							
							for(var Iter = 0;Iter < BIA.length;Iter++)
							{
								Collector.push(BigInt(BIA[Iter]));
							}
							
							BIA = null;
							
							return Collector;
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigIntArrayCrypto error",message:"GetGeneratedBigIntArrayCrypto, line number "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigIntArray:function(IncomingTargetLength)
					{
						try
						{
							var TargetLength = BigInt(IncomingTargetLength)
							var Collector = []
							
							for(var Iter = 0n;Iter < TargetLength;Iter++)
							{
								Collector.push(this.GetGeneratedBigInt())
							}
							
							TargetLength = null;
							return Collector;
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigInt error",message:"GetGeneratedBigInt, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigIntAsNumber:function()
					{
						try
						{
							var Temp = 0n;
							
							if(this.GeneratedBigInts.length > 0)
							{
								Temp = this.GeneratedBigInts.shift();
							}
							else
							{
								Temp = this.GenerateRandomBigInt(true);
							}
							
							Temp = Temp % this.MaxSafeInt;
							
							return Number(Temp)
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigIntAsNumber error",message:"GetGeneratedBigIntAsNumber, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigIntBetweenRange:function(IncomingStart,IncomingEnd)
					{
						try
						{	
							var Start = BigInt(IncomingStart);
							var End = BigInt(IncomingEnd);
						
							if(Start == End)
							{
								return Start+0n;
							}
							
							if(Start > End)
							{
								throw{name:"GetGeneratedBigIntBetweenRange error",message:"IncomingStart is higher than IncomingEnd. Start is "+Start+". End is "+End+"."}
							}
							
							return (this.GetGeneratedBigInt() % (End+1n)) + Start
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigInt error",message:"GetGeneratedBigInt, line numer "+err.lineNumber+": "+err.message}
						}
					},
					GetGeneratedBigIntBetweenRangeAsNumber(IncomingStart,IncomingEnd)
					{
						try
						{	
							if(IncomingStart == IncomingEnd)
							{
								return IncomingStart;
							}
							
							if(IncomingStart > IncomingEnd)
							{
								throw{name:"GetGeneratedBigIntBetweenRangeAsNumber error",message:"IncomingStart is higher than IncomingEnd. Start is "+Start+". End is "+End+"."}
							}
							
							var Difference = Math.abs(IncomingEnd-IncomingStart)
							
							var GetInt = this.GetGeneratedBigIntAsNumber()
							var Modulo = GetInt % (Difference+1)
							
							return IncomingStart+Modulo;
							
						}
						catch(err)
						{
							throw{name:"GetGeneratedBigIntBetweenRangeAsNumber error",message:"GetGeneratedBigIntBetweenRangeAsNumber, line numer "+err.lineNumber+": "+err.message}
						}
					},
					
					GenerateRandomInterval:function()
					{
						try
						{
							return ((+ new Date()+performance.now()) % (this.MaxInterval+1) )+this.MinInterval;
						}
						catch(err)
						{
							throw{name:"GenerateRandomInterval error",message:"GenerateRandomInterval, line numer "+err.lineNumber+": "+err.message}
						}
					},
					AddThread:function()
					{
						try
						{
							var RandomInterval = this.GenerateRandomInterval(this.MinInterval,this.MaxInterval);
							this.AsyncThreadsIDs.push(setInterval(this.GenerateRandomBigInt.bind(this),RandomInterval));
						}
						catch(err)
						{
							throw{name:"AddThread error",message:"AddThread, line numer "+err.lineNumber+": "+err.message}
						}
					},
					AddThreads:function(Amount)
					{
						try
						{
							for(var Iter = 0;Iter < Amount;Iter++)
							{
								this.AddThread();
							}
						}
						catch(err)
						{
							throw{name:"AddThreads error",message:"AddThreads, line numer "+err.lineNumber+": "+err.message}
						}
					},
					RemoveThread:function()
					{
						try
						{						
							if(this.AsyncThreadsIDs.length > 0)
							{
								clearInterval(this.AsyncThreadsIDs.shift());
								return true;
							}
							return false;
						}
						catch(err)
						{
							throw{name:"RemoveThread error",message:"RemoveThread, line numer "+err.lineNumber+": "+err.message}
						}
					},
					RemoveThreads:function()
					{
						try
						{
							while(this.RemoveThread());
						}
						catch(err)
						{
							throw{name:"ClearThreads error",message:"ClearThreads, line numer "+err.lineNumber+": "+err.message}
						}
					},
					Destructor:function()
					{
						try
						{
							this.StartTimeStamp = null;
							this.LatestTimeStamp = null;
							this.UniqueScuffID = null;
							this.RandomBigInt = null;
							this.GeneratedBigInts = null;
							this.MaximumArraySize = null;
							this.MinInterval = null;
							this.MaxInterval = null;
							this.MouseX = null;
							this.MouseY = null;
							
							this.CEO = null;
							this.MaxSafeInt = null;
							
							this.RemoveThreads();
							this.AsyncThreadsIDs = null;
						}
						catch(err)
						{
							throw{name:"Destructor (PseudoRandomBigInt) error",message:"Destructor (PseudoRandomBigInt), line numer "+err.lineNumber+": "+err.message}
						}
						
					}
					
				};		
		Temp.Initialise(TargetNumberOfThreads);
		return Temp;
	}
	catch(err)
	{
		throw{name:"PseudoRandomObject error",message:"PseudoRandomObject, line numer "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:PseudoRandomBigIntObject,IsWebWorkerCompatible:true});

G_PRBIO = PseudoRandomBigIntObject();

G_Library.Add({Type:"Variable",Variable:G_PRBIO,Constructor:PseudoRandomBigIntObject,VariableName:"G_PRBIO",IsWebWorkerCompatible:true});


G_Library.Add({Type:"Header",Filename:"PseudoRandomBigInt.js"});
