/*
	Quinine.js
	
	Created: 22/07/2021
	Updated: 29/12/2021
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Detector.js
		Temporal.js
		lz-string.js
		Threaded.js
		PrimeBigIntThreaded.js
		IntegerCompression.js
		DiffieHellmanUpgradedKeyExchangeBigInt.js
	
	Updates:
	
		2.0
			Added G_AdditionalCompressorChars to standise between functions and class
			Added G_RemovedCompressorChars to standardise between functions and class
			
		
		1.0
			Added library
	
	Todo:
		Merge functions as far as possible into class		

*/

var G_QuinineVersion = 16n;
var G_TimestampLength = 14;
var G_AdditionalCompressorChars = "£€";
var G_RemovedCompressorChars = ", ";

//Pos 0: Prime length
//Pos 1: Prime
//Pos 2: Timestamp
//Pos 3: Temporal precision
//Pos 4: Root primitive
function CreateQuininePrimeHolder(TargetIncomingXORMemory)
{
	try
	{
		
		var Temp = {
						
						FPTWorkerManager:null,
						
						//Pos 0: Prime length
						//Pos 1: Prime
						//Pos 2: Timestamp
						//Pos 3: Temporal precision
						//Pos 4: Root primitive
						XORMemory:CreateXORMemoryBigInt(5),
						DHUK:CreateDHUKEBI(),
						PrimeGenerated:false,
						InternalMasqueValue:G_PRBIO.GetGeneratedBigInt(),
						Initialise:function(IncomingXORMemory)
						{
							try
							{
								if(IncomingXORMemory != null)
								{
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(0,this.InternalMasqueValue),0,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(1,this.InternalMasqueValue),1,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(2,this.InternalMasqueValue),2,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(3,this.InternalMasqueValue),3,this.InternalMasqueValue);
									this.XORMemory.SetBigIntAt(IncomingXORMemory.GetBigIntAt(4,this.InternalMasqueValue),4,this.InternalMasqueValue);
								}
							}
							catch(err)
							{
								throw{name:"Initialise error",message:"Initialise, line number "+err.lineNumber+": "+err.message}
							}
						},
						//Generates the key required to perform an agreement
						GenerateDHKeyAgreement:function()
						{
							try
							{
								//Pos 0: timedate stamp (or other prime initialising variable)
								//Pos 1: target length of prime
								//Pos 2: prime number itself
								//Pos 3: root primitive
								var Temp = CreateXORMemoryBigInt(4);
								
								//Pos 2: Timestamp
								//Pos 0: Prime length
								//Pos 1: Prime
								//Pos 4: Root primitive
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(2,this.InternalMasqueValue),0,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(0,this.InternalMasqueValue),1,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(1,this.InternalMasqueValue),2,this.InternalMasqueValue)
								Temp.SetBigIntAt(this.XORMemory.GetBigIntAt(4,this.InternalMasqueValue),3,this.InternalMasqueValue)
							
								return Temp;
							}
							catch(err)
							{
								throw{name:"GenerateDHKeyAgreement error",message:"GenerateDHKeyAgreement, line number "+err.lineNumber+": "+err.message}
							}
						},
						DisplayResponseCode:function(IncomingText)
						{
							try
							{
								var Target = document.getElementById("ResponseCode");
								var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
								Target[Type] = "";
								Target[Type] = IncomingText;
							}
							catch(err)
							{
								alert("DisplayResponseCode: "+err.message);
							}
						},
						CallbackRootPrimitiveFunction:function(IncomingRootPrimitive)
						{
							try
							{
								//alert("callback root prim");
								
								this.XORMemory.SetBigIntAt(IncomingRootPrimitive,4);
								this.DHUK.AcceptPrimeAndRootPrimitive(this.GenerateDHKeyAgreement());
								
								this.DHUK.GenerateSuitableSecret();
								this.DHUK.GenerateSecretAndSharedKey();
								
								var TempXOR = this.DHUK.GetSharedKeyAsXORMemory();
								
								TempXOR.AddBigInt(G_QuinineVersion);
								TempXOR.AddBigInt(1n);
								
								//Pos 0: IncomingSharedKey
								//Pos 1: IncomingIdenifier
								//Pos 2: Quinine version
								//Pos 3: Handshake stage
									//1 - initiation
								
								var MasqueValue = G_PRBIO.GetGeneratedBigInt();
								
								var TempCompressor = CreateIntegerCompressor(G_AdditionalCompressorChars,G_RemovedCompressorChars);
								
								var ReturnString = TempCompressor.Compress(""+MasqueValue) + ",";
								
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(0,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(1,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(2,MasqueValue)) + ",";
								ReturnString += TempCompressor.Compress(""+TempXOR.GetBigIntAt(3,MasqueValue));
								
								this.DisplayResponseCode(""+ReturnString);
								this.PrimeGenerated = true;
								this.FPTWorkerManager.Destructor();
								this.FPTWorkerManager = null;
							}
							catch(err)
							{
								throw{name:"CallbackRootPrimitiveFunction error",message:"CallbackRootPrimitiveFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
						CallbackPrimeFunction:function(IncomingPrime)
						{
							try
							{
								
								if(this.FPTWorkerManager == null)
								{								
									this.XORMemory.SetBigIntAt(IncomingPrime,1);
									
									this.FPTWorkerManager =  WorkerManager(WorkerFunction,G_Library.GetWebWorkerLibrary(),1);
									
									function Temp(KnownPrime)
									{
										//Was FindRootPrimitiveBigInt
										return G_PGBI.FindRootPrimitivePrimeBigInt(KnownPrime);
									}
									
									this.CallbackRootPrimitiveFunction(2n);
									
									//this.FPTWorkerManager.SetOnCompleteHook(this.CallbackRootPrimitiveFunction.bind(this));
									//this.FPTWorkerManager.ProcessWorkload(Temp,[[IncomingPrime]]);
									
									//alert("root prim loaded");
								}
								else
								{
									throw{name:"CallbackPrimeFunction error",message:"Callback made for prime when root primitive hasn't finished for earlier prime."}
								}
									
							}
							catch(err)
							{
								throw{name:"CallbackPrimeFunction error",message:"CallbackPrimeFunction, line number "+err.lineNumber+": "+err.message}
							}
						},
					};
		
		Temp.Initialise(TargetIncomingXORMemory);
		return Temp;
	}
	catch(err)
	{
		throw{name:"CreateQuininePrimeHolder error",message:"CreateQuininePrimeHolder, line number "+err.lineNumber+": "+err.message}
	}
}
