/*

	WorkerManager.js library
	
	Version: 3.1
	Created: 30/06/2021
	Updated: 29/12/2021
	
	Purpose:
		
		Simplify threading operations in JavaScript and handle memory management tasks
	
	Relies upon:
		
		Detector.js (G_Detector.IsJavaScriptArray)
		Surrogate.js (CreateBlob)
		Worker.js 

	Licence(s):
		
		Rest of code: MIT

	Updates:
	
		4.0
			
	
		3.2
			Removed ImportScripts support from WorkerManager and added generic prepend statement
	
		3.1
			Updated Reset to include hooks
	
	
		3.0
			Refactored WorkManager to become non-blocking and use poll-by-work referencing instead of busy-polling
			Refactored Employee to enable scripts support
			Externalised AreWebWorkersAvailable function so any process can call it
			Modified WorkManager to include process hooks
			Added ClearWorkload to make it easier to know what to invoke in order to clear off the workload
			Added Reset
	
		2.0
			Added CreateWebWorkerFromString to allow worker construction from a string rather than file
			Added CreateWorkloadResponseObject to allow the creation of a response object for processing feedback
			Added WorkerFunction as a generic Functor to handle other functors
			Added CreateEmployeeObject to produce Web Workers with status updates and IDs
			Added WorkerManager to handle employees
	
		1.0
			Added the WorkerManager library header
			
	Todo:
		None
	
*/

function AreWebWorkersAvailable()
{
	"use strict";
	if(!AmWebWorker())
	{
		return !!window.Worker;
	}
	
	return false;
}


G_Library.Add({Type:"Function",Functor:AreWebWorkersAvailable,IsWebWorkerCompatible:true});

function CreateWebWorkerFromString(IncomingResponse)
{
	"use strict";
	try
	{
		var BlobObject = CreateBlob([IncomingResponse], {type: 'application/javascript'});	
		return new Worker(URL.createObjectURL(BlobObject));
	}
	catch(err)
	{
		throw{name:"CreateWebWorkerFromString error",message:"CreateWebWorkerFromString: "+err.message}
	}
	finally
	{
		BlobObject = null;
	}
}

G_Library.Add({Type:"Function",Functor:CreateWebWorkerFromString,IsWebWorkerCompatible:true});

function CreateWorkloadResponseObject(IncomingWorkloadID,IncomingWorkloadResult,IncomingWorkloadStatus)
{
	"use strict";
	return {
				WorkloadID:IncomingWorkloadID,
				WorkloadResult:IncomingWorkloadResult,
				//0: no status returned
				//1: success
				//2: error
				WorkloadStatus:IncomingWorkloadStatus
			}
}

G_Library.Add({Type:"Function",Functor:CreateWorkloadResponseObject,IsWebWorkerCompatible:true});

function WebWorkerTemplateFunction(IncomingEvent)
{
	"use strict";
	try
	{
		if(IncomingEvent.data[0].indexOf("function") < 0)
		{
			throw{name:"Worker.js error",message:"IncomingEvent.data[0] does not contain a function."}
		}
		
		var Functor = null;
		
		if(IncomingEvent.data[0].indexOf("function(") > -1)
		{
			//anonymous function call
			Functor = eval("var __A = "+IncomingEvent.data[0]+"\r\n__A;")
		}
		else
		{
			//Named function call
			var TargetFunct = "function ";
			
			var FunctionName = IncomingEvent.data[0].substring(IncomingEvent.data[0].indexOf(TargetFunct)+TargetFunct.length,IncomingEvent.data[0].indexOf("("))
			
			Functor = eval(IncomingEvent.data[0] + "\r\n"+FunctionName+";")
		}
		
		postMessage(Functor.apply(Functor,IncomingEvent.data[1]));
		
	}
	catch(err)
	{
		console.log("Worker.js, "+err.lineNumber+": "+err.message);
		throw{name:"Worker.js error",message:"Worker.js, "+err.lineNumber+": "+err.message}
	}
	finally
	{
		//console.log("WorkerFunction finished");
	}
}

G_Library.Add({Type:"Function",Functor:WebWorkerTemplateFunction,IsWebWorkerCompatible:true});

var WorkerFunction = WebWorkerTemplateFunction;

G_Library.Add({Type:"Literal",Text:"var WorkerFunction = WebWorkerTemplateFunction"});

//OptionalTargetImportScripts - must be a list of strings indicating file location
function CreateEmployeeObject(TargetFunction=WorkerFunction,TargetWorkManagerReference=null,OptionalTargetPrependStatement=null)
{
	var Temp = {
						WorkManagerReference:null,
						Employee:null,
						//0: inactive
						//1: busy
						//2: finished
						EmployeeStatus:0,
						//-1: no valid/assigned WorkloadID
						CurrentWorkloadID:-1,
						CurrentWorkloadResult:[],
						//0: no status returned
						//1: success
						//2: error
						CurrentWorkloadStatus:0,
						InitialiseMessage:"",
						GetEmployeeID:function()
						{
							return this.EmployeeID;
						},
						OnSuccess:function(IncomingEvent)
						{
							try
							{
								this.CurrentWorkloadResult.push(IncomingEvent.data);
								this.CurrentWorkloadStatus = 1;
								this.EmployeeStatus = 2;
								
								if(this.WorkManagerReference != null)
								{
									this.WorkManagerReference.ProcessEmployee(this,this.PopWorkload());
								}
							}
							catch(err)
							{
								console.log("OnSuccess: "+err.message)
								throw{name:"OnSuccess error",message:"OnSuccess: "+err.message}
							}
						},
						OnError:function(IncomingEvent)
						{
							try
							{
								this.CurrentWorkloadResult.push(undefined)
								this.CurrentWorkloadStatus = 2;
								this.EmployeeStatus = 2;
								
								if(this.WorkManagerReference != null)
								{
									this.WorkManagerReference.ProcessEmployee(this,this.PopWorkload());
								}
							}
							catch(err)
							{
								throw{name:"OnError error",message:"OnError: "+err.message}
							}
						},
						Initialise:function(IncomingFunction,IncomingWorkManagerReference=null,OptionalPrependStatement=null)
						{
							try
							{
								if(G_Detector.IsString(OptionalPrependStatement))
								{
									this.InitialiseMessage = ""+OptionalPrependStatement+"self.onmessage="+IncomingFunction;
								}
								else
								{
									this.InitialiseMessage = "self.onmessage="+IncomingFunction;
								}
								
								this.Employee = CreateWebWorkerFromString(this.InitialiseMessage);
								
								this.Employee.onmessage = this.OnSuccess.bind(this);
								this.Employee.onerror = this.OnError.bind(this);
								this.WorkManagerReference = IncomingWorkManagerReference;
								
							}
							catch(err)
							{
								throw{name:"Initialise (CreateEmployeeObject) error",message:"Initialise (CreateEmployeeObject):"+err.message}
							}
						},
						Reinitialise:function()
						{
							try
							{
								this.Employee = null;
								
								this.Employee = CreateWebWorkerFromString(this.InitialiseMessage);
								
								this.Employee.onmessage = this.OnSuccess.bind(this);
								this.Employee.onerror = this.OnError.bind(this);
								
							}
							catch(err)
							{
								throw{name:"Reinitialise (CreateEmployeeObject) error",message:"Reinitialise (CreateEmployeeObject):"+err.message}
							}
						},
						PushWorkload:function(IncomingWorkloadID,IncomingArguments)
						{
							try
							{
								if(this.EmployeeStatus == 0)
								{
									this.CurrentWorkloadStatus = 0;
									this.CurrentWorkloadID = IncomingWorkloadID+0;
									this.Employee.postMessage(IncomingArguments);
									this.EmployeeStatus = 1;
								}
								else
								{
									throw{name:"PushWorkload error",message:"this.EmployeeStatus is "+this.EmployeeStatus,lineNumber:90}
								}
							}
							catch(err)
							{
								throw{name:"PushWorkload error",message:"PushWorkload, line number "+err.lineNumber+": "+err.message}
							}
						},
						PopWorkload:function()
						{
							try
							{
								if(this.EmployeeStatus == 2)
								{
									var Temp = CreateWorkloadResponseObject(this.CurrentWorkloadID+0,this.CurrentWorkloadResult.pop(),this.CurrentWorkloadStatus);
								
									this.CurrentWorkloadID = -1;
									this.CurrentWorkloadStatus = 0;
									this.EmployeeStatus = 0;
								
									return Temp;
								}
								else
								{
									return null;
								}
							}
							catch(err)
							{
								throw{name:"PopWorkload error",message:"PopWorkload:"+err.message}
							}
						},
						Stop:function()
						{
							this.Employee.terminate();
						},
						Destructor:function()
						{
							if(this.Employee != null)
							{
								this.Employee.terminate();
							}
							this.Employee = null;
						}
				};
	
	Temp.Initialise(TargetFunction,TargetWorkManagerReference,OptionalTargetPrependStatement);
	return Temp;
}

G_Library.Add({Type:"Function",Functor:CreateEmployeeObject,IsWebWorkerCompatible:true});

function WorkerManager(TargetFunction=WorkerFunction,OptionalTargetPrependStatement=null,OptionalTargetEmployeeCountOverride=null)
{
	try
	{
		var Temp = {
					Employees:[],
					CurrentWorkload:null,
					CompletedWorkloadStorage:[],
					CurrentWorkloadID:0,
					CompletedWorkloads:0,
					FunctorTemplate:null,
					OnCompleteHook:null,//An optional function to call/trigger upon workload completion
					ConditionalHook:null,//An optional function to call to assess if the current result meets a specific criterion to trigger a WorkloadResult
					OnResultHook:null, //Called if a result triggers WorkloadResultConditionalFunctionHook to return true
					
					InternalWorkerFunction:null,
					InternalPrependStatement:null,
					EmployeeCountOverride:null,
					
					TempFunctor:null,
					
					SetPrependStatement:function(PrependStatement)
					{
						"use strict";
						try
						{
							this.InternalPrependStatement = PrependStatement;
						}
						catch(err)
						{
							throw{name:"SetPrependStatement error",message:"SetPrependStatement: "+err.message}
						}
					},
					Initialise:function(IncomingFunction,OptionalPrependStatement,OptionalEmployeeCountOverride)
					{
						"use strict";
						try
						{
							this.InternalWorkerFunction = IncomingFunction;
							this.InternalPrependStatement = (OptionalPrependStatement === undefined) ? null : OptionalPrependStatement;
							this.EmployeeCountOverride = (OptionalEmployeeCountOverride === undefined) ? null : OptionalEmployeeCountOverride;
						}
						catch(err)
						{
							throw{name:"Initialise error",message:"Initialise: "+err.message}
						}
					},
					InitialiseEmployees:function(OptionalEmployeeCountOverride=null)
					{
						try
						{
							if(this.Employees.length < 1)
							{
								var SafetySwitch = 1;
								
								SafetySwitch = (navigator.hardwareConcurrency > 0) ? navigator.hardwareConcurrency : 1;
								
								if(OptionalEmployeeCountOverride != null)
								{
									if(OptionalEmployeeCountOverride > 0)
									{
										SafetySwitch = OptionalEmployeeCountOverride;
									}
								}
								else if(this.EmployeeCountOverride != null)
								{
									if(this.EmployeeCountOverride > 0)
									{
										SafetySwitch = this.EmployeeCountOverride;
									}
								}
								
								SafetySwitch = (navigator.hardwareConcurrency < SafetySwitch) ? navigator.hardwareConcurrency : SafetySwitch;
								
								for(var Iter = SafetySwitch; Iter > 0; Iter--)
								{
									this.Employees.push(CreateEmployeeObject(this.InternalWorkerFunction,this,this.InternalPrependStatement))
								}
							}
						}
						catch(err)
						{
							throw{name:"InitialiseEmployees error",message:"InitialiseEmployees: "+err.message}
						}
						finally
						{
							SafetySwitch = null;
						}
					},
					ReinitialiseEmployees:function()
					{
						"use strict";
						try
						{
							for(var Iter = 0; Iter > this.Employees.length; Iter++)
							{
								this.Employees[Iter].Reinitialise();
							}
						}
						catch(err)
						{
							throw{name:"ReinitialiseEmployees error",message:"ReinitialiseEmployees: "+err.message}
						}
					},
					Reset:function()
					{
						"use strict";
						
						this.Employees = [];
						this.CurrentWorkload = null;
						this.CompletedWorkloadStorage = [];
						this.CurrentWorkloadID = 0;
						this.CompletedWorkloads = 0;
						this.FunctorTemplate = null;
						this.OnCompleteHook = null;
						this.ConditionalHook = null;
						this.OnResultHook = null;
					},
					AreTasksComplete:function()
					{
						"use strict";
						try
						{
							if(this.CompletedWorkloadStorage == undefined || this.CompletedWorkloadStorage == null)
							{
								return true;
							}
							
							if(this.CompletedWorkloadStorage.length < 1)
							{
								return true;
							}
							
							return (this.CompletedWorkloads >= this.CompletedWorkloadStorage.length);
						}
						catch(err)
						{
							throw{name:"AreTasksComplete error",message:"AreTasksComplete: "+err.message}
						}
					},
					SetOnCompleteHook:function(IncomingFunction)
					{
						"use strict";
						try
						{
							if(!G_Detector.IsJavaScriptFunction(IncomingFunction))
							{
								throw{name:"SetOnCompleteHook error",message:"IncomingFunction is not a function."}
							}
							
							this.OnCompleteHook = IncomingFunction;
						}
						catch(err)
						{
							throw{name:"SetOnCompleteHook error",message:"SetOnCompleteHook: "+err.message}
						}
					},
					SetConditionalHook:function(IncomingFunction)
					{
						"use strict";
						try
						{
							if(!G_Detector.IsJavaScriptFunction(IncomingFunction))
							{
								throw{name:"SetConditionalHook error",message:"IncomingFunction is not a function."}
							}
							
							this.ConditionalHook = IncomingFunction;
						}
						catch(err)
						{
							throw{name:"SetConditionalHook error",message:"SetConditionalHook: "+err.message}
						}
					},
					SetOnResultHook:function(IncomingFunction)
					{
						"use strict";
						try
						{
							if(!G_Detector.IsJavaScriptFunction(IncomingFunction))
							{
								throw{name:"SetOnResultHook error",message:"IncomingFunction is not a function."}
							}
							
							this.OnResultHook = IncomingFunction;
						}
						catch(err)
						{
							throw{name:"SetOnResultHook error",message:"SetOnResultHook: "+err.message}
						}
					},
					//Note: ProcessEmployer is not contiguous and correct results can arrive out-of-order
					//due to multithreading, it will report the result it first encounters
					//not necessarily the earliest result in a workset
					ProcessEmployee:function(IncomingEmployee,IncomingWorkload)
					{
						"use strict";
						try
						{
							this.CompletedWorkloadStorage[IncomingWorkload.WorkloadID] = IncomingWorkload;
							this.CompletedWorkloads++;
							
							if(G_Detector.IsJavaScriptFunction(this.ConditionalHook))
							{
								if(this.ConditionalHook(IncomingWorkload))
								{
									if(G_Detector.IsJavaScriptFunction(this.OnResultHook))
									{
										this.OnResultHook(IncomingWorkload);
									}
								}
							}
							
							if(this.CurrentWorkload == null)
							{
								return;
							}
							
							if(this.CurrentWorkloadID < this.CurrentWorkload.length)
							{
								IncomingEmployee.PushWorkload(this.CurrentWorkloadID+0,[""+this.FunctorTemplate,this.CurrentWorkload[this.CurrentWorkloadID]])
								this.CurrentWorkloadID++;
							}
							else if(this.AreTasksComplete())
							{
								this.ReinitialiseEmployees();
								
								if(G_Detector.IsJavaScriptFunction(this.OnCompleteHook))
								{
									this.OnCompleteHook(this.CurrentWorkload);
								}
							}
						}
						catch(err)
						{
							throw{name:"ProcessEmployee error",message:"ProcessEmployee, "+err.lineNumber+": "+err.message}
						}
					},
					//Note: Functor must be convertable to text because JavaScript is too snotty to do a functor transfer
					//Note: Items in arguments list must be cloneable (read: iterable), so functions, special objects may not suit
					//Note: Web Workers support a second argument (called 'transfer') that permits object transferance but it's an atomic locking process (IE locks the object and cannot be used whilst in use by web worker)
					ProcessWorkload:function(IncomingFunctorTemplate,ListOfArgumentLists)
					{
						"use strict";
						try
						{
							if(!this.AreTasksComplete())
							{
								throw{name:"ProcessWorkload error",message:"Pending tasks are not yet complete. Cannot process new workloads. Either call Destructor to clear or use AppendWorkload."}
							}
							
							if(typeof(IncomingFunctorTemplate) !== "function")
							{
								throw{name:"ProcessWorkload error",message:"FunctorTemplate is not a function"};
							}
							
							if(!G_Detector.IsJavaScriptArray(ListOfArgumentLists))
							{
								throw{name:"ProcessWorkload error",message:"ListOfArgumentLists is not a list"};
							}
							
							if(ListOfArgumentLists.length < 1)
							{
								throw{name:"ProcessWorkload error",message:"ListOfArgumentLists is less then 1"};
							}
							
							for(var Iter = 0;Iter < ListOfArgumentLists.length;Iter++)
							{
								if(!G_Detector.IsJavaScriptArray(ListOfArgumentLists[Iter]))
								{
									throw{name:"ProcessWorkload error",message:"ListOfArgumentLists["+Iter+"] is not a list. This list must contain other lists holding all the arguments."};
								}
							}
							
							this.InitialiseEmployees();
							
							this.FunctorTemplate = IncomingFunctorTemplate;
							
							this.CompletedWorkloadStorage = Array.apply(null, Array(ListOfArgumentLists.length)).map(function () {});
							
							this.CurrentWorkload = ListOfArgumentLists;
							this.CurrentWorkloadID = 0
							this.CompletedWorkloads = 0;
							
							this.TempFunctor = ""+this.FunctorTemplate;
							
							//Start the ball rolling by assigning employees tasks
							for(var Iter = 0;Iter < this.Employees.length;Iter++)
							{
								if(this.Employees[Iter].EmployeeStatus == 0)
								{
									if(this.CurrentWorkloadID >= this.CurrentWorkload.length)
									{
										//continue;
										break;
									}
									
									this.Employees[Iter].PushWorkload(this.CurrentWorkloadID+0,[this.TempFunctor,this.CurrentWorkload[this.CurrentWorkloadID]])
									this.CurrentWorkloadID++;
								}
							}
						}
						catch(err)
						{
							throw{name:"ProcessWorkload error",message:"ProcessWorkload: "+err.message}
						}
					},
					AppendWorkload:function(ListOfArgumentLists)
					{
						"use strict";
						try
						{	
							if(ListOfArgumentLists.length < 1)
							{
								throw{name:"ProcessWorkload error",message:"ListOfArgumentLists is less then 1"};
							}
							
							this.CurrentWorkload = this.CurrentWorkload.concat(ListOfArgumentLists);
							this.TempFunctor = ""+this.FunctorTemplate;
							
							for(var Iter = 0;Iter < this.Employees.length;Iter++)
							{
								if(this.Employees[Iter].EmployeeStatus == 0)
								{
									if(this.CurrentWorkloadID >= this.CurrentWorkload.length)
									{
										continue;
									}
									
									this.Employees[Iter].PushWorkload(this.CurrentWorkloadID+0,[this.TempFunctor,this.CurrentWorkload[this.CurrentWorkloadID]])
									this.CurrentWorkloadID++;
								}
							}
							
							
						}
						catch(err)
						{
							throw{name:"ProcessWorkload error",message:"ProcessWorkload: "+err.message}
						}
					},
					ClearWorkload:function()
					{
						"use strict";
						try
						{
							if(this.Employees.length > 0)
							{
								for(var Iter = 0; Iter < this.Employees.length; Iter++)
								{
									this.Employees[Iter].Stop();
								}
							}
							
							this.CurrentWorkload = null;
							this.CompletedWorkloadStorage = [];
							this.CurrentWorkloadID = 0;
							this.CompletedWorkloads = 0;
						}
						catch(err)
						{
							throw{name:"ClearWorkload error",message:"ClearWorkload: "+err.message}
						}
					},
					Destructor:function()
					{
						"use strict";
						try
						{
							if(this.Employees.length > 0)
							{
								for(var Iter = 0; Iter < this.Employees.length; Iter++)
								{
									this.Employees[Iter].Destructor();
								}
							}
							
							this.Reset();
						}
						catch(err)
						{
							throw{name:"Destructor (WorkerManager) error",message:"Destructor (WorkerManager): "+err.message}
						}
					}
				};
				
		Temp.Initialise(TargetFunction,OptionalTargetPrependStatement,OptionalTargetEmployeeCountOverride);
		return Temp;
	}
	catch(err)
	{
		throw{name:"WorkerManager error",message:"WorkerManager error, line number "+err.lineNumber+": "+err.message}
	}
}

G_Library.Add({Type:"Function",Functor:WorkerManager,IsWebWorkerCompatible:true});

G_Library.Add({Type:"Header",Filename:"Threaded.js"});
