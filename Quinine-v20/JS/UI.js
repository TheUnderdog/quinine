/*
	UI.js
	
	Created: 02/02/2022
	Updated: 02/02/2022
	
	Licence(s):
		
		Rest of the code: MIT
	
	Depends upon:
		
		Temporal.js
	
	Updates:
		
		1.0
			Added library
	
	Todo:
			

*/

function ClearType(TargetID)
{
	try
	{
		var Target = document.getElementById(TargetID);
		var Type = (G_Detector.IsEmpty(Target.innerHTML)) ? "value": "innerHTML";
		Target[Type] = "";
	}
	catch(err)
	{
		alert("ClearType: "+err.message);
	}
}

function ClearDecrypted()	{ ClearType("unencrypted");	}
function ClearEncrypted()	{ ClearType("encrypted");	}

function TimestampOnChange(IncomingElement)
{
	try
	{
		var Temp = IncomingElement.value;
		IncomingElement.value = Temp + "0".repeat(G_TimestampLength - Temp.length)
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function TimestampOnLoad(IncomingID)
{
	try
	{
		var IncomingElement = document.getElementById(IncomingID);
		var Temp = CreateTemporalObject();
		Temp.LoadCurrentTime();
		IncomingElement.value =	Temp.GetFixedTime("YYYYMMDDhhmmss");
	}
	catch(err)
	{
		alert("TimestampOnChange: "+err.message);
	}
	finally
	{
		Temp = null;
	}
}

function FlipTimestamp(IncomingTimestamp,TemporalPrecision)
{
	try
	{
		var TempTO = CreateTemporalObject();
		var Temp = ""+IncomingTimestamp;
		
		if(Temp.length > G_TimestampLength)
		{
			Temp = Temp.substring(0,G_TimestampLength);
		}
		
		var TempPrecision = TemporalPrecision
		
		if(Temp.length > TempPrecision)
		{
			Temp = (TempPrecision > 0) ? Temp.substring(0,TempPrecision) : "";
		}
		
		if(Temp.length < G_TimestampLength)
		{
			Temp = Temp + "0".repeat(G_TimestampLength - Temp.length)
		}
		
		TempTO.SetFixedTime(Temp,"YYYYMMDDhhmmss");
		
		var NewTimestamp = TempTO.GetFixedTime("ssmmhhDDMMYYYY");
		
		while(NewTimestamp.charAt(0) == "0")
		{
			NewTimestamp = NewTimestamp.substring(1);
		}
		
		if(NewTimestamp == "")
		{
			NewTimestamp = "0";
		}
		
		return NewTimestamp;
	}
	catch(err)
	{
		throw{name:"FlipTimestamp error",message:"FlipTimestamp, line number "+err.lineNumber+": "+err.message}
	}
}
