#Comment to stop GitLab deleting this stuff

# Quinine

JavaScript based keysharing and agreement program 

To use:
Download Quinine-v16 (download the entire source code as a zip).
Go into the Quinine-v16 folder and open 'generation.html' in a relatively modern browser (one that supports BigInt and webworkers).
Follow the step-by-step instructions.

If you are testing to see if it works, set Prime Length to 10. This is a very insecure length but it will generate extremely fast to make it easy to test.

Please note:
Quinine has **not** passed security validation testing and should not at present be considered secure. This is a development version.

How to Use:

* In the top right, click the down arrow, and choose 'zip' to download.
* Extract the quinine.zip file to any location of your choosing (even a pendrive)
* Enter the folder with the highest number
* Right-click index.htm and click to open it with Firefox (if Firefox is the default, you can just double-click)
* Follow the instructions that are displayed on-screen step-by-step
* Ctrl + F5 to forcefully clear the page if you have any issues
* Make sure the other person follows the same steps as above

Current Features:

* Temporal Salt with Temporal Precision
* Custom User-Specified Bit-set
* Optimised Prime Searching
* XOR Sandwich Hash
* XOR Memory Storage
* Hashed Key
* Key verification code
* Version Detection (only same versions can talk to each other)

Missing features:

* Report on progress [Halting Problem?]
* Hash key forking [Standalone?]
* Steganography overhaul [Mobile Compatible?]
* Unicode support for the IntegerCompression class
* Custom characters for the IntegerCompression class
* Less annoying user interface
* Compact mode
* Multi-key agreement system
* Shared Computer Hint Mode (read: giving hints to another machine as to where to look)
* Progressive complexity testing to work out the strongest bit-set a computer can do in 30 seconds
* Add functionality for Brave
* Mouse and Keylogger resistant on-screen keyboard

Bugs:

* Does not currently work in Brave (and likely Chrome)
* Estimate on time for completion does not factor in processing power and is horribly inaccurate
